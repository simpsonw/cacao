#!/bin/bash
DOCKER_IP=$(ip addr show docker0 | grep -Po 'inet \K[\d.]+')
DOCKER_PORT=$(docker port k3d-user-cluster-serverlb | grep -Po "(?<=:)(?<port>[0-9]+)")
echo $(\
  k3d kubeconfig get user-cluster |\
	sed "s/server:.*$/server: https:\/\/${DOCKER_IP}:${DOCKER_PORT}/" |\
	sed "s/certificate-authority-data:.*$/insecure-skip-tls-verify: true/" |\
	base64 -w0\
)
