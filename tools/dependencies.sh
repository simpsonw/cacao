#!/bin/bash

# This script checks to ensure that the dependencies in internal/imports/imports.go
# match the actual dependencies needed for the application.

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd $SCRIPT_DIR/..
cp internal/imports/imports.go internal/imports/imports.go.original
go generate internal/imports/gen.go
diff internal/imports/imports.go internal/imports/imports.go.original > /tmp/imports.go.diff
if [ `cat /tmp/imports.go.diff | wc -l` -ne 0 ]; then
    printf "Dependency check failed!\n"
    printf "Diff output:\n\n"
    cat /tmp/imports.go.diff
    printf "\nIf you're a human running this script, check the newly generated internal/imports/imports.go files and ensure that it works.\n"
    printf "The original imports.go file has been preserved in internal/imports/imports.go.original\n"
else
    rm internal/imports/imports.go.original
fi
go build internal/imports/imports.go || exit 1
