package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// ListHandler is a handler for List query
type ListHandler func(actor string, emulator string) ([]types.Workspace, error)

// GetHandler is a handler for Get query
type GetHandler func(actor string, emulator string, worksapceID cacao_common.ID) (types.Workspace, error)

// MockIncomingQueryPort is a mock implementation of IncomingQueryPort
type MockIncomingQueryPort struct {
	Config      *types.Config
	ListHandler ListHandler
	GetHandler  GetHandler
}

// Init inits the port
func (port *MockIncomingQueryPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingQueryPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingQueryPort) InitChannel(channel chan types.WorkspaceChannelRequest) {}

// Start starts the port
func (port *MockIncomingQueryPort) Start() {}

// SetListHandler sets a handler for List query
func (port *MockIncomingQueryPort) SetListHandler(listHandler ListHandler) {
	port.ListHandler = listHandler
}

// SetGetHandler sets a handler for Get query
func (port *MockIncomingQueryPort) SetGetHandler(getHandler GetHandler) {
	port.GetHandler = getHandler
}

// List lists workspaces
func (port *MockIncomingQueryPort) List(actor string, emulator string) ([]types.Workspace, error) {
	return port.ListHandler(actor, emulator)
}

// Get returns a workspace
func (port *MockIncomingQueryPort) Get(actor string, emulator string, worksapceID cacao_common.ID) (types.Workspace, error) {
	return port.GetHandler(actor, emulator, worksapceID)
}
