package types

import "fmt"

// WorkspaceNotFoundError is a struct for workspace not found error
type WorkspaceNotFoundError struct {
	message string
}

// NewWorkspaceNotFoundError creates WorkspaceNotFoundError struct
func NewWorkspaceNotFoundError(message string) *WorkspaceNotFoundError {
	return &WorkspaceNotFoundError{
		message: message,
	}
}

// NewWorkspaceNotFoundErrorf creates WorkspaceNotFoundError struct
func NewWorkspaceNotFoundErrorf(format string, v ...interface{}) *WorkspaceNotFoundError {
	return &WorkspaceNotFoundError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *WorkspaceNotFoundError) Error() string {
	return e.message
}

// IsWorkspaceNotFoundError evaluates if the given error is WorkspaceNotFoundError
func IsWorkspaceNotFoundError(err error) bool {
	if _, ok := err.(*WorkspaceNotFoundError); ok {
		return true
	}

	return false
}

// WorkspaceNotAuthorizedError is a struct for unauthorized access error
type WorkspaceNotAuthorizedError struct {
	message string
}

// NewWorkspaceNotAuthorizedError creates WorkspaceNotAuthorizedError struct
func NewWorkspaceNotAuthorizedError(message string) *WorkspaceNotAuthorizedError {
	return &WorkspaceNotAuthorizedError{
		message: message,
	}
}

// NewWorkspaceNotAuthorizedErrorf creates WorkspaceNotAuthorizedError struct
func NewWorkspaceNotAuthorizedErrorf(format string, v ...interface{}) *WorkspaceNotAuthorizedError {
	return &WorkspaceNotAuthorizedError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *WorkspaceNotAuthorizedError) Error() string {
	return e.message
}

// IsWorkspaceNotAuthorizedError evaluates if the given error is WorkspaceNotAuthorizedError
func IsWorkspaceNotAuthorizedError(err error) bool {
	if _, ok := err.(*WorkspaceNotAuthorizedError); ok {
		return true
	}

	return false
}

// WorkspaceConflictError is a struct for conflict error
type WorkspaceConflictError struct {
	message string
}

// NewWorkspaceConflictError creates WorkspaceConflictError struct
func NewWorkspaceConflictError(message string) *WorkspaceConflictError {
	return &WorkspaceConflictError{
		message: message,
	}
}

// NewWorkspaceConflictErrorf creates WorkspaceConflictError struct
func NewWorkspaceConflictErrorf(format string, v ...interface{}) *WorkspaceConflictError {
	return &WorkspaceConflictError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *WorkspaceConflictError) Error() string {
	return e.message
}

// IsWorkspaceConflictError evaluates if the given error is WorkspaceConflictError
func IsWorkspaceConflictError(err error) bool {
	if _, ok := err.(*WorkspaceConflictError); ok {
		return true
	}

	return false
}
