package domain

import (
	"fmt"
	"time"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// EventPortImpl implements IncomingEventPort
type EventPortImpl struct {
	Config     *types.Config
	Channel    chan types.WorkspaceChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *EventPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *EventPortImpl) Finalize() {
}

// InitChannel ...
func (impl *EventPortImpl) InitChannel(channel chan types.WorkspaceChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *EventPortImpl) Start() {

}

// Create creates a workspace
func (impl *EventPortImpl) Create(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return fmt.Errorf("input validation error: actor is empty")
	}

	if len(workspace.ID) == 0 {
		workspace.ID = cacao_common_service.NewWorkspaceID()
	}

	if len(workspace.Name) == 0 {
		return fmt.Errorf("input validation error: workspace name is empty")
	}

	now := time.Now().UTC()

	responseChannel := make(chan types.WorkspaceChannelResponse)

	workspaceRequest := types.WorkspaceChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Workspace{
			ID:                workspace.ID,
			Owner:             actor,
			Name:              workspace.Name,
			DefaultProviderID: workspace.DefaultProviderID,
			CreatedAt:         now,
			UpdatedAt:         now,
		},
		Operation:     string(cacao_common_service.WorkspaceCreateRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- workspaceRequest

	// receive response
	workspaceResponse := <-responseChannel
	return workspaceResponse.Error
}

// Update updates the workspace
func (impl *EventPortImpl) Update(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return fmt.Errorf("input validation error: actor is empty")
	}

	if len(workspace.ID) == 0 {
		return fmt.Errorf("input validation error: workspace id is empty")
	}

	now := time.Now().UTC()

	responseChannel := make(chan types.WorkspaceChannelResponse)

	workspaceRequest := types.WorkspaceChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Workspace{
			ID:                workspace.ID,
			Owner:             actor,
			Name:              workspace.Name,
			DefaultProviderID: workspace.DefaultProviderID,
			UpdatedAt:         now,
		},
		Operation:        string(cacao_common_service.WorkspaceUpdateRequestedEvent),
		UpdateFieldNames: append(updateFieldNames, "updated_at"),
		TransactionID:    transactionID,
		Response:         responseChannel,
	}

	// request to model
	impl.Channel <- workspaceRequest

	// receive response
	workspaceResponse := <-responseChannel
	return workspaceResponse.Error
}

// Delete deletes the workspace
func (impl *EventPortImpl) Delete(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return fmt.Errorf("input validation error: actor is empty")
	}

	if len(workspace.ID) == 0 {
		return fmt.Errorf("input validation error: workspace id is empty")
	}

	responseChannel := make(chan types.WorkspaceChannelResponse)

	workspaceRequest := types.WorkspaceChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Workspace{
			ID: workspace.ID,
		},
		Operation:     string(cacao_common_service.WorkspaceDeleteRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- workspaceRequest

	// receive response
	workspaceResponse := <-responseChannel
	return workspaceResponse.Error
}
