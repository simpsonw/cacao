package domain

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// ListWorkspaces returns workspaces owned by a user
func (d *Domain) ListWorkspaces(actor string, emulator string) ([]types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "Domain.ListWorkspaces",
	})

	workspaces, err := d.Storage.List(actor)
	if err != nil {
		logger.WithError(err).Errorf("failed to list workspaces for the owner %s", actor)
		return nil, fmt.Errorf("failed to list workspaces for the owner %s", actor)
	}

	return workspaces, nil
}

// GetWorkspace returns the workspace with the ID
func (d *Domain) GetWorkspace(actor string, emulator string, workspaceID cacao_common.ID) (types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "Domain.GetWorkspace",
	})

	workspace, err := d.Storage.Get(actor, workspaceID)
	if err != nil {
		if types.IsWorkspaceNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the workspace for id %s", workspaceID)
			return types.Workspace{}, types.NewWorkspaceNotFoundErrorf("unable to find the workspace for id %s", workspaceID)
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the workspace for id %s is not authorized", workspaceID)
			return types.Workspace{}, types.NewWorkspaceNotAuthorizedErrorf("access to the workspace for id %s is not authorized", workspaceID)
		} else {
			logger.WithError(err).Errorf("failed to get the workspace for id %s", workspaceID)
			return types.Workspace{}, fmt.Errorf("failed to get the workspace for id %s", workspaceID)
		}
	}

	return workspace, nil
}

// CreateWorkspace creates a workspace
func (d *Domain) CreateWorkspace(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.CreateWorkspace",
	})

	err := d.Storage.Create(workspace)
	if err != nil {
		createFailedEvent := types.Workspace{
			ID:    workspace.ID,
			Owner: actor,
			Name:  workspace.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsWorkspaceConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		}

		err2 := d.EventOut.CreateFailed(actor, emulator, createFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.WithError(err).Errorf("failed to create a workspace for id %s", workspace.ID)
		return fmt.Errorf("failed to create a workspace for id %s", workspace.ID)
	}

	// output event
	createdEvent := types.Workspace{
		ID:        workspace.ID,
		Owner:     actor,
		Name:      workspace.Name,
		CreatedAt: workspace.CreatedAt,
	}

	status := cacao_common.HTTPStatus{
		Message: "created a workspace successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.Created(actor, emulator, createdEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Created event")
	}

	return nil
}

// UpdateWorkspace updates the workspace
func (d *Domain) UpdateWorkspace(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.UpdateWorkspace",
	})

	err := d.Storage.Update(workspace, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.Workspace{
			ID:    workspace.ID,
			Owner: actor,
			Name:  workspace.Name, // may be empty
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsWorkspaceConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		} else if types.IsWorkspaceNotFoundError(err) {
			status.Code = http.StatusNotFound
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.WithError(err).Errorf("failed to update the workspace for id %s", workspace.ID)
		return fmt.Errorf("failed to update the workspace for id %s", workspace.ID)
	}

	// get the final result
	updatedWorkspace, err := d.Storage.Get(actor, workspace.ID)
	if err != nil {
		// update failed somehow - updated but not exist
		logger.WithError(err).Errorf("failed to update the workspace for id %s", workspace.ID)

		updateFailedEvent := types.Workspace{
			ID:    workspace.ID,
			Owner: actor,
			Name:  workspace.Name, // may be empty
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		if types.IsWorkspaceNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the workspace for id %s", workspace.ID)
			return types.NewWorkspaceNotFoundErrorf("unabled to find the workspace for id %s", workspace.ID)
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the workspace for id %s is not authorized", workspace.ID)
			return types.NewWorkspaceNotAuthorizedErrorf("access to the workspace for id %s is not authorized", workspace.ID)
		} else {
			logger.WithError(err).Errorf("failed to update the workspace for id %s", workspace.ID)
			return fmt.Errorf("failed to update the workspace for id %s", workspace.ID)
		}
	}

	// output event
	updatedEvent := types.Workspace{
		ID:        workspace.ID,
		Owner:     actor,
		Name:      updatedWorkspace.Name,
		UpdatedAt: updatedWorkspace.UpdatedAt,
	}

	status := cacao_common.HTTPStatus{
		Message: "updated the workspace successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.Updated(actor, emulator, updatedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Updated event")
	}

	return nil
}

// DeleteWorkspace deletes the workspace
func (d *Domain) DeleteWorkspace(actor string, emulator string, workspaceID cacao_common.ID, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.DeleteWorkspace",
	})

	// get the workspace first
	workspace, err := d.Storage.Get(actor, workspaceID)
	if err != nil {
		// not exist
		logger.WithError(err).Errorf("failed to delete the workspace for id %s", workspaceID)

		deleteFailedEvent := types.Workspace{
			ID:    workspaceID,
			Owner: actor,
			Name:  "", // unknown
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsWorkspaceNotFoundError(err) {
			status.Code = http.StatusNotFound
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		if types.IsWorkspaceNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the workspace for id %s", workspaceID)
			return types.NewWorkspaceNotFoundErrorf("unable to find the workspace for id %s", workspaceID)
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the workspace for id %s is not authorized", workspaceID)
			return types.NewWorkspaceNotAuthorizedErrorf("access to the workspace for id %s is not authorized", workspaceID)
		} else {
			logger.WithError(err).Errorf("failed to delete the workspace for id %s", workspaceID)
			return types.NewWorkspaceNotFoundErrorf("failed to delete the workspace for id %s", workspaceID)
		}
	}

	err = d.Storage.Delete(actor, workspaceID)
	if err != nil {
		deleteFailedEvent := types.Workspace{
			ID:    workspaceID,
			Owner: actor,
			Name:  workspace.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsWorkspaceConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		} else if types.IsWorkspaceNotFoundError(err) {
			status.Code = http.StatusNotFound
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, status, transactionID)
		if err != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.WithError(err).Errorf("failed to delete the workspace for id %s", workspaceID)
		return fmt.Errorf("failed to delete the workspace for id %s", workspaceID)
	}

	// output event
	deletedEvent := types.Workspace{
		ID:    workspaceID,
		Owner: actor,
		Name:  workspace.Name,
	}

	status := cacao_common.HTTPStatus{
		Message: "deleted the workspace successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.Deleted(actor, emulator, deletedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deleted event")
	}

	return nil
}
