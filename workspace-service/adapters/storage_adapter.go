package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config *types.Config
	Store  cacao_common_db.ObjectStore
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// InitMock initialize mongodb adapter with mock_objectstore
func (adapter *MongoAdapter) InitMock(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.InitMock",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.Store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Finalize finalizes mongodb adapter
func (adapter *MongoAdapter) Finalize() {
	err := adapter.Store.Release()
	if err != nil {
		log.Fatal(err)
	}

	adapter.Store = nil
}

// List returns workspaces owned by a user
func (adapter *MongoAdapter) List(user string) ([]types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.Workspace{}

	err := adapter.Store.ListForUser(adapter.Config.WorkspaceMongoDBCollectionName, user, &results)
	if err != nil {
		logger.WithError(err).Errorf("unable to list workspaces for the user %s", user)
		return nil, fmt.Errorf("unable to list workspaces for the user %s", user)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedWorkspaces []types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("ListForUser", adapter.Config.WorkspaceMongoDBCollectionName, user).Return(expectedWorkspaces, expectedError)
	return nil
}

// Get returns the workspace with the ID
func (adapter *MongoAdapter) Get(user string, workspaceID cacao_common.ID) (types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.Workspace{}

	err := adapter.Store.Get(adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String(), &result)
	if err != nil {
		logger.WithError(err).Errorf("unable to get the workspace for id %s", workspaceID)
		return result, types.NewWorkspaceNotFoundErrorf("unable to get the workspace for id %s", workspaceID)
	}

	if result.Owner != user {
		// unauthorized
		logger.Errorf("unauthorized access to the workspace for id %s", workspaceID)
		return result, types.NewWorkspaceNotAuthorizedErrorf("unauthorized access to the workspace for id %s", workspaceID)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, workspaceID cacao_common.ID, expectedWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(expectedWorkspace, expectedError)
	return nil
}

// Create inserts a workspace
func (adapter *MongoAdapter) Create(workspace types.Workspace) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.WorkspaceMongoDBCollectionName, workspace)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			logger.WithError(err).Errorf("unable to insert a workspace because id %s conflicts", workspace.ID)
			return types.NewWorkspaceConflictErrorf("unable to insert a workspace because id %s conflicts", workspace.ID)
		}

		logger.WithError(err).Errorf("unable to insert a workspace with id %s", workspace.ID)
		return fmt.Errorf("unable to insert a workspace with id %s", workspace.ID)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(workspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.WorkspaceMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a workspace
func (adapter *MongoAdapter) Update(workspace types.Workspace, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// get and check ownership
	result := types.Workspace{}

	err := adapter.Store.Get(adapter.Config.WorkspaceMongoDBCollectionName, workspace.ID.String(), &result)
	if err != nil {
		logger.WithError(err).Errorf("unable to get the workspace for id %s", workspace.ID)
		return types.NewWorkspaceNotFoundErrorf("unable to get the workspace for id %s", workspace.ID)
	}

	if result.Owner != workspace.Owner {
		// unauthorized
		logger.Errorf("unauthorized access to the workspace for id %s", workspace.ID)
		return types.NewWorkspaceNotAuthorizedErrorf("unauthorized access to the workspace for id %s", workspace.ID)
	}

	// update
	updated, err := adapter.Store.Update(adapter.Config.WorkspaceMongoDBCollectionName, workspace.ID.String(), workspace, updateFieldNames)
	if err != nil {
		logger.WithError(err).Errorf("unable to update the workspace for id %s", workspace.ID)
		return fmt.Errorf("unable to update the workspace for id %s", workspace.ID)
	}

	if !updated {
		logger.Errorf("unable to update the workspace for id %s", workspace.ID)
		return fmt.Errorf("unable to update the workspace for id %s", workspace.ID)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingWorkspace types.Workspace, newWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.WorkspaceMongoDBCollectionName, newWorkspace.ID.String()).Return(existingWorkspace, expectedError)
	mock.On("Update", adapter.Config.WorkspaceMongoDBCollectionName, newWorkspace.ID.String()).Return(expectedResult, expectedError)
	return nil
}

// Delete deletes a workspace
func (adapter *MongoAdapter) Delete(user string, workspaceID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.Workspace{}

	err := adapter.Store.Get(adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String(), &result)
	if err != nil {
		logger.WithError(err).Errorf("unable to get the workspace for id %s", workspaceID)
		return types.NewWorkspaceNotFoundErrorf("unable to get the workspace for id %s", workspaceID)
	}

	if result.Owner != user {
		// unauthorized
		logger.Errorf("unauthorized access to the workspace for id %s", workspaceID)
		return types.NewWorkspaceNotAuthorizedErrorf("unauthorized access to the workspace for id %s", workspaceID)
	}

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String())
	if err != nil {
		logger.WithError(err).Errorf("unable to delete the workspace for id %s", workspaceID)
		return fmt.Errorf("unable to delete the workspace for id %s", workspaceID)
	}

	if !deleted {
		logger.Errorf("unable to delete the workspace for id %s", workspaceID)
		return types.NewWorkspaceNotFoundErrorf("unable to delete the workspace for id %s", workspaceID)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, workspaceID cacao_common.ID, existingWorkspace types.Workspace, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(existingWorkspace, expectedError)
	mock.On("Delete", adapter.Config.WorkspaceMongoDBCollectionName, workspaceID.String()).Return(expectedResult, expectedError)
	return nil
}
