package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingQueryPort
	// internal
	Connection     cacao_common_messaging.QueryEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *QueryAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.Init",
	})

	logger.Info("initializing QueryAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *QueryAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.Finalize",
	})

	logger.Info("finalizing QueryAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *QueryAdapter) getEventHandlerMapping() []cacao_common_messaging.QueryEventHandlerMapping {
	return []cacao_common_messaging.QueryEventHandlerMapping{
		{
			Subject:      cacao_common_service.WorkspaceListQueryOp,
			EventHandler: adapter.handleWorkspaceListQuery,
		},
		{
			Subject:      cacao_common_service.WorkspaceGetQueryOp,
			EventHandler: adapter.handleWorkspaceGetQuery,
		},
		{
			Subject:      cacao_common.QueryOp(""),
			EventHandler: adapter.handleDefaultQuery,
		},
	}
}

// Start starts the adapter
func (adapter *QueryAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-query" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.ConnectNatsForService(&natsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *QueryAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.StartMock",
	})

	logger.Info("starting QueryAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.CreateMockNatsConnection(&adapter.Config.NatsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *QueryAdapter) handleWorkspaceListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.handleWorkspaceListQuery",
	})

	var listRequest cacao_common_service.Session
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into Session")
		return nil, fmt.Errorf("unable to unmarshal JSON bytes into Session")
	}

	listResult, err := adapter.IncomingPort.List(listRequest.GetSessionActor(), listRequest.SessionEmulator)
	var workspaceListResponse cacao_common.ServiceRequestResult

	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		models := []cacao_common_service.WorkspaceModel{}
		for _, workspace := range listResult {
			model := types.ConvertToModel(session, workspace)
			models = append(models, model)
		}

		workspaceListResponse = cacao_common.ServiceRequestResult{
			Data: models,
			Status: cacao_common.HTTPStatus{
				Message: "listed workspaces successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		logger.Error(err)

		workspaceListResponse = cacao_common.ServiceRequestResult{
			Status: cacao_common.HTTPStatus{
				Message: err.Error(),
				Code:    http.StatusInternalServerError,
			},
		}
	}

	resBytes, err := json.Marshal(workspaceListResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleWorkspaceGetQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.handleWorkspaceGetQuery",
	})

	var getRequest cacao_common_service.WorkspaceModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into WorkspaceModel")
		return nil, fmt.Errorf("unable to unmarshal JSON bytes into WorkspaceModel")
	}

	getResult, err := adapter.IncomingPort.Get(getRequest.GetSessionActor(), getRequest.GetSessionEmulator(), getRequest.GetID())
	var workspaceGetResponse cacao_common.ServiceRequestResult

	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		model := types.ConvertToModel(session, getResult)

		workspaceGetResponse = cacao_common.ServiceRequestResult{
			Data: model,
			Status: cacao_common.HTTPStatus{
				Message: "got a workspace successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		if types.IsWorkspaceNotFoundError(err) {
			workspaceGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusNotFound,
				},
			}
		} else if types.IsWorkspaceNotAuthorizedError(err) {
			workspaceGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusUnauthorized,
				},
			}
		} else {
			logger.Error(err)

			workspaceGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusInternalServerError,
				},
			}
		}
	}

	resBytes, err := json.Marshal(workspaceGetResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleDefaultQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.handleDefaultQuery",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)

	errResponse := cacao_common.ServiceRequestResult{
		Status: cacao_common.HTTPStatus{
			Message: fmt.Sprintf("request %s is not handled", subject),
			Code:    http.StatusNotImplemented,
		},
	}
	resBytes, err := json.Marshal(errResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}
