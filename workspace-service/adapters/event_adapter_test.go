package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

func createTestEventInPort() *ports.MockIncomingEventPort {
	var config types.Config
	config.ProcessDefaults()

	eventInImpl := &ports.MockIncomingEventPort{}
	eventInImpl.Init(&config)

	return eventInImpl
}

func createTestEventAdapter(eventInImpl ports.IncomingEventPort) *EventAdapter {
	var config types.Config
	config.ProcessDefaults()

	eventAdapter := &EventAdapter{}
	eventAdapter.Init(&config)

	eventAdapter.IncomingPort = eventInImpl

	go eventAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return eventAdapter
}

func TestInitEventAdapter(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)
	assert.NotNil(t, eventAdapter)
	assert.NotEmpty(t, eventAdapter.Connection)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestWorkspaceCreateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:                testWorkspaceID,
		Name:              "test_workspace1",
		DefaultProviderID: "test_default_provider1",
	}

	eventInImpl.SetCreateHandler(func(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspace.ID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.WorkspaceCreateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestWorkspaceCreateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:                testWorkspaceID,
		Name:              "test_workspace1",
		DefaultProviderID: "test_default_provider1",
	}

	eventInImpl.SetCreateHandler(func(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspace.ID)
		return fmt.Errorf("unable to create a workspace")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.WorkspaceCreateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestWorkspaceUpdateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:                testWorkspaceID,
		Name:              "test_workspace1",
		DefaultProviderID: "test_default_provider1",
	}

	eventInImpl.SetUpdateHandler(func(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspace.ID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.WorkspaceUpdateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestWorkspaceUpdateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:                testWorkspaceID,
		Name:              "test_workspace1",
		DefaultProviderID: "test_default_provider1",
	}

	eventInImpl.SetUpdateHandler(func(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspace.ID)
		return fmt.Errorf("unable to update a workspace")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.WorkspaceUpdateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestWorkspaceDeleteEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testWorkspaceID,
	}

	eventInImpl.SetDeleteHandler(func(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspace.ID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.WorkspaceDeleteRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestWorkspaceDeleteEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := cacao_common_service.WorkspaceModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testWorkspaceID,
	}

	eventInImpl.SetDeleteHandler(func(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testWorkspaceID, workspace.ID)
		return fmt.Errorf("unable to delete a workspace")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.WorkspaceDeleteRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestWorkspaceCreatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := types.Workspace{
		ID:    testWorkspaceID,
		Owner: testUser,
		Name:  "test_workspace1",
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "workspace is created",
		Code:    http.StatusOK,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.WorkspaceCreatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.WorkspaceCreatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		workspaceJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var workspaceModel cacao_common_service.WorkspaceModel

		err = json.Unmarshal(workspaceJSON, &workspaceModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, workspaceModel.GetSessionActor())
		assert.Equal(t, testUser, workspaceModel.GetSessionEmulator())
		assert.Equal(t, testWorkspaceID, workspaceModel.ID)
		return nil
	})

	err := eventAdapter.Created(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestWorkspaceCreateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := types.Workspace{
		ID:    testWorkspaceID,
		Owner: testUser,
		Name:  "test_workspace1",
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "workspace is not created",
		Code:    http.StatusInternalServerError,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.WorkspaceCreateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.WorkspaceCreateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		workspaceJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var workspaceModel cacao_common_service.WorkspaceModel

		err = json.Unmarshal(workspaceJSON, &workspaceModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, workspaceModel.GetSessionActor())
		assert.Equal(t, testUser, workspaceModel.GetSessionEmulator())
		assert.Equal(t, testWorkspaceID, workspaceModel.ID)
		return nil
	})

	err := eventAdapter.CreateFailed(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestWorkspaceUpdatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := types.Workspace{
		ID:    testWorkspaceID,
		Owner: testUser,
		Name:  "test_workspace1",
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "workspace is updated",
		Code:    http.StatusOK,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.WorkspaceUpdatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.WorkspaceUpdatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		workspaceJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var workspaceModel cacao_common_service.WorkspaceModel

		err = json.Unmarshal(workspaceJSON, &workspaceModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, workspaceModel.GetSessionActor())
		assert.Equal(t, testUser, workspaceModel.GetSessionEmulator())
		assert.Equal(t, testWorkspaceID, workspaceModel.ID)
		return nil
	})

	err := eventAdapter.Updated(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestWorkspaceUpdateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := types.Workspace{
		ID:    testWorkspaceID,
		Owner: testUser,
		Name:  "test_workspace1",
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "workspace is not updated",
		Code:    http.StatusInternalServerError,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.WorkspaceUpdateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.WorkspaceUpdateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		workspaceJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var workspaceModel cacao_common_service.WorkspaceModel

		err = json.Unmarshal(workspaceJSON, &workspaceModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, workspaceModel.GetSessionActor())
		assert.Equal(t, testUser, workspaceModel.GetSessionEmulator())
		assert.Equal(t, testWorkspaceID, workspaceModel.ID)
		return nil
	})

	err := eventAdapter.UpdateFailed(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestWorkspaceDeletedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := types.Workspace{
		ID:    testWorkspaceID,
		Owner: testUser,
		Name:  "test_workspace1",
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "workspace is deleted",
		Code:    http.StatusOK,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.WorkspaceDeletedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.WorkspaceDeletedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		workspaceJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var workspaceModel cacao_common_service.WorkspaceModel

		err = json.Unmarshal(workspaceJSON, &workspaceModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, workspaceModel.GetSessionActor())
		assert.Equal(t, testUser, workspaceModel.GetSessionEmulator())
		assert.Equal(t, testWorkspaceID, workspaceModel.ID)
		return nil
	})

	err := eventAdapter.Deleted(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestWorkspaceDeleteFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testWorkspaceID := cacao_common_service.NewWorkspaceID()

	eventData := types.Workspace{
		ID:    testWorkspaceID,
		Owner: testUser,
		Name:  "test_workspace1",
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "workspace is not deleted",
		Code:    http.StatusInternalServerError,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.WorkspaceDeleteFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.WorkspaceDeleteFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		workspaceJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var workspaceModel cacao_common_service.WorkspaceModel

		err = json.Unmarshal(workspaceJSON, &workspaceModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, workspaceModel.GetSessionActor())
		assert.Equal(t, testUser, workspaceModel.GetSessionEmulator())
		assert.Equal(t, testWorkspaceID, workspaceModel.ID)
		return nil
	})

	err := eventAdapter.DeleteFailed(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}
