package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

// UpdateWorkspaceCommand ...
type UpdateWorkspaceCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateWorkspaceCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "new name of cluster")
	defaultProviderID := flagSet.String("default_provider_id", "", "default provider id")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  defaultProviderID: '%s'\n", *filename, *name, *defaultProviderID)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) != 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	workspaceID := args[0]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		data = c.override(data, name, defaultProviderID)
		c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
	} else {
		dataString := ""
		if len(*name) > 0 {
			dataString += fmt.Sprintf(`, "name": "%s"`, *name)
		}
		if len(*defaultProviderID) > 0 {
			dataString += fmt.Sprintf(`, "default_provider_id": "%s"`, *defaultProviderID)
		}

		if len(dataString) > 0 {
			dataString = "{" + dataString[2:] + "}"
		}

		data = []byte(dataString)
		c.PrintDebug("Constructed request body:\n%s\n", string(data))
	}

	req := c.NewRequest("PUT", "/workspaces/"+workspaceID, "")

	// Only fill request body if it is not empty
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateWorkspaceCommand) Synopsis() string {
	return "update a Workflow Definition"
}

// Help ...
func (c *UpdateWorkspaceCommand) Help() string {
	helpText := `
	Usage: cacao update workspace [options] [ID]

	Options:
		-f <filename>
			name of JSON file to read info from
		-name <string>
			Name of the workspace
		-default_provider_id <string>
			(optional) default provider id to use if none is specified
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateWorkspaceCommand) override(data []byte, name *string, defaultProviderID *string) []byte {
	var workspaceJSON struct {
		Name              string `json:"name,omitempty"`
		DefaultProviderID string `json:"default_provider_id,omitempty"`
	}

	json.Unmarshal(data, &workspaceJSON)

	if len(*name) > 0 {
		workspaceJSON.Name = *name
	}
	if len(*defaultProviderID) > 0 {
		workspaceJSON.DefaultProviderID = *defaultProviderID
	}

	data, err := json.Marshal(workspaceJSON)
	if err != nil {
		panic(err)
	}
	return data
}
