package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// CreateCommand ...
type CreateCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *CreateCommand) Synopsis() string {
	return "create a cluster, secret, user, workspace or Workflow Definition "
}

// Help ...
func (c *CreateCommand) Help() string {
	helpText := `
	Usage: cacao create <subcommand> [options]

`
	return strings.TrimSpace(helpText)
}
