package main

import (
	"os"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

// Config is used to contain configuration information
type Config struct {
	LogLevel string `envconfig:"CACAO_LOG_LEVEL"`
}

func main() {

	// uncomment this line to set the trace level
	// this is a hack until https://cyverse.atlassian.net/browse/CACAO-358
	c := Config{}
	err := envconfig.Process("", &c)
	if err == nil && c.LogLevel != "" {
		l, e := log.ParseLevel(c.LogLevel)
		if e == nil {
			log.SetLevel(l)
		}
	}

	os.Exit(Run(os.Args[1:]))
}
