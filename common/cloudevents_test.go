package common

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateCloudEvent(t *testing.T) {
	cloudEvent, err := CreateCloudEvent([]byte("request"), "testType", "testSource", "testID")
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEvent)
}

func TestGetRequestFromCloudEvent(t *testing.T) {
	cloudEvent, err := CreateCloudEvent([]byte("request"), "testType", "testSource", "testID")
	assert.NoError(t, err)
	assert.NotEmpty(t, cloudEvent)

	req, err := GetRequestFromCloudEvent(cloudEvent)
	assert.NoError(t, err)
	assert.NotEmpty(t, req)
}

func TestGetRequestFromCloudEventError(t *testing.T) {
	_, err := GetRequestFromCloudEvent([]byte("bad cloud event"))
	assert.Error(t, err)
}
