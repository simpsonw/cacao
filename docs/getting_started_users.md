# Getting Started

This document outlines getting started with CACAO as a user. This assumed you have an account and access to a running CACAO server with a connected user cluster.


### Table of Contents
[[_TOC_]]


### Prepare CACAO CLI
The CACAO CLI is the simplest way to interact with the CACAO API.

In order to use the CACAO CLI, you will need to build it
1. Clone or download the repository from GitLab
	```bash
	git clone https://gitlab.com/cyverse/cacao.git
	# or download it without git:
	wget https://gitlab.com/cyverse/cacao/-/archive/master/cacao-master.zip
	unzip cacao.zip
	```
2. Build the `cacao` binary:
	```bash
	cd cacao/cmd
	go build -o cacao
	```
3. Move this to somewhere in your `$PATH`, or add this directory to your path:
	```bash
	sudo mv cacao /usr/local/bin/cacao
	# or
	export PATH=$PATH:$(pwd)
	```


### First Steps

In order to use the CACAO CLI, you need to log in:
```bash
$ cacao login
```

If this is your first time logging in, the CACAO CLI will prompt you for the address off the CACAO API.
```bash
Please provide address of CACAO API.
Format Should be: http://<ip address>:<port>      or    <ip address>:<port>
CACAO API address:
```

To access the production deployment of CACAO, enter `http://ca.cyverse.org`.  For local development environments, enter `localhost`.


### User Actions
Now you have everything you need to interact with your user's resources in CACAO, specifically the Kubernetes cluster info and secrets. By default, you should already have a cluster provided by CyVerse.

```bash
cacao get user ${username}

# list all secrets
cacao get secret ${username}
# get a specific secret
cacao get secret ${username} ${secretID}
# delete a secret
cacao delete secret ${username} ${secretID}
# create a new secret (type must be git, dockerhub, or gcr)
cacao create secret -type ${type} -username ${username} -value ${token_secret}
# create a new secret from JSON file
cacao create secret -f secret.json

# list all clusters
cacao get cluster
# get a specific cluster
cacao get cluster ${clusterID}
# delete a cluster
cacao delete cluster ${clusterID}
# create a new kclsuter
cacao create cluster -config <base64_kubeconfig> -name ${name} -default_namespace ${default_namespace}
# create a new cluster from JSON file
cacao create cluster -f cluster.json

# use this to get your Cluster ID if you have 1 cluster
export clusterID=$(cacao get cluster | jq -r 'keys'[0])
```

### Create a WorkflowDefinition

First, check out [this document](workflowdefinition/README.md) for more information on creating your `wfd.yaml` file.

Now, you are ready to create your WorkflowDefinition in CACAO:
```bash
cacao create wfd -url ${git_url} -branch ${git_branch}
# Note: -branch can be excluded to use default 'master'

# use this to create a WorkflowDefinition and save it's ID
export wfdID=$(cacao create wfd -url ${git_url} -branch ${git_branch} | jq -r .id)
```

A few examples of WorkflowDefinition repos to try:
- https://gitlab.com/cyverse/cacao-helloworld
- https://gitlab.com/cyverse/cacao-simple-http-counter
- https://gitlab.com/cyverse/cacao-postgres
- https://gitlab.com/cyverse/cacao-helloworld-argo

Now that you have a WorkflowDefinition, you can use the following commands:
```bash
# list all of your WorkflowDefinitions
cacao get wfd
# get information about a specific WorkflowDefinition
cacao get wfd ${wfdID}
# delete a WorkflowDefinition
cacao delete wfd ${wfdID}
```


### Build

First, you will need to create a Docker Hub or Google Container Registry secret for pushing your built image. Follow [this guide](https://docs.docker.com/docker-hub/access-tokens/) to create an access token for Docker Hub, then use the commands above to create the secret, making sure to save the ID (you can find it again by listing all secrets: `cacao get secret ${username}`).

Use the CACAO CLI to get the IDs for your cluster and container registry secret (see section above titled ["User Actions"](#user-actions)). Then, use the following command to initiate a build:

```bash
cacao build -registry-secret ${secretID} -cluster ${clusterID} ${wfdID}
```
  - You can also specify a `-git-secret` for private repositories or `-namespace` to choose the namespace to build in


### Run

Once again, use the CACAO CLI to get the ID for your cluster and then use the following command:

```bash
cacao run -cluster ${clusterID} ${wfdID}
# Use this to start a run and save its ID
export runID=$(cacao run -cluster ${clusterID} ${wfdID} | jq -r .id)
```
  - You can also use `-namespace` to choose the namespace to run in
  - `-disable-auth` can be used to disable the auth requirement for accessing the running workflow. This is recommended if you intend to access the workflow in a web browser

You can now use the Run ID for additional commands:
```bash
cacao get run
cacao get run ${runID}
cacao delete run ${runID}
```

You will be able to access this run at:
```bash
https://username.ca-run.cyverse.org/runID:port
```

Due to SSL issues, you currently have to use the `-k` or `--insecure` flag with `curl` to hit your endpoint.  You also need to ensure that you have an `Authorization` header set to access the endpoint.

Putting it all together, your `curl` command should look like:

```
curl -k -H "Authorization: $CACAO_TOKEN" https://username.ca-run.cyverse.org/runID:port
```
