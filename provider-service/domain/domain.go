package domain

import (
	"context"
	"fmt"
	"sync"

	"github.com/hashicorp/go-multierror"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/provider"
	"gitlab.com/cyverse/cacao/provider-service/adapters"
	"gitlab.com/cyverse/cacao/provider-service/ports"
	"gitlab.com/cyverse/cacao/provider-service/types"
)

// Domain define the functions that a Domain type must support.
type Domain interface {
	Init(c types.Configuration)
	Start()
}

// BaseDomain is an embeddable struct that can be used in situations
// where multiple domain types are needed.
type BaseDomain struct {
	config    *types.Configuration
	QueryIn   ports.IncomingQueryPort
	OpenStack *adapters.OpenStackAdapter
}

// OpenStackDomain is a domain that lists images from OpenStack and writes the responses out
type OpenStackDomain struct {
	BaseDomain
}

// NewOpenStackDomain returns a new *ImageLister. Init is called
// on it as part of this function.
func NewOpenStackDomain(c *types.Configuration, queryIn ports.IncomingQueryPort, openstack *adapters.OpenStackAdapter) *OpenStackDomain {
	retval := &OpenStackDomain{
		BaseDomain{
			config:    c,
			QueryIn:   queryIn,
			OpenStack: openstack,
		},
	}
	retval.Init()
	return retval

}

// Init initializes an ImageLister based on the configuration.
func (i *OpenStackDomain) Init() {
	log.Trace("ImageLister.Init()")
}

// Start fires up the domain object, allowing it to listen for incoming queries.
func (i *OpenStackDomain) Start() {
	var wg sync.WaitGroup

	incomingQueryChan := make(chan types.CloudEventRequest, i.config.DefaultChannelBufferSize)
	i.QueryIn.InitChannel(incomingQueryChan)
	go i.QueryIn.Start()

	wg.Add(1)
	go i.processQueryWorker(incomingQueryChan, wg)

	wg.Wait()
}

func doReply(ctx context.Context, cloudEventReq *types.CloudEventRequest, obj interface{}, err error) {
	transactionIDVal := ctx.Value(types.TransactionIDKey)
	if transactionIDVal == nil {
		transactionIDVal = interface{}("none")
	}
	transactionID := transactionIDVal.(string)
	if err != nil {
		log.Error(err)

		if err = cloudEventReq.Replyer.Reply(provider.OpenStackProviderError{
			TransactionID: transactionID,
			Error:         err.Error(),
		}); err != nil {
			log.Error(err)
		}
	} else {
		if err = cloudEventReq.Replyer.Reply(obj); err != nil {
			log.Error(err)
		}
	}
}

// processQueryWorker performs the listing and writes out the response on the ResponsePort.
func (i *OpenStackDomain) processQueryWorker(cloudEventReqChan chan types.CloudEventRequest, wg sync.WaitGroup) {
	defer wg.Done()

	origin := context.Background()

	for cloudEventReq := range cloudEventReqChan {
		listingRequest, err := adapters.ParseOpenStackRequest(&cloudEventReq.CloudEvent)
		if err != nil {
			doReply(origin, &cloudEventReq, nil, err)
			continue
		}

		ctx := context.WithValue(origin, types.UsernameKey, listingRequest.User)
		ctx = context.WithValue(ctx, types.CredentialsIDKey, listingRequest.Credentials.ID)
		ctx = context.WithValue(ctx, types.TransactionIDKey, listingRequest.TransactionID)
		ctx = context.WithValue(ctx, types.EnvironmentKey, listingRequest.Credentials.OpenStackEnv)

		go func(ctx context.Context, listingRequest *provider.ListingRequest, cloudEventReq *types.CloudEventRequest) {
			switch listingRequest.Op {
			case provider.ImagesGetOp:
				image, err := i.OpenStack.GetImage(ctx, listingRequest.Args.(string))
				doReply(ctx, cloudEventReq, image, err)
				break

			case provider.ImagesListOp:
				images, err := i.OpenStack.ListImages(ctx)
				doReply(ctx, cloudEventReq, images, err)
				break

			case provider.FlavorsGetOp:
				flavor, err := i.OpenStack.GetFlavor(ctx, listingRequest.Args.(string))
				doReply(ctx, cloudEventReq, flavor, err)
				break

			case provider.FlavorsListOp:
				flavors, err := i.OpenStack.ListFlavors(ctx)
				doReply(ctx, cloudEventReq, flavors, err)
				break

			// Force the cache to get filled in response to the cache.populate operation.
			// Remember, the user is set in the environment variables passed along in the
			// credentials, so the username will not appear in the args here.
			case provider.CachePopulateOp:
				var errors error
				_, err := i.OpenStack.ListFlavorsForceable(ctx, true)
				if err != nil {
					errors = multierror.Append(errors, err)
				}
				_, err = i.OpenStack.ListImagesForceable(ctx, true)
				if err != nil {
					errors = multierror.Append(errors, err)
				}
				doReply(ctx, cloudEventReq, provider.CachePopulateOp, errors)
				break

			default:
				doReply(ctx, cloudEventReq, listingRequest.Op, fmt.Errorf("unknown operation: %s", listingRequest.Op))
			}
		}(ctx, listingRequest, &cloudEventReq)

	}
}
