# provider-service

A microservice that contacts registered providers and provides access to
resources contained within them.

Currently contains the following operations on OpenStack-related resources:

- images.get - Get the details for a single image, denoted by its UUID within
  the OpenStack cluster.
- images.list - List available images in the OpenStack cluster.
- flavors.get - Get the details for a single flavor, denoted by its UUID within
  the OpenStack cluster.
- flavors.list - List the available flavors in the OpenStack cluster.

## Building

To build this microservice, you'll need to have a reasonably up-to-date version
of Go installed. Then run the following in the top-level directory containing
the service code:

```
go build
```

The service should build with that single step. If that is not the case, please
file an issue.

This service uses Go modules.

## Configuration

The service is configured through a set of environment variables. Here are the
currently supported variables and their defaults:

- `NATS_URL` - The connection string to the NATS cluster. Defaults to
  `nats://nats:4222`.
- `NATS_CLIENT_ID` - Required. Has no default.
- `NATS_QGROUP` - Required. Has no default.
- `NATS_CLUSTER_ID` - Defaults to `cacao-cluster`.
- `NATS_DURABLE_NAME` - Required. Has no default.
- `NATS_EVENTS_SUBJECT` - Defaults to `cacao.events`.
- `NATS_OPENSTACK_SUBJECT` - Defaults to `cacao.providers.openstack.>`.
- `DEFAULT_CHANNEL_BUFFER_SIZE` - Defaults to `1`.
- `REDIS_ADDRESS` - Defaults to `redis:6379`.
- `REDIS_PASSWORD` - Defaults to an empty string.
- `REDIS_DB` - Defaults to `0` and should be parseable as an integer.

## Examples

It's recommended to use the nats-cli provided at
https://github.com/nats-io/natscli. The commands below section assume that the
nats-cli commands are installed.

During development, I normally have a nats-server running locally in debug mode.
I fire it up like this:

```
nats-server --debug
```

In another terminal, I fire up a subscription to the reply subject that I will
set on the messages I send to the service:

```
nats sub cacao.replies
```

In yet another terminal, I start up a build of the service:

```
NATS_URL=nats://127.0.0.1:4222 NATS_CLIENT_ID=test NATS_QGROUP=test NATS_DURABLE_NAME=test ./provider-service
```

Finally, I have a short Go script that I use to send messages to the service
over NATS:

```
NATS_URL=nats://localhost:4222 NATS_SUBJECT=cacao.providers.openstack.images.list NATS_REPLY=cacao.replies go run scripts/sendmsg.go
```
