package main

import (
	"log"
	"strconv"
	"time"

	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
	"github.com/go-redis/redis/v8"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/cacao/provider-service/adapters"
	"gitlab.com/cyverse/cacao/provider-service/domain"
	"gitlab.com/cyverse/cacao/provider-service/types"
)

func main() {
	var (
		err    error
		config *types.Configuration
	)

	config = &types.Configuration{}

	if err = envconfig.Process("", config); err != nil {
		log.Fatal(err.Error())
	}

	if err = config.Validate(); err != nil {
		log.Fatal(err)
	}

	if err != nil {
		log.Fatal(err)
	}

	ttl, err := time.ParseDuration("5m")
	if err != nil {
		log.Fatal(err)
	}

	dbNum, err := strconv.ParseInt(config.RedisDB, 10, 32)
	if err != nil {
		log.Fatal(err)
	}

	redisStore := &ProviderRedisStore{
		store: store.NewRedis(redis.NewClient(&redis.Options{
			Addr:     config.RedisAddress,
			Password: config.RedisPassword,
			DB:       int(dbNum),
		}), &store.Options{
			Expiration: ttl,
		}),
	}

	// We'll use a chain here since we might want to add multiple caching tiers later.
	cacheMgr := cache.NewChain(
		cache.New(redisStore),
	)

	osAdapter := adapters.NewOpenStackAdapter(ttl, cacheMgr)

	osQueryAdapter, err := adapters.NewQueryAdapter(*config, config.NatsOpenStackSubject)
	if err != nil {
		log.Fatal(err)
	}

	imageLister := domain.NewOpenStackDomain(config, osQueryAdapter, osAdapter)
	imageLister.Start()
}
