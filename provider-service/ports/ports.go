package ports

import "gitlab.com/cyverse/cacao/provider-service/types"

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(config types.Configuration)
}

// AsyncPort is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type AsyncPort interface {
	Port
	Start()
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fulfill the purposes of both income and outoing query ports
type IncomingQueryPort interface {
	AsyncPort
	InitChannel(data chan types.CloudEventRequest)
}
