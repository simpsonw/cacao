package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/service/provider"
)

func main() {
	natsURL := os.Getenv("NATS_URL")
	reply := os.Getenv("NATS_REPLY")
	subject := os.Getenv("NATS_SUBJECT")

	data := map[string]interface{}{
		"user": "wregglej",
		"op":   "images.list",
		"credentials": provider.Credentials{
			ID: "test-credential-id",
			OpenStackEnv: provider.Environment{
				"OS_AUTH_URL":             "<replace me>",
				"OS_PROJECT_ID":           "<replace me>",
				"OS_PROJECT_NAME":         "<replace me>",
				"OS_PROJECT_DOMAIN_ID":    "<replace me>",
				"OS_USER_DOMAIN_NAME":     "<replace me>",
				"OS_USERNAME":             "<replace me>",
				"OS_PASSWORD":             "<replace me>",
				"OS_REGION_NAME":          "<replace me>",
				"OS_INTERFACE":            "<replace me>",
				"OS_IDENTITY_API_VERSION": "3",
			},
		},
		"args": provider.ImageListingArgs{
			Public: true,
			SortOpts: []provider.ImageListingSortOption{
				{
					Key:       "name",
					Direction: "asc",
				},
			},
		},
	}

	encodedData, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}

	id := xid.New().String()
	now := time.Now().Format(time.RFC3339)

	ce := map[string]string{
		"id":          id,
		"type":        "type",
		"time":        now,
		"source":      "source",
		"specversion": "1.0",
		"data":        string(encodedData),
	}
	encodedEvent, err := json.Marshal(ce)
	if err != nil {
		log.Fatal(err)
	}

	nc, err := nats.Connect(natsURL)
	if err != nil {
		log.Fatal(err)
	}

	msg := nats.Msg{
		Subject: subject,
		Reply:   reply,
		Data:    encodedEvent,
	}

	fmt.Println(natsURL)
	fmt.Println(subject)
	fmt.Println(reply)
	fmt.Println(string(msg.Data))

	if err = nc.PublishMsg(&msg); err != nil {
		log.Fatal(err)
	}

	nc.Close()
}
