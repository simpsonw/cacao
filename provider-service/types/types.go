package types

import (
	"context"
	"errors"

	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.com/cyverse/cacao-common/service/provider"
)

// CachePrefixListFlavors is the prefix on the cache key for the results of a
// user requesting a list of flavors.
const CachePrefixListFlavors = "ListFlavors"

// CachePrefixGetFlavor is the prefix on the cache key for the results of a
// user requesting a single flavor.
const CachePrefixGetFlavor = "GetFlavor"

// CachePrefixListImages is the prefix on the cache key for the results of a
// user requesting a list of images.
const CachePrefixListImages = "ListImages"

// CachePrefixGetImage is the rpefix on the cache key for the results of a
// user requesting a single image.
const CachePrefixGetImage = "GetImage"

// Configuration contains the settings needed to configure this microservice.
type Configuration struct {
	NatsURL      string `envconfig:"NATS_URL" default:"nats://nats:4222"` // The URL string to use for the NATS connection.
	NatsClientID string `envconfig:"NATS_CLIENT_ID"`
	NatsQGroup   string `envconfig:"NATS_QGROUP"`

	NatsClusterID            string `envconfig:"NATS_CLUSTER_ID" default:"cacao-cluster"`
	NatsDurableName          string `envconfig:"NATS_DURABLE_NAME"`
	NatsEventsSubject        string `envconfig:"NATS_EVENTS_SUBJECT" default:"cacao.events"`
	NatsOpenStackSubject     string `envconfig:"NATS_OPENSTACK_SUBJECT" default:"cacao.providers.openstack.>"`
	DefaultChannelBufferSize int    `envconfig:"DEFAULT_CHANNEL_BUFFER_SIZE" default:"1"`

	RedisAddress  string `envconfig:"REDIS_ADDRESS" default:"redis:6379"`
	RedisPassword string `envconfig:"REDIS_PASSWORD" default:""`
	RedisDB       string `envConfig:"REDIS_DB" default:"0"`
}

// Validate returns an error if there's something wrong with the configuration.
func (c *Configuration) Validate() error {
	if c.NatsClientID == "" {
		return errors.New("NATS_CLIENT_ID environment variable must be set")
	}

	if c.NatsQGroup == "" {
		return errors.New("NATS_QGROUP environment variable must be set")
	}

	if c.NatsDurableName == "" {
		return errors.New("NATS_DURABLE_NAME environment variable must be set")
	}

	return nil
}

const (
	// UsernameKey is what you should use as a key for the username in context.Context.
	UsernameKey = iota

	// CredentialsIDKey is what you should use as a key for the credentials ID in context.Context.
	CredentialsIDKey

	// TransactionIDKey is the key for the transaction ID in context.Context.
	TransactionIDKey

	// EnvironmentKey is the key for the environment variables stored in context.Context.
	EnvironmentKey
)

// QueryReply defines a type that can send a response of some sort.
type QueryReply interface {
	Reply(interface{}) error
}

// CloudEventRequest is what gets comes in as a request
type CloudEventRequest struct {
	CloudEvent event.Event
	Replyer    QueryReply
}

// OpenStackGetter represents a function that calls `openstack show` via
// os/exec.CommandContext and returns the output.
type OpenStackGetter func(context.Context, provider.Environment, string) ([]byte, error)
