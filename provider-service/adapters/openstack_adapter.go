package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/cyverse/cacao-common/service/provider"
	"gitlab.com/cyverse/cacao/provider-service/types"

	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
)

// OpenStackAdapter is used to access metadata from OpenStack clusters
type OpenStackAdapter struct {
	cli   OpenStackCLI
	cache cache.CacheInterface
	ttl   time.Duration
	env   provider.Environment
}

// NewOpenStackAdapter creates a new *OpenStackAdapter.
func NewOpenStackAdapter(ttl time.Duration, cacheMgr cache.CacheInterface) *OpenStackAdapter {
	cli := &OpenStackCLIAdapter{}
	retval := &OpenStackAdapter{
		cli:   cli,
		cache: cacheMgr,
		ttl:   ttl,
		env:   provider.Environment(splitEnvironment(os.Environ())),
	}
	return retval
}

func splitEnvironment(env []string) map[string]string {
	retval := map[string]string{}

	for _, setting := range env {
		parts := strings.Split(setting, "=")
		if len(parts) != 2 {
			continue
		}
		retval[parts[0]] = parts[1]
	}

	return retval
}

// GetImage attempts to retrieve a specific OpenStack flavor by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetImage(ctx context.Context, id string) (*provider.Image, error) {
	var (
		image *provider.Image
		err   error
	)

	images, err := o.ListImages(ctx)
	if err != nil {
		return nil, err
	}

	for _, i := range images {
		if i.ID == id {
			image = i
		}
	}

	if image == nil {
		err = fmt.Errorf("image not found: %s", id)
	}

	return image, err
}

// GetFlavor attempts to retrieve a specific OpenStack flavor by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetFlavor(ctx context.Context, id string) (*provider.Flavor, error) {
	var (
		flavor *provider.Flavor
		err    error
	)

	flavors, err := o.ListFlavors(ctx)
	if err != nil {
		return nil, err
	}

	for _, f := range flavors {
		if f.ID == id {
			flavor = f
		}
	}

	if flavor == nil {
		err = fmt.Errorf("flavor not found: %s", id)
	}

	return flavor, err
}

func (o *OpenStackAdapter) getCLI(ctx context.Context, prefix, id string, cmd types.OpenStackGetter) ([]byte, error) {
	var (
		bytes            []byte
		outputString     string
		err              error
		username, credID string
	)
	username = ctx.Value(types.UsernameKey).(string)
	credID = ctx.Value(types.CredentialsIDKey).(string)
	key := o.GetCacheKey(prefix, username, credID)

	value, err := o.cache.Get(key)
	if err != nil {
		bytes, err = cmd(ctx, o.env, id)
		outputString = string(bytes)
		o.cache.Set(key, outputString, &store.Options{
			Cost:       int64(len(bytes)),
			Expiration: o.ttl,
		})
	} else {
		outputString = value.(string)
	}

	return []byte(outputString), err
}

// ListImagesForceable attempts to list all the available flavors in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster. If force is set to true, the cache is reset
// with the results of a new execution of the listing command.
func (o *OpenStackAdapter) ListImagesForceable(ctx context.Context, force bool) ([]*provider.Image, error) {
	var (
		images       []*provider.Image
		imageBytes   []byte
		imagesString string
		username     string
		credID       string
		err          error
	)
	username = ctx.Value(types.UsernameKey).(string)
	credID = ctx.Value(types.CredentialsIDKey).(string)

	cacheKey := o.GetCacheKey(types.CachePrefixListImages, username, credID)
	value, err := o.cache.Get(cacheKey)

	if err != nil || force {
		imageBytes, err = o.cli.ListImages(ctx, o.env)
		imagesString = string(imageBytes)
		o.cache.Set(cacheKey, imagesString, &store.Options{
			Cost:       int64(len(imageBytes)),
			Expiration: o.ttl,
		})
	} else {
		imagesString = value.(string)
	}

	err = json.Unmarshal([]byte(imagesString), &images)

	return images, err
}

// ListImages calls ListImagesForceable with the force option set to false.
// You should use this one, it's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListImages(ctx context.Context) ([]*provider.Image, error) {
	return o.ListImagesForceable(ctx, false)
}

// ListFlavorsForceable attempts to list all the available flavors in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster.
func (o *OpenStackAdapter) ListFlavorsForceable(ctx context.Context, force bool) ([]*provider.Flavor, error) {
	var (
		flavors       []*provider.Flavor
		flavorBytes   []byte
		flavorsString string
		err           error
		username      string
		credID        string
	)

	username = ctx.Value(types.UsernameKey).(string)
	credID = ctx.Value(types.CredentialsIDKey).(string)

	cacheKey := o.GetCacheKey(types.CachePrefixListFlavors, username, credID)
	value, err := o.cache.Get(cacheKey)
	if err != nil || force {
		flavorBytes, err = o.cli.ListFlavors(ctx, o.env)
		flavorsString = string(flavorBytes)
		o.cache.Set(cacheKey, flavorsString, &store.Options{
			Cost:       int64(len(flavorBytes)),
			Expiration: o.ttl,
		})
	} else {
		flavorsString = value.(string)
	}

	err = json.Unmarshal([]byte(flavorsString), &flavors)
	return flavors, err
}

// ListFlavors calls ListFlavorsForceable with the force option set to false.
// If in doubt, call this one. It's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListFlavors(ctx context.Context) ([]*provider.Flavor, error) {
	return o.ListFlavorsForceable(ctx, false)
}

// GetCacheKey accepts a prefix and generates a key that can be used to retrieve
// a cached value.  By convention, this prefix should match the function which
// is calling GetCacheKey.
func (o *OpenStackAdapter) GetCacheKey(prefix string, username string, credID string) string {
	return fmt.Sprintf("%s.%s.%s", prefix, username, credID)
}
