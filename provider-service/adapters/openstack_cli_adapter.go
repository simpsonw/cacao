package adapters

import (
	"context"
	"fmt"
	"io/ioutil"
	"os/exec"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/provider"
	"gitlab.com/cyverse/cacao/provider-service/types"
)

// OpenStackCLI is an interface that comprises the necessary operations for
// interacting with the OpenStack CLI
type OpenStackCLI interface {
	GetImage(context.Context, provider.Environment, string) ([]byte, error)
	GetFlavor(context.Context, provider.Environment, string) ([]byte, error)
	ListFlavors(context.Context, provider.Environment) ([]byte, error)
	ListImages(context.Context, provider.Environment) ([]byte, error)
}

// OpenStackCLIAdapter is a concrete implementation of the OpenStackCLI interface
type OpenStackCLIAdapter struct {
}

// GetFlavor retrieves a specifc OpenStack Flavor by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) GetFlavor(ctx context.Context, env provider.Environment, id string) ([]byte, error) {
	return o.show(ctx, env, "flavor", id)
}

// GetImage retrieves a specifc OpenStack Image by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) GetImage(ctx context.Context, env provider.Environment, id string) ([]byte, error) {
	return o.show(ctx, env, "image", id)
}

// show is an unexported helper function used for retrieving a single OpenStack
// resource by ID
func (o OpenStackCLIAdapter) show(ctx context.Context, env provider.Environment, resource, id string) ([]byte, error) {
	parsedArgs := []string{resource, "show", id, "-f", "json"}
	return o.execute(ctx, env, parsedArgs)

}

// ListImages retrieves all the available OpenStack flavors and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) ListImages(ctx context.Context, env provider.Environment) ([]byte, error) {
	return o.execute(ctx, env, []string{"image", "list", "-f", "json", "--long"})
}

// ListFlavors retrieves all the available OpenStack flavors and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) ListFlavors(ctx context.Context, env provider.Environment) ([]byte, error) {
	return o.execute(ctx, env, []string{"flavor", "list", "-f", "json", "--all"})
}

// execute calls the openstack command with the provided context and arguments
// and returns a []byte containing the stdout of the command.
func (o OpenStackCLIAdapter) execute(ctx context.Context, env provider.Environment, args []string) ([]byte, error) {
	var (
		err         error
		outputBytes []byte
		stderrBytes []byte
		ctxEnv      provider.Environment
	)

	// Merge the passed in environment with the process's environment
	// This will pick up the EDITOR env var, which the credentials
	// are unlikely to provide.
	ctxEnv = ctx.Value(types.EnvironmentKey).(provider.Environment)
	for name, val := range ctxEnv {
		env[name] = val
	}

	flattenedEnvironment := flattenEnvironment(env)

	cmd := exec.CommandContext(ctx, "openstack", args...)
	cmd.Env = flattenedEnvironment

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, err
	}

	if err = cmd.Start(); err != nil {
		return nil, err
	}

	if outputBytes, err = ioutil.ReadAll(stdout); err != nil {
		return nil, err
	}
	if stderrBytes, err = ioutil.ReadAll(stderr); err != nil {
		return nil, err
	}

	if err = cmd.Wait(); err != nil {
		log.Info(string(outputBytes))
		log.Info(string(stderrBytes))
		return nil, err
	}

	return outputBytes, err
}

// flattenEnvironment is a helper function that accepts an Enrivonment struct
// and returns a []string with values in the format "<property name>=<value>".
// This makes it possible to set the environment in which a command executes.
func flattenEnvironment(env provider.Environment) []string {
	var flattenedEnv []string
	for k, v := range env {
		flattenedEnv = append(flattenedEnv, fmt.Sprintf("%s=%s", k, v))
	}
	return flattenedEnv
}
