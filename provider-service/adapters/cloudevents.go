package adapters

import (
	"encoding/json"
	"strconv"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/rs/xid"
	"gitlab.com/cyverse/cacao-common/service/provider"
)

// CreateCloudEvent takes a JSON byte slice and uses it as the data for a new
// CloudEvent object Marshaled as a JSON byte slice
func CreateCloudEvent(request interface{}, eventType, source string) (*cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	id := xid.New()
	event.SetID(id.String())
	event.SetType(eventType)
	event.SetTime(time.Now())
	event.SetSource(source)
	err := event.SetData(cloudevents.ApplicationJSON, request)
	if err != nil {
		return nil, err
	}

	return &event, nil
}

// ParseCloudEvent ...
func ParseCloudEvent(msgData []byte) (*cloudevents.Event, error) {
	event := cloudevents.NewEvent()
	err := json.Unmarshal(msgData, &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}

// ParseOpenStackRequest parses the data contained in a cloud event into a
// *types.ImageListingRequest.
func ParseOpenStackRequest(event *event.Event) (*provider.ListingRequest, error) {
	data := &provider.ListingRequest{}
	unquoted, err := strconv.Unquote(string(event.Data()))
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal([]byte(unquoted), data); err != nil {
		return nil, err
	}
	return data, nil
}
