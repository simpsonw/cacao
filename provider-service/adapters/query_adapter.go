package adapters

import (
	"encoding/json"

	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/provider"
	"gitlab.com/cyverse/cacao/provider-service/types"
)

// QueryAdapter implements ports.IncomingQueryPort & ports.OutgoingQueryPort
type QueryAdapter struct {
	queryChan chan types.CloudEventRequest
	clientID  string
	subject   string
	queue     string
	nc        *nats.Conn
	sub       *nats.Subscription
}

// NewQueryAdapter creates a new QueryAdapter
func NewQueryAdapter(conf types.Configuration, subject string) (*QueryAdapter, error) {
	retval := &QueryAdapter{subject: subject}
	err := retval.natsConnect(conf)
	if err != nil {
		return nil, err
	}
	retval.clientID = conf.NatsClientID
	retval.queue = conf.NatsQGroup
	return retval, nil
}

// Init perform initialization on the adapter
func (adapter *QueryAdapter) Init(conf types.Configuration) {
	err := adapter.natsConnect(conf)
	if err != nil {
		log.Fatal(err)
	}

	adapter.clientID = conf.NatsClientID
	adapter.queue = conf.NatsQGroup
}

// natsConnect establish the NATS connection
func (adapter *QueryAdapter) natsConnect(conf types.Configuration) error {
	nc, err := nats.Connect(conf.NatsURL)
	if err != nil {
		return err
	}
	log.Info("NATS connection established")

	adapter.nc = nc

	return nil
}

// InitChannel takes in the data channel for depositing incoming message
func (adapter *QueryAdapter) InitChannel(c chan types.CloudEventRequest) {
	adapter.queryChan = c
}

// Start starts the queue subscriber
func (adapter *QueryAdapter) Start() {
	err := adapter.natsQueueSubscribe()
	if err != nil {
		log.Fatal(err)
	}
}

// natsQueueSubscribe starts the subscriber
func (adapter *QueryAdapter) natsQueueSubscribe() error {
	// subscribe with adapter.natsCallbck()
	sub, err := adapter.nc.QueueSubscribe(adapter.subject, adapter.queue, adapter.natsCallback)
	if err != nil {
		return err
	}
	adapter.sub = sub
	return nil
}

func (adapter QueryAdapter) natsCallback(m *nats.Msg) {
	ce, err := ParseCloudEvent(m.Data)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("unable to get request from CloudEvent")
		return
	}

	log.WithFields(log.Fields{"request": string(ce.Data())}).Trace("received message")
	var request types.CloudEventRequest

	// Set the CloudEvent in the request object
	request.CloudEvent = *ce

	// create reply object to be passed to domain
	request.Replyer = QueryReplyer{adapter: &adapter, msg: m}

	// deposit query into channel
	adapter.queryChan <- request

}

// Send implements ports.OutgoingQueryPort
func (adapter QueryAdapter) Send(queryOp provider.QueryOp, data interface{}) error {
	ce, err := CreateCloudEvent(data, string(queryOp), adapter.clientID)
	if err != nil {
		return nil
	}
	bytes, err := ce.MarshalJSON()
	if err != nil {
		return nil
	}
	adapter.nc.Publish(string(queryOp), bytes)
	return nil
}

// Reply replys a NATS msg with a response
// Alternative, this could also depoisit the response to an internal go channel
func (adapter QueryAdapter) Reply(msg *nats.Msg, response interface{}) error {
	// Need to double encode the response data since that's what was coming in.
	marshalled, err := json.Marshal(response)
	if err != nil {
		return err
	}

	// uses reply subject as event type and uses client id as event source
	ce, err := CreateCloudEvent(string(marshalled), msg.Reply, adapter.clientID)
	if err != nil {
		return err
	}
	dat, err := json.Marshal(ce)
	if err != nil {
		return err
	}
	return msg.Respond(dat)
}

// QueryReplyer implements types.QueryReply and allows the domain to send
// a response.
type QueryReplyer struct {
	msg     *nats.Msg
	adapter *QueryAdapter
}

// Reply send a response
func (r QueryReplyer) Reply(response interface{}) error {
	return r.adapter.Reply(r.msg, response)
}
