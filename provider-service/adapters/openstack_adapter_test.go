package adapters

import (
	"context"
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/cyverse/cacao-common/service/provider"
	"gitlab.com/cyverse/cacao/provider-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/provider-service/types"

	"github.com/dgraph-io/ristretto"
	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
	"github.com/stretchr/testify/assert"
)

var testEnv = provider.Environment{
	"OS_REGION_NAME":                   "RegionOne",
	"OS_PROJECT_DOMAIN_ID":             "eef394bdb97165674e11dca9d8c6934d",
	"OS_INTERFACE":                     "public",
	"OS_AUTH_URL":                      "https://example.com:5000/v3",
	"OS_APPLICATION_CREDENTIAL_SECRET": "YqMyZgBNx5LiC9N0X-GM8RA5kGvv5NEajtAjSqIwFidu2xasTDLXzjE6GIYm8WlFVH0Y5c6mE2CDiSgBFi4Irw",
	"OS_APPLICATION_CREDENTIAL_ID":     "0a00d21d9b188dc4ca823e6440bbe36c",
	"OS_AUTH_TYPE":                     "v3applicationcredential",
	"OS_USER_DOMAIN_NAME":              "abc",
	"OS_IDENTITY_API_VERSION":          "3",
}

var testImages = []byte(`
	[
		{
			"ID": "85effb53-9842-49cc-9c5f-f26b4a2bf08c",
			"Name": "CentOS-6.5-x86_64-bin-DVD1.iso",
			"Disk Format": "iso",
			"Container Format": "bare",
			"Size": 4467982336,
			"Checksum": "83221db52687c7b857e65bfe60787838",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"id": "bff03fee-1886-4e4c-8d18-02f7e2e8ac96",
			"name": "centos-7-x86_64-dvd-1708.iso",
			"disk format": "iso",
			"container format": "bare",
			"size": 4521459712,
			"checksum": "82b4160df8d2a360f0f38432ad7e049b",
			"status": "active",
			"visibility": "public",
			"protected": false,
			"project": "74d53196e919420b8a23b6fd286cab63",
			"tags": []
		},
		{
			"ID": "faca47d6-0dc0-4871-b444-f2114fc88bb0",
			"Name": "CentOS-7-x86_64-DVD-1708.iso",
			"Disk Format": "qcow2",
			"Container Format": "bare",
			"Size": null,
			"Checksum": null,
			"Status": "queued",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "411c9204-a802-4258-a081-71e51b0ffe1c",
			"Name": "CentOS-7-x86_64-GenericCloud-1805.raw",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 8589934592,
			"Checksum": "aa7f819fabfd8c531ab560b30ec4946c",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "598de09d-1547-45ec-999d-05398b2a8b74",
			"Name": "centos7_gpu_capable",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 8589934592,
			"Checksum": "aa7f819fabfd8c531ab560b30ec4946c",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "df7e01cb-0c0e-4fef-8f50-c5cd23d7f914",
			"Name": "cirros-0.3.4",
			"Disk Format": "qcow2",
			"Container Format": "bare",
			"Size": 13287936,
			"Checksum": "ee1eca47dc88f4879d8a229cc70a07c6",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "4c5b5230-2eab-43ae-b0f3-4274bc86c3f7",
			"Name": "coreos_production_openstack_image.img-20181128.raw",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 9116319744,
			"Checksum": "bc74be77c2a54688a25bc25eb5312e91",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "07f53fd0-52ac-4d17-964b-db3ea824b8be",
			"Name": "ubuntu-16.04.raw",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 2361393152,
			"Checksum": "dbd94136555a8fc8b067d78afb216b1e",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": [
			"distro-base"
			]
		},
		{
			"ID": "86a8e915-4627-48b8-8001-8473e349dd3a",
			"Name": "ubuntu-18.04.raw",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 2361393152,
			"Checksum": "2e6dd6f746c7884e3a2961b8c0a90e83",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": [
			"distro-base"
			]
		},
		{
			"ID": "7a914011-d4bd-4f77-8ec0-ef2ca6f67d17",
			"Name": "ubuntu-20.04",
			"Disk Format": "qcow2",
			"Container Format": "bare",
			"Size": 533856256,
			"Checksum": "be096b5b3c1a28f9416deed0253ad3e2",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": [
			"distro-base"
			]
		},
		{
			"ID": "2e365e64-2d21-4617-8b58-13ebba3baf78",
			"Name": "ubuntu-20.04_raw",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 2361393152,
			"Checksum": "e1d6334f722bb6d9d260a6d66fa27d61",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "3929d455-4a6c-4497-833b-019537a4061e",
			"Name": "ubuntu_20_04_gpu_capable",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 2361393152,
			"Checksum": "e1d6334f722bb6d9d260a6d66fa27d61",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "a88bd9ba-d076-4072-ba24-c3c396da7b06",
			"Name": "ubuntu_sahara_spark_latest_xenial",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 3775856640,
			"Checksum": "1f0a8b790112f837c2cfd602a0c4305b",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "8000a130-2c48-4c3a-b783-ad2406737c24",
			"Name": "warning-qcow2-broken-donotuseme-coreos_production_openstack_image.img-20181128",
			"Disk Format": "raw",
			"Container Format": "bare",
			"Size": 933101568,
			"Checksum": "3e5fc3a4d295e29e1c48e7b0b21fc2da",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "d31df26a-502f-4d5e-8ee3-cfe56509f996",
			"Name": "warning-qcow2-donotuseme-centos-6-cloudimage",
			"Disk Format": "qcow2",
			"Container Format": "bare",
			"Size": 807600128,
			"Checksum": "e5053204f5b9ec4990f921c4e663dfd5",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "03e3886b-28b4-44da-b3cd-78bcd3f60bee",
			"Name": "warning-qcow2-donotuseme-centos-7-cloudimage",
			"Disk Format": "qcow2",
			"Container Format": "bare",
			"Size": 877985792,
			"Checksum": "b4548edf0bc476c50c083fb88717d92f",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "5b14e64d-43eb-46ed-b968-007d34961c90",
			"Name": "warning-qcow2-donotuseme-trusty-server-cloudimg-amd64-disk1",
			"Disk Format": "qcow2",
			"Container Format": "bare",
			"Size": 262734336,
			"Checksum": "c449695cd20f51f22bacd4d7f9227a3f",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		},
		{
			"ID": "4419b3ba-42e2-465b-aea5-c7dcc60f8620",
			"Name": "warning-qcow2-donotuseme-ubuntu-16.04",
			"Disk Format": "qcow2",
			"Container Format": "bare",
			"Size": 289603584,
			"Checksum": "2bdfeef469409adb3384a6b17177c36b",
			"Status": "active",
			"Visibility": "public",
			"Protected": false,
			"Project": "74d53196e919420b8a23b6fd286cab63",
			"Tags": []
		}
	]
`)

// Trailing spaces are present since that matches what the OpenStack CLI does
var testFlavors = []byte(`
[
  {
    "Name": "large3", 
    "RAM": 65536, 
    "Ephemeral": 0, 
    "VCPUs": 8, 
    "Is Public": true, 
    "Disk": 0, 
    "ID": "000d1872-c4a7-11eb-8529-0242ac130003"
  }, 
  {
    "Name": "xlarge1", 
    "RAM": 32768, 
    "Ephemeral": 0, 
    "VCPUs": 16, 
    "Is Public": true, 
    "Disk": 0, 
    "ID": "08daa0be-c4a7-11eb-8529-0242ac130003"
  }, 
  {
    "Name": "xxlarge1", 
    "RAM": 65536, 
    "Ephemeral": 0, 
    "VCPUs": 32, 
    "Is Public": true, 
    "Disk": 0, 
    "ID": "12492d6e-c4a7-11eb-8529-0242ac130003"
  }
]
	`)

var testFlavor = []byte(`
  {
    "Name": "large3", 
    "RAM": 65536, 
    "Ephemeral": 0, 
    "VCPUs": 8, 
    "Is Public": true, 
    "Disk": 0, 
    "ID": "000d1872-c4a7-11eb-8529-0242ac130003"
  }
`)

var (
	adapter  OpenStackAdapter
	cli      *mocks.OpenStackCLI
	cacheMgr *cache.Cache
)

func TestMain(m *testing.M) {
	var err error
	cli = new(mocks.OpenStackCLI)
	rCache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,   // Far more than we will probably need.
		MaxCost:     2e9,   // MaxCost of the cache. Assume this is bytes.
		BufferItems: 64,    // The recommended default value, according to the docs.
		Metrics:     false, // This is the default, but make it explicit. Don't want the overhead.
	})
	if err != nil {
		log.Fatalf("Unable to create test cache: %s\n", err)
	}
	rStore := store.NewRistretto(rCache, &store.Options{
		Expiration: 5 * time.Minute,
	})
	cacheMgr = cache.New(rStore)

	adapter = OpenStackAdapter{cli, cacheMgr, 5 * time.Minute, testEnv}
	os.Exit(m.Run())
}

func TestOpentStackCLIListFlavors(t *testing.T) {
	ctx := context.WithValue(context.Background(), types.UsernameKey, "test-username")
	ctx = context.WithValue(ctx, types.CredentialsIDKey, "placeholder")

	cli.On("ListFlavors", ctx, testEnv).Return(testFlavors, nil)
	_, err := adapter.ListFlavors(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
}

func TestOpentStackCLICheckFlavorID(t *testing.T) {
	ctx := context.WithValue(context.Background(), types.UsernameKey, "test-username")
	ctx = context.WithValue(ctx, types.CredentialsIDKey, "placeholder")

	cli.On("ListFlavors", ctx, testEnv).Return(testFlavors, nil)
	flavors, err := adapter.ListFlavors(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	found := false
	for _, flavor := range flavors {
		if flavor.ID == "08daa0be-c4a7-11eb-8529-0242ac130003" {
			found = true
		}
	}

	if !found {
		t.Error("flavor ID 08daa0be-c4a7-11eb-8529-0242ac130003 was not found in results")
	}
}

func TestOpentStackCLICheckFlavorCache(t *testing.T) {
	ctx := context.WithValue(context.Background(), types.UsernameKey, "test-username")
	ctx = context.WithValue(ctx, types.CredentialsIDKey, "placeholder")

	cli.On("ListFlavors", ctx, testEnv).Return(testFlavors, nil)
	_, err := adapter.ListFlavors(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	key := adapter.GetCacheKey("ListFlavors", "test-username", "placeholder")
	assert.NoError(t, err)
	_, err = cacheMgr.Get(key)
	if err != nil {
		t.Error("results were not found in the cache")
	}
}

func TestOpentStackCLIGetFlavor(t *testing.T) {
	ctx := context.WithValue(context.Background(), types.UsernameKey, "test-username")
	ctx = context.WithValue(ctx, types.CredentialsIDKey, "placeholder")
	id := "000d1872-c4a7-11eb-8529-0242ac130003"

	cli.On("ListFlavors", ctx, testEnv).Return(testFlavors, nil)
	flavor, err := adapter.GetFlavor(ctx, id)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Equal(t, id, flavor.ID)
}

func TestOpentStackCLICheckImageID(t *testing.T) {
	ctx := context.WithValue(context.Background(), types.UsernameKey, "test-username")
	ctx = context.WithValue(ctx, types.CredentialsIDKey, "placeholder")

	cli.On("ListImages", ctx, testEnv).Return(testImages, nil)
	images, err := adapter.ListImages(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	found := false
	for _, image := range images {
		if image.ID == "5b14e64d-43eb-46ed-b968-007d34961c90" {
			found = true
		}
	}

	if !found {
		t.Error("image ID 5b14e64d-43eb-46ed-b968-007d34961c90 was not found in results")
	}
}

func TestOpentStackCLICheckImageCache(t *testing.T) {
	ctx := context.WithValue(context.Background(), types.UsernameKey, "test-username")
	ctx = context.WithValue(ctx, types.CredentialsIDKey, "placeholder")

	cli.On("ListImages", ctx, testEnv).Return(testImages, nil)
	_, err := adapter.ListImages(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	key := adapter.GetCacheKey("ListImages", "test-username", "placeholder")
	assert.NoError(t, err)
	_, err = cacheMgr.Get(key)
	if err != nil {
		t.Error("results were not found in the cache")
	}
}
