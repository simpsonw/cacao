// This only exists because the RedisStore in gocache doesn't correctly
// implement the store.StoreInterface. Hopefully we'll be able to nuke
//  it after a future update.
package main

import (
	"context"
	"time"

	"github.com/eko/gocache/store"
)

// ProviderRedisStore fixes an issue in the store.RedisStore compliance
// with the store.StoreInterface.
type ProviderRedisStore struct {
	store *store.RedisStore
}

// Get complies with the store.StoreInterface.
func (m *ProviderRedisStore) Get(key interface{}) (interface{}, error) {
	return m.store.Get(context.Background(), key)
}

// GetWithTTL complies with the store.StoreInterface.
func (m *ProviderRedisStore) GetWithTTL(key interface{}) (interface{}, time.Duration, error) {
	return m.store.GetWithTTL(context.Background(), key)
}

// Set complies with the store.StoreInterface.
func (m *ProviderRedisStore) Set(key interface{}, value interface{}, options *store.Options) error {
	return m.store.Set(context.Background(), key, value, options)
}

// Delete complies with the store.StoreInterface.
func (m *ProviderRedisStore) Delete(key interface{}) error {
	return m.store.Delete(context.Background(), key)
}

// Invalidate complies with the store.StoreInterface.
func (m *ProviderRedisStore) Invalidate(options store.InvalidateOptions) error {
	return m.store.Invalidate(context.Background(), options)
}

// Clear complies with the store.StoreInterface.
func (m *ProviderRedisStore) Clear() error {
	return m.store.Clear(context.Background())
}

// GetType complies with the store.StoreInterface.
func (m *ProviderRedisStore) GetType() string {
	return m.store.GetType()
}
