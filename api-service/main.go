package main

import (
	"context"
	"net/http"
	"os"
	"strconv"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/cacao/api-service/api"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)

	err := envconfig.Process(constants.ConfigVarPrefix, &config.GlobalConfig)
	if err != nil {
		log.Fatal(err.Error())
	}

	level, err := log.ParseLevel(config.GlobalConfig.LogLevel)
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	logger := log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	})

	// initialize the AppContext
	if config.GlobalConfig.AppContext == nil {
		config.GlobalConfig.AppContext = context.Background()
	}

	log.Trace("initializing NATS connections variables")
	api.InitNATSConnectionVars(&config.GlobalConfig)

	err = api.InitAuthenticationDriver(&config.GlobalConfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Panic("unable to initialize authentication driver")
		panic(err)
	}
}

func main() {
	log.Info("Auto create user = " + strconv.FormatBool(config.GlobalConfig.AutoCreateUser))
	log.Info("Log level set to: " + config.GlobalConfig.LogLevel)
	log.Info("Nats URL = " + config.GlobalConfig.Nats.URL)

	// Create the microservice clients.
	deploymentsClient := deployments.New(&config.GlobalConfig.Nats, &config.GlobalConfig.Stan)

	// Router is only used for User things since they won't require authentication
	router := mux.NewRouter().StrictSlash(true)
	api.AuthAPIRouter(router)
	api.DocsAPIRouter(router)

	// Require Admin permission to access
	//api.AdminAPIRouter(router)

	// Subrouter will be used for all paths other than login since it will
	// require headers to authenticate with AuthenticateMiddleware
	subRouter := router.NewRoute().Subrouter()
	subRouter.Use(api.AuthenticationMiddleware)
	api.UserAPIRouter(subRouter)
	api.DeploymentAPIRouter(deploymentsClient, subRouter)
	api.CredentialAPIRouter(subRouter)
	// api.ClusterAPIRouter(subRouter)
	// api.WorkflowDefinitionAPIRouter(subRouter)
	// api.BuildAPIRouter(subRouter)
	// api.AnchoreAPIRouter(subRouter)
	api.WorkspaceAPIRouter(subRouter)
	//Put new routes above this line
	//RunAPIRouter defines a "catchall" endpoint for HTTP scaling
	// api.RunAPIRouter(subRouter)

	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("API Service listening on port 8080")
	http.ListenAndServe(":8080", router)
}
