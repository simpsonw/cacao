package utils

import (
	"net/http"
	"strconv"

	"gitlab.com/cyverse/cacao/api-service/constants"
)

// GetCacaoHeaders returns headers added by Cacao
// Currently, this includes username, emulator, isAdmin
func GetCacaoHeaders(r *http.Request) (actor string, emulator string, isAdmin bool) {
	actor = r.Header.Get(constants.RequestHeaderCacaoUser)
	emulator = r.Header.Get(constants.RequestHeaderCacaoEmulator)
	isAdmin, _ = strconv.ParseBool(r.Header.Get(constants.RequestHeaderCacaoAdmin))
	return
}
