package common

import "fmt"

// ErrNotImplemented is an error indicating that a feature hasn't been implemented yet.
var ErrNotImplemented = fmt.Errorf("not implemented")
