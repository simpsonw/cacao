package deployments

import (
	"fmt"

	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/messaging"
	cc "gitlab.com/cyverse/cacao/api-service/clients/common"
)

// Client is an interface for interacting with the Deployments microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the Deployments microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListDeployments(workspaceID cacaocommon.ID) ([]hm.Deployment, error)
	ValidateDeploymentCreationRequest(creationRequest *hm.Deployment) error
	AddDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error)
	GetDeployment(deploymentID cacaocommon.ID) (*hm.Deployment, error)
	ValidateDeploymentParameterUpdateRequest(deploymentID cacaocommon.ID, updateRequest *hm.Parameters) error
	UpdateDeploymentParameters(deploymentID cacaocommon.ID, updateRequest *hm.Parameters) (cacaocommon.ID, error)
	ValidateDeploymentDeletionRequest(deploymentID cacaocommon.ID) error
	DeleteDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, error)
	ListDeploymentBuilds(deploymentID cacaocommon.ID) ([]hm.DeploymentBuild, error)
	ValidateDeploymentRebuildRequest(deploymentID cacaocommon.ID, params *hm.Parameters) error
	RebuildDeployment(deploymentID cacaocommon.ID, params *hm.Parameters) (cacaocommon.ID, error)
	GetDeploymentBuild(deploymentID, buildID cacaocommon.ID) (*hm.DeploymentBuild, error)
	ListDeploymentRuns(deploymentID cacaocommon.ID) ([]hm.DeploymentRun, error)
	ValidateDeploymentRunRequest(deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun) error
	RunDeployment(deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun) (cacaocommon.ID, error)
	GetDeploymentRun(deploymentID, runID cacaocommon.ID) (*hm.DeploymentRun, error)
}

// deploymentsClient is the primary Client implementation.
type deploymentsClient struct {
	natsConfig *messaging.NatsConfig
	stanConfig *messaging.StanConfig
}

// New creates a new Deployments microservice client.
func New(natsConfig *messaging.NatsConfig, stanConfig *messaging.StanConfig) Client {
	return &deploymentsClient{
		natsConfig: natsConfig,
		stanConfig: stanConfig,
	}
}

// Session returns a new Deployments microservice client session.
func (c *deploymentsClient) Session(actor, emulator string, isAdmin bool) (Session, error) {

	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, fmt.Errorf("no actor specified")
	}

	// Define and return the session.
	session := deploymentsSession{
		natsConfig: c.natsConfig,
		stanConfig: c.stanConfig,
		actor:      actor,
		emulator:   emulator,
		isAdmin:    isAdmin,
	}
	return &session, nil
}

// deploymentsSession is the primary DeploymentsSession implementation.
type deploymentsSession struct {
	natsConfig *messaging.NatsConfig
	stanConfig *messaging.StanConfig
	actor      string
	emulator   string
	isAdmin    bool
}

// ListDeployments obtains a list of deployments, optionally filtered by workspace ID.
// TODO: implement me
func (s *deploymentsSession) ListDeployments(workspaceID cacaocommon.ID) ([]hm.Deployment, error) {
	return nil, cc.ErrNotImplemented
}

// ValidateDeploymentCreationRequest checks a deployment creation request to ensure that it's valid.
// TODO: implement me
func (s *deploymentsSession) ValidateDeploymentCreationRequest(creationRequest *hm.Deployment) error {
	return cc.ErrNotImplemented
}

// AddDeployment creates a new deployment.
// TODO: ipmplement me
func (s *deploymentsSession) AddDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error) {
	return "", cc.ErrNotImplemented
}

// GetDeployment returns the deployment with the given ID if it exists and the user has permission to view it.
// TODO: implement me
func (s *deploymentsSession) GetDeployment(dpeloymentID cacaocommon.ID) (*hm.Deployment, error) {
	return nil, cc.ErrNotImplemented
}

// ValidateDepoymentParameterUpdateRequest checks a deployment parameter update request to ensure that it's valid.
// TODO: ipmplement me
func (s *deploymentsSession) ValidateDeploymentParameterUpdateRequest(
	deploymentID cacaocommon.ID, updateRequest *hm.Parameters,
) error {
	return cc.ErrNotImplemented
}

// UpdateDeploymentParameters updates parameters for an existing deployment.
// TODO: implement me
func (s *deploymentsSession) UpdateDeploymentParameters(
	deploymentID cacaocommon.ID, updateRequest *hm.Parameters,
) (cacaocommon.ID, error) {
	return "", cc.ErrNotImplemented
}

// ValidateDeploymentDeletionRequest checks a deployment deletion request to ensure that it's valid.
// TODO: implement me
func (s *deploymentsSession) ValidateDeploymentDeletionRequest(deploymentID cacaocommon.ID) error {
	return cc.ErrNotImplemented
}

// DeleteDeployment deletes an existing deployment.
// TODO: implement me
func (s *deploymentsSession) DeleteDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, error) {
	return "", cc.ErrNotImplemented
}

// ListDeploymentBuilds lists the builds for the given deployment.
// TODO: implement me
func (s *deploymentsSession) ListDeploymentBuilds(deploymentID cacaocommon.ID) ([]hm.DeploymentBuild, error) {
	return nil, cc.ErrNotImplemented
}

// ValidateDeploymentRebuildRequest checks a deployment rebuild request to ensure that it's valid.
// TODO: implement me
func (s *deploymentsSession) ValidateDeploymentRebuildRequest(
	deploymentID cacaocommon.ID, params *hm.Parameters,
) error {
	return cc.ErrNotImplemented
}

// RebuildDeployment rebuilds an existing deployment.
// TODO: implement me
func (s *deploymentsSession) RebuildDeployment(
	deploymentID cacaocommon.ID, params *hm.Parameters,
) (cacaocommon.ID, error) {
	return "", cc.ErrNotImplemented
}

// GetDeploymentBuild returns information about a single deployment build.
// TODO: implement me
func (s *deploymentsSession) GetDeploymentBuild(deploymentID, buildID cacaocommon.ID) (*hm.DeploymentBuild, error) {
	return nil, cc.ErrNotImplemented
}

// ListDeploymentRuns lists the runs for the given deployment.
// TODO: implement me
func (s *deploymentsSession) ListDeploymentRuns(deploymentID cacaocommon.ID) ([]hm.DeploymentRun, error) {
	return nil, cc.ErrNotImplemented
}

// ValidateDeploymentRunRequest checks a deployment run request to ensure that it's valid.
// TODO: implement me
func (s *deploymentsSession) ValidateDeploymentRunRequest(
	deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun,
) error {
	return cc.ErrNotImplemented
}

// RunDeployment launches a new deployment run.
// TODO: implement me
func (s *deploymentsSession) RunDeployment(
	deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun,
) (cacaocommon.ID, error) {
	return "", cc.ErrNotImplemented
}

// GetDeploymentRun gets information about an existing deployment run.
// TODO: implement me
func (s *deploymentsSession) GetDeploymentRun(deploymentID, runID cacaocommon.ID) (*hm.DeploymentRun, error) {
	return nil, cc.ErrNotImplemented
}
