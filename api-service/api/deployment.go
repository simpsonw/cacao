package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	cc "gitlab.com/cyverse/cacao/api-service/clients/common"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// deploymentsAPI is a single instance of the deployments API implementation.
type deploymentsAPI struct {
	deploymentsClient deployments.Client
}

// DeploymentAPIRouter creates routes for operations related to deployments.
func DeploymentAPIRouter(deploymentsClient deployments.Client, router *mux.Router) {
	d := &deploymentsAPI{deploymentsClient: deploymentsClient}

	router.HandleFunc("/deployments", d.getDeployments).Methods("GET")
	router.HandleFunc("/deployments", d.addDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_id}", d.getDeploymentByID).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}", d.updateDeploymentParameters).Methods("PATCH")
	router.HandleFunc("/deployments/{deployment_id}", d.deleteDeployment).Methods("DELETE")
	router.HandleFunc("/deployments/{deployment_id}/builds", d.listDeploymentBuilds).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}/builds", d.rebuildDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_id}/builds/{build_id}", d.getBuildInformation).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}/runs", d.listDeploymentRuns).Methods("GET")
	router.HandleFunc("/deployments/{deployment_id}/runs", d.runDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_id}/runs/{run_id}", d.getDeploymentRun).Methods("GET")
}

// unmarshalRequest reads the request body and unmarshals it.
func (d *deploymentsAPI) unmarshalRequest(
	logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{},
) error {

	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := "unable to read request body"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		msg := "unable to decode the request body"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return err
	}

	return nil
}

// getDeploymentsClientSession obtains the deployments client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (d *deploymentsAPI) getDeploymentsClientSession(
	logger *log.Entry, w http.ResponseWriter, r *http.Request,
) deployments.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := d.deploymentsClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the deployments microservice client")
		utils.JSONError(w, r, "unexpected error", err.Error(), http.StatusInternalServerError)
		return nil
	}
	return session
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (d *deploymentsAPI) extractID(
	logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder, idType string,
) (cacaocommon.ID, error) {
	deploymentID := cacaocommon.ID(mux.Vars(r)[placeholder])
	if !deploymentID.Validate() {
		msg := fmt.Sprintf("invalid %s ID: %s", idType, deploymentID)
		logger.Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return cacaocommon.ID(""), fmt.Errorf(msg)
	}
	return deploymentID, nil
}

// notImplemented returns an error indicating that an operation isn't implemented yet.
func (d *deploymentsAPI) notImplemented(logger *log.Entry, w http.ResponseWriter, r *http.Request) {
	logger.Error("not implemented")
	utils.JSONError(w, r, "not implemented", "not implemented", http.StatusNotImplemented)
}

// getDeployments implements the GET /deployments endpoint.
func (d *deploymentsAPI) getDeployments(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getDeployments",
	})
	logger.Info("api.getDeployments(): start")
	var err error

	// The listing may optionally be filtered by workspace ID.
	workspaceID := cacaocommon.ID(r.URL.Query().Get("workspace_id"))
	if workspaceID != "" {
		if !workspaceID.Validate() {
			msg := fmt.Sprintf("invalid workspace ID: %s", workspaceID)
			logger.Error(msg)
			utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
			return
		}
		logger.Debugf("filtering deployment listing by workspace: %s", workspaceID)
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of deployments
	deployments, err := session.ListDeployments(workspaceID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := fmt.Sprintf("error listing deployments: %s", err.Error())
		logger.WithField("error", err).Error("error returned by ListDeployments")
		utils.JSONError(w, r, msg, err.Error(), http.StatusInternalServerError)
		return
	}

	// Guard against ListDeployments returning nil.
	if deployments == nil {
		deployments = make([]hm.Deployment, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, deployments, http.StatusOK)
}

// addDeployment implements the POST /deployments endpoint.
func (d *deploymentsAPI) addDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "addDeployment",
	})
	logger.Info("api.addDeployment(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest hm.Deployment
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the deployments session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the deployment creation request.
	err = session.ValidateDeploymentCreationRequest(&incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment creation request validation failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Submit the creation request.
	tid, err := session.AddDeployment(&incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment creation request submission failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// getDeploymentByID returns the deployment with the specified ID if it exists and the authenticated user may view it.
func (d *deploymentsAPI) getDeploymentByID(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getDeployments",
	})
	logger.Info("api.getDeploymentByID(): start")
	var err error

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the deployment.
	deployment, err := session.GetDeployment(deploymentID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "error obtaining deployment information"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, msg, err.Error(), http.StatusInternalServerError)
		return
	}
	if deployment == nil {
		msg := fmt.Sprintf("deployment not found: %s", deploymentID)
		logger.Error(msg)
		utils.JSONError(w, r, "not found", msg, http.StatusNotFound)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, deployment, http.StatusOK)
}

// updateDeploymentParameters updates the parameters for a deployment.
func (d *deploymentsAPI) updateDeploymentParameters(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateDeploymentParameters",
	})
	logger.Info("api.updateDeploymentParameters(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest hm.Parameters
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the deployment parameter update request.
	err = session.ValidateDeploymentParameterUpdateRequest(deploymentID, &incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment parameter update request validation failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateDeploymentParameters(deploymentID, &incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment parameter update request submission failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) deleteDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteDeployment",
	})
	logger.Info("api.deleteDeployment(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the deployment deletion request.
	err = session.ValidateDeploymentDeletionRequest(deploymentID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment deletion request validation failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Submit the deletion request.
	tid, err := session.DeleteDeployment(deploymentID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment deletion request submission failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(deploymentID.String(), tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) listDeploymentBuilds(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listDeploymentBuilds",
	})
	logger.Info("api.listDeploymentBuilds(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// List the deployment builds.
	builds, err := session.ListDeploymentBuilds(deploymentID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment build listing failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Guard against ListDeploymentBuild returning nil.
	if builds == nil {
		builds = make([]hm.DeploymentBuild, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, builds, http.StatusOK)
}

func (d *deploymentsAPI) rebuildDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "rebuildDeployment",
	})
	logger.Info("api.rebuildDeployment(): start")

	// Extract the deployment ID from the request and validate it.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest hm.Parameters
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the rebuild request.
	err = session.ValidateDeploymentRebuildRequest(deploymentID, &incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment rebuild request validation failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Submit the rebuild request.
	tid, err := session.RebuildDeployment(deploymentID, &incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment rebuild request submission failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(deploymentID.String(), tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) getBuildInformation(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getBuildInformation",
	})
	logger.Info("api.getBuildInformation(): start")

	// Extract and validate the deployment ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Extract and validate the build ID.
	buildID, err := d.extractID(logger, w, r, "build_id", "build")
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the deployment build information.
	build, err := session.GetDeploymentBuild(deploymentID, buildID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment build information retrieval failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, build, http.StatusOK)
}

func (d *deploymentsAPI) listDeploymentRuns(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listDeploymentRuns",
	})
	logger.Info("api.listDeploymentRuns(): start")

	// Extract and validate the deployment ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Create the deployment client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Obtain the list of deployment runs.
	runs, err := session.ListDeploymentRuns(deploymentID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment run listing failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Guard against ListDeploymentRuns returning nil.
	if runs == nil {
		runs = make([]hm.DeploymentRun, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, runs, http.StatusOK)
}

func (d *deploymentsAPI) runDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "runDeployment",
	})
	logger.Info("api.runDeployment(): start")

	// Extract and validate the deployment ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest hm.DeploymentRun
	err = d.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Get the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the deployment run request.
	err = session.ValidateDeploymentRunRequest(deploymentID, &incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment run request validation failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "bad request", msg, http.StatusBadRequest)
		return
	}

	// Submit the request.
	tid, err := session.RunDeployment(deploymentID, &incomingRequest)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment run request submission failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Format and return the response.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

func (d *deploymentsAPI) getDeploymentRun(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getDeploymentRun",
	})
	logger.Info("api.getDeploymentRun(): start")

	// Extract and validate the deployment ID and run ID.
	deploymentID, err := d.extractID(logger, w, r, "deployment_id", "deployment")
	if err != nil {
		return
	}
	runID, err := d.extractID(logger, w, r, "run_id", "run")
	if err != nil {
		return
	}

	// Get the deployments client session.
	session := d.getDeploymentsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the deployment run.
	run, err := session.GetDeploymentRun(deploymentID, runID)
	if err == cc.ErrNotImplemented {
		d.notImplemented(logger, w, r)
		return
	}
	if err != nil {
		msg := "deployment run information retrieval failed"
		logger.WithField("error", err).Error(msg)
		utils.JSONError(w, r, "internal server error", msg, http.StatusInternalServerError)
		return
	}

	// Return the response.
	utils.ReturnStatus(w, run, http.StatusOK)
}
