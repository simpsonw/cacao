package api

import (
	"github.com/gorilla/mux"
)

const (
	workspaceReconnectWait  = 2
	workspaceMaxReconnects  = 60
	workspaceRequestTimeout = 10
)

// TODO: Temporarily comment out the code to apply changes made on cacao-common

// WorkspaceAPIRouter creates routes for workspace-related operations such as creating and
// updating workspaces
func WorkspaceAPIRouter(router *mux.Router) {
	/*
		router.HandleFunc("/workspaces", createWorkspace).Methods("POST")
		router.HandleFunc("/workspaces", listWorkspaces).Methods("GET")
		router.HandleFunc("/workspaces/{workspaceID}", getWorkspace).Methods("GET")
		router.HandleFunc("/workspaces/{workspaceID}", updateWorkspace).Methods("PATCH")
		router.HandleFunc("/workspaces/{workspaceID}", updateWorkspace).Methods("PUT")
		router.HandleFunc("/workspaces/{workspaceID}", deleteWorkspace).Methods("DELETE")
	*/
}

/*
func initNatsConn() (*cacao_common_messaging.NatsConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "initNatsConn",
	})

	natsConfig := cacao_common_messaging.NatsConfig{
		URL:            natsInfo["address"],
		ClientID:       natsInfo["client_id"],
		MaxReconnects:  workspaceMaxReconnects,
		ReconnectWait:  workspaceReconnectWait,
		RequestTimeout: workspaceRequestTimeout,
	}

	natsConn, err := cacao_common_messaging.ConnectNatsForServiceClient(&natsConfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("Unable to connect to NATS")
		return nil, err
	}

	return natsConn, nil
}

func initStanConn() (*cacao_common_messaging.StanConnection, error) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "initStanConn",
	})

	natsConfig := cacao_common_messaging.NatsConfig{
		URL:            natsInfo["address"],
		ClientID:       natsInfo["client_id"],
		MaxReconnects:  workspaceMaxReconnects,
		ReconnectWait:  workspaceReconnectWait,
		RequestTimeout: workspaceRequestTimeout,
	}

	stanConfig := cacao_common_messaging.StanConfig{
		ClusterID: natsInfo["cluster_id"],
	}

	stanConn, err := cacao_common_messaging.ConnectStanForServiceClient(&natsConfig, &stanConfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("Unable to connect to NATS Streaming")
		return nil, err
	}

	return stanConn, nil
}

func initEventSource() (*cacao_common_messaging.EventSource, error) {
	natsConfig := cacao_common_messaging.NatsConfig{
		URL:            natsInfo["address"],
		ClientID:       natsInfo["client_id"] + "_eventsource",
		MaxReconnects:  workspaceMaxReconnects,
		ReconnectWait:  workspaceReconnectWait,
		RequestTimeout: workspaceRequestTimeout,
	}

	stanConfig := cacao_common_messaging.StanConfig{
		ClusterID: natsInfo["cluster_id"],
	}

	return cacao_common_messaging.NewEventSource(natsConfig, stanConfig), nil
}

func isAdmin(r *http.Request) bool {
	return r.Header.Get("X-Cacao-Admin") == "true"
}

func getUserFromRequest(r *http.Request) cacao_common.User {
	user := cacao_common.User{
		Username: r.Header.Get("X-Cacao-User"),
		IsAdmin:  (r.Header.Get("X-Cacao-Admin") == "true"),
		Secrets:  map[string]cacao_common.Secret{},
	}

	r.Header.Del("X-Cacao-User")
	r.Header.Del("X-Cacao-Admin")
	return user
}

func makeRequestBody(r *http.Request, obj interface{}) (int, error) {
	if r.Body != nil {
		var reqBody []byte
		reqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return http.StatusInternalServerError, err
		}

		if len(reqBody) > 0 {
			err = json.Unmarshal(reqBody, obj)
			if err != nil {
				err = fmt.Errorf("Error reading request body: " + err.Error())
				return http.StatusBadRequest, err
			}
		}
	}
	return http.StatusOK, nil
}

func makeWorkspaceListAdminRequest(r *http.Request) (*cacao_common_service.WorkspaceListAdmin, int, error) {
	request := cacao_common_service.WorkspaceListAdmin{
		User: getUserFromRequest(r),
	}

	return &request, http.StatusOK, nil
}

func makeWorkspaceListRequest(r *http.Request) (*cacao_common_service.WorkspaceList, int, error) {
	request := cacao_common_service.WorkspaceList{
		User: getUserFromRequest(r),
	}

	return &request, http.StatusOK, nil
}

func getWorkspaceListAdminRequestRedacted(request *cacao_common_service.WorkspaceListAdmin) cacao_common_service.WorkspaceListAdmin {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func getWorkspaceListRequestRedacted(request *cacao_common_service.WorkspaceList) cacao_common_service.WorkspaceList {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func makeWorkspaceGetAdminRequest(r *http.Request, workspaceID cacao_common.ID) (*cacao_common_service.WorkspaceGetAdmin, int, error) {
	request := cacao_common_service.WorkspaceGetAdmin{}
	status, err := makeRequestBody(r, &request)
	if err != nil {
		return nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	return &request, http.StatusOK, nil
}

func makeWorkspaceGetRequest(r *http.Request, workspaceID cacao_common.ID) (*cacao_common_service.WorkspaceGet, int, error) {
	request := cacao_common_service.WorkspaceGet{}
	status, err := makeRequestBody(r, &request)
	if err != nil {
		return nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	return &request, http.StatusOK, nil
}

func getWorkspaceGetAdminRequestRedacted(request *cacao_common_service.WorkspaceGetAdmin) cacao_common_service.WorkspaceGetAdmin {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func getWorkspaceGetRequestRedacted(request *cacao_common_service.WorkspaceGet) cacao_common_service.WorkspaceGet {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func makeWorkspaceCreateAdminRequest(r *http.Request, workspaceID cacao_common.ID) (*cacao_common_service.WorkspaceCreateAdmin, int, error) {
	request := cacao_common_service.WorkspaceCreateAdmin{}
	status, err := makeRequestBody(r, &request)
	if err != nil {
		return nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	return &request, http.StatusOK, nil
}

func makeWorkspaceCreateRequest(r *http.Request, workspaceID cacao_common.ID) (*cacao_common_service.WorkspaceCreate, int, error) {
	request := cacao_common_service.WorkspaceCreate{}
	status, err := makeRequestBody(r, &request)
	if err != nil {
		return nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	return &request, http.StatusOK, nil
}

func getWorkspaceCreateAdminRequestRedacted(request *cacao_common_service.WorkspaceCreateAdmin) cacao_common_service.WorkspaceCreateAdmin {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func getWorkspaceCreateRequestRedacted(request *cacao_common_service.WorkspaceCreate) cacao_common_service.WorkspaceCreate {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func makeWorkspaceUpdateAdminRequest(r *http.Request, workspaceID cacao_common.ID) (map[string]interface{}, *cacao_common_service.WorkspaceUpdateAdmin, int, error) {
	requestMap := map[string]interface{}{}
	status, err := makeRequestBody(r, &requestMap)
	if err != nil {
		return nil, nil, status, err
	}

	request := cacao_common_service.WorkspaceUpdateAdmin{}
	status, err = makeRequestBody(r, &request)
	if err != nil {
		return nil, nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	requestMap["user"] = request.User
	requestMap["workspace_id"] = workspaceID

	return requestMap, &request, http.StatusOK, nil
}

func makeWorkspaceUpdateRequest(r *http.Request, workspaceID cacao_common.ID) (map[string]interface{}, *cacao_common_service.WorkspaceUpdate, int, error) {
	requestMap := map[string]interface{}{}
	status, err := makeRequestBody(r, &requestMap)
	if err != nil {
		return nil, nil, status, err
	}

	request := cacao_common_service.WorkspaceUpdate{}
	status, err = makeRequestBody(r, &request)
	if err != nil {
		return nil, nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	requestMap["user"] = request.User
	requestMap["workspace_id"] = workspaceID

	return requestMap, &request, http.StatusOK, nil
}

func getWorkspaceUpdateAdminRequestRedacted(request *cacao_common_service.WorkspaceUpdateAdmin) cacao_common_service.WorkspaceUpdateAdmin {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func getWorkspaceUpdateRequestRedacted(request *cacao_common_service.WorkspaceUpdate) cacao_common_service.WorkspaceUpdate {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func makeWorkspaceDeleteAdminRequest(r *http.Request, workspaceID cacao_common.ID) (*cacao_common_service.WorkspaceDeleteAdmin, int, error) {
	request := cacao_common_service.WorkspaceDeleteAdmin{}
	status, err := makeRequestBody(r, &request)
	if err != nil {
		return nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	return &request, http.StatusOK, nil
}

func makeWorkspaceDeleteRequest(r *http.Request, workspaceID cacao_common.ID) (*cacao_common_service.WorkspaceDelete, int, error) {
	request := cacao_common_service.WorkspaceDelete{}
	status, err := makeRequestBody(r, &request)
	if err != nil {
		return nil, status, err
	}

	request.User = getUserFromRequest(r)
	request.WorkspaceID = workspaceID

	return &request, http.StatusOK, nil
}

func getWorkspaceDeleteAdminRequestRedacted(request *cacao_common_service.WorkspaceDeleteAdmin) cacao_common_service.WorkspaceDeleteAdmin {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func getWorkspaceDeleteRequestRedacted(request *cacao_common_service.WorkspaceDelete) cacao_common_service.WorkspaceDelete {
	redactedRequest := *request
	redactedRequest.User = redactedRequest.User.GetRedacted()
	return redactedRequest
}

func listWorkspaces(w http.ResponseWriter, r *http.Request) {
	if isAdmin(r) {
		listWorkspacesAdmin(w, r)
	} else {
		listWorkspacesUser(w, r)
	}
}

func listWorkspacesAdmin(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listWorkspacesAdmin",
	})
	logger.Info("received a request to list workspaces (admin)")

	listRequest, status, err := makeWorkspaceListAdminRequest(r)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceListAdminRequestRedacted(listRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(listRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to list workspaces without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	natsConn, err := initNatsConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer natsConn.Disconnect()

	responseBytes, err := natsConn.Request(cacao_common_service.WorkspaceListAdminQueryOp, listRequest)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to request a list workspaces event")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var response cacao_common_service.WorkspaceListAdminResult
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to object")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if response.Status.Code == http.StatusOK {
		var apiResponse struct {
			Workspaces []cacao_common_service.Workspace `json:"workspaces"`
		}

		workspaceJSON, err := json.Marshal(response.Data)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response data to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = json.Unmarshal(workspaceJSON, &apiResponse.Workspaces)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to a workspace object array")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		apiResponseBytes, err := json.Marshal(apiResponse)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal an api response object to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponseBytes)
	} else {
		// error
		apiResponse := []byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, response.Status.Code, response.Status.Message))

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponse)
	}
}

func listWorkspacesUser(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listWorkspacesUser",
	})
	logger.Info("received a request to list workspaces")

	listRequest, status, err := makeWorkspaceListRequest(r)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceListRequestRedacted(listRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(listRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to list workspaces without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	natsConn, err := initNatsConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer natsConn.Disconnect()

	responseBytes, err := natsConn.Request(cacao_common_service.WorkspaceListQueryOp, listRequest)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to request a list workspaces event")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var response cacao_common_service.WorkspaceListResult
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to object")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if response.Status.Code == http.StatusOK {
		var apiResponse struct {
			Workspaces []cacao_common_service.Workspace `json:"workspaces"`
		}

		workspaceJSON, err := json.Marshal(response.Data)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response data to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = json.Unmarshal(workspaceJSON, &apiResponse.Workspaces)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to a workspace object array")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		apiResponseBytes, err := json.Marshal(apiResponse)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal an api response object to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponseBytes)
	} else {
		// error
		apiResponse := []byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, response.Status.Code, response.Status.Message))

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponse)
	}
}

func getWorkspace(w http.ResponseWriter, r *http.Request) {
	if isAdmin(r) {
		getWorkspaceAdmin(w, r)
	} else {
		getWorkspaceUser(w, r)
	}
}

func getWorkspaceAdmin(w http.ResponseWriter, r *http.Request) {
	workspaceID := mux.Vars(r)["workspaceID"]
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "getWorkspaceAdmin",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to get a workspace (admin)")

	getRequest, status, err := makeWorkspaceGetAdminRequest(r, cacao_common.ID(workspaceID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceGetAdminRequestRedacted(getRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(getRequest.WorkspaceID) == 0 {
		err = fmt.Errorf("'workspace_id' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to get a workspace without workspace_id")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(getRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to get a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	natsConn, err := initNatsConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer natsConn.Disconnect()

	responseBytes, err := natsConn.Request(cacao_common_service.WorkspaceGetAdminQueryOp, getRequest)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to request a get workspace event")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var response cacao_common_service.WorkspaceGetAdminResult
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to object")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if response.Status.Code == http.StatusOK {
		var apiResponse struct {
			Workspace cacao_common_service.Workspace `json:"workspace"`
		}

		workspaceJSON, err := json.Marshal(response.Data)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response data to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = json.Unmarshal(workspaceJSON, &apiResponse.Workspace)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to workspace object")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		apiResponseBytes, err := json.Marshal(apiResponse)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal an api response object to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponseBytes)
	} else {
		// error
		apiResponse := []byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, response.Status.Code, response.Status.Message))

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponse)
	}
}

func getWorkspaceUser(w http.ResponseWriter, r *http.Request) {
	workspaceID := mux.Vars(r)["workspaceID"]
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "getWorkspaceUser",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to get a workspace")

	getRequest, status, err := makeWorkspaceGetRequest(r, cacao_common.ID(workspaceID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceGetRequestRedacted(getRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(getRequest.WorkspaceID) == 0 {
		err = fmt.Errorf("'workspace_id' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to get a workspace without workspace_id")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(getRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to get a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	natsConn, err := initNatsConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer natsConn.Disconnect()

	responseBytes, err := natsConn.Request(cacao_common_service.WorkspaceGetQueryOp, getRequest)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to request a get workspace event")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var response cacao_common_service.WorkspaceGetResult
	err = json.Unmarshal(responseBytes, &response)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to object")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if response.Status.Code == http.StatusOK {
		var apiResponse struct {
			Workspace cacao_common_service.Workspace `json:"workspace"`
		}

		workspaceJSON, err := json.Marshal(response.Data)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal response data to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = json.Unmarshal(workspaceJSON, &apiResponse.Workspace)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal response JSON to workspace object")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		apiResponseBytes, err := json.Marshal(apiResponse)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to marshal an api response object to JSON")
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponseBytes)
	} else {
		// error
		apiResponse := []byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, response.Status.Code, response.Status.Message))

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.Status.Code)
		w.Write(apiResponse)
	}
}

func createWorkspace(w http.ResponseWriter, r *http.Request) {
	if isAdmin(r) {
		createWorkspaceAdmin(w, r)
	} else {
		createWorkspaceUser(w, r)
	}
}

func createWorkspaceAdmin(w http.ResponseWriter, r *http.Request) {
	workspaceID := cacao_common_service.NewWorkspaceID()
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "createWorkspaceAdmin",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to create a workspace (admin)")

	createRequest, status, err := makeWorkspaceCreateAdminRequest(r, workspaceID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceCreateAdminRequestRedacted(createRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(createRequest.WorkspaceName) == 0 {
		err = fmt.Errorf("'name' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to create a workspace without name")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(createRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to create a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(createRequest.Owner) == 0 {
		createRequest.Owner = createRequest.User.Username
	}

	stanConn, err := initStanConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer stanConn.Disconnect()

	var wg sync.WaitGroup
	wg.Add(1)

	eventSource, err := initEventSource()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	eventsSubscribe := []cacao_common.EventType{
		cacao_common_service.WorkspaceCreatedEvent,
		cacao_common_service.WorkspaceCreateFailedEvent,
	}

	transactionID := cacao_common_messaging.NewTransactionID()
	callback := func(ev cacao_common.EventType, ce cloudevents.Event) {
		defer wg.Done()

		if ev == cacao_common_service.WorkspaceCreatedEvent {
			idResponse(w, string(workspaceID), http.StatusAccepted)
		} else if ev == cacao_common_service.WorkspaceCreateFailedEvent {
			var response cacao_common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal a workspace create response")
				JSONError(w, err.Error(), http.StatusInternalServerError)
				return
			}

			logger.Errorf("unable to create a workspace - %s", response.Status.Message)
			JSONError(w, response.Status.Message, response.Status.Code)
		}
	}

	listener := cacao_common_messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to add an event listener")
		JSONError(w, "unable to add an event listener : "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(cacao_common_service.WorkspaceCreateAdminRequestedEvent, createRequest, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish a create workspace event")
		JSONError(w, "unable to publish a create workspace event : "+err.Error(), http.StatusInternalServerError)
		return
	}

	// wait for response, max 10 sec
	if waitTimeout(&wg, 10*time.Second) {
		// timed out
		logger.Error("unable to create a workspace - workspace service is not responding")
		JSONError(w, "workspace service is not responding", http.StatusRequestTimeout)
		return
	}
}

func createWorkspaceUser(w http.ResponseWriter, r *http.Request) {
	workspaceID := cacao_common_service.NewWorkspaceID()
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "createWorkspaceUser",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to create a workspace")

	createRequest, status, err := makeWorkspaceCreateRequest(r, workspaceID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceCreateRequestRedacted(createRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(createRequest.WorkspaceName) == 0 {
		err = fmt.Errorf("'name' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to create a workspace without name")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(createRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to create a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	stanConn, err := initStanConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer stanConn.Disconnect()

	var wg sync.WaitGroup
	wg.Add(1)

	eventSource, err := initEventSource()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	eventsSubscribe := []cacao_common.EventType{
		cacao_common_service.WorkspaceCreatedEvent,
		cacao_common_service.WorkspaceCreateFailedEvent,
	}

	transactionID := cacao_common_messaging.NewTransactionID()
	callback := func(ev cacao_common.EventType, ce cloudevents.Event) {
		defer wg.Done()

		if ev == cacao_common_service.WorkspaceCreatedEvent {
			idResponse(w, string(workspaceID), http.StatusAccepted)
		} else if ev == cacao_common_service.WorkspaceCreateFailedEvent {
			var response cacao_common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal a workspace create response")
				JSONError(w, err.Error(), http.StatusInternalServerError)
				return
			}

			logger.Errorf("unable to create a workspace - %s", response.Status.Message)
			JSONError(w, response.Status.Message, response.Status.Code)
		}
	}

	listener := cacao_common_messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to add an event listener")
		JSONError(w, "unable to add an event listener : "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(cacao_common_service.WorkspaceCreateRequestedEvent, createRequest, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish a create workspace event")
		JSONError(w, "unable to publish a create workspace event : "+err.Error(), http.StatusInternalServerError)
		return
	}

	// wait for response, max 10 sec
	if waitTimeout(&wg, 10*time.Second) {
		// timed out
		logger.Error("unable to create a workspace - workspace service is not responding")
		JSONError(w, "workspace service is not responding", http.StatusRequestTimeout)
		return
	}
}

func updateWorkspace(w http.ResponseWriter, r *http.Request) {
	if isAdmin(r) {
		updateWorkspaceAdmin(w, r)
	} else {
		updateWorkspaceUser(w, r)
	}
}

func updateWorkspaceAdmin(w http.ResponseWriter, r *http.Request) {
	workspaceID := mux.Vars(r)["workspaceID"]
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "updateWorkspaceAdmin",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to update a workspace (admin)")

	updateRequestMap, updateRequest, status, err := makeWorkspaceUpdateAdminRequest(r, cacao_common.ID(workspaceID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceUpdateAdminRequestRedacted(updateRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(updateRequest.WorkspaceID) == 0 {
		err = fmt.Errorf("'workspace_id' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to update a workspace without workspace_id")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(updateRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to update a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	stanConn, err := initStanConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer stanConn.Disconnect()

	var wg sync.WaitGroup
	wg.Add(1)

	eventSource, err := initEventSource()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	eventsSubscribe := []cacao_common.EventType{
		cacao_common_service.WorkspaceUpdatedEvent,
		cacao_common_service.WorkspaceUpdateFailedEvent,
	}

	transactionID := cacao_common_messaging.NewTransactionID()
	callback := func(ev cacao_common.EventType, ce cloudevents.Event) {
		defer wg.Done()

		if ev == cacao_common_service.WorkspaceUpdatedEvent {
			idResponse(w, string(workspaceID), http.StatusAccepted)
		} else if ev == cacao_common_service.WorkspaceUpdateFailedEvent {
			var response cacao_common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal a workspace update response")
				JSONError(w, err.Error(), http.StatusInternalServerError)
				return
			}

			logger.Errorf("unable to update a workspace - %s", response.Status.Message)
			JSONError(w, response.Status.Message, response.Status.Code)
		}
	}

	listener := cacao_common_messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to add an event listener")
		JSONError(w, "unable to add an event listener : "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(cacao_common_service.WorkspaceUpdateAdminRequestedEvent, updateRequestMap, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish an update workspace event")
		JSONError(w, "unable to publish an update workspace event: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// wait for response, max 10 sec
	if waitTimeout(&wg, 10*time.Second) {
		// timed out
		logger.Error("unable to update a workspace - workspace service is not responding")
		JSONError(w, "workspace service is not responding", http.StatusRequestTimeout)
		return
	}
}

func updateWorkspaceUser(w http.ResponseWriter, r *http.Request) {
	workspaceID := mux.Vars(r)["workspaceID"]
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "updateWorkspaceUser",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to update a workspace")

	updateRequestMap, updateRequest, status, err := makeWorkspaceUpdateRequest(r, cacao_common.ID(workspaceID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceUpdateRequestRedacted(updateRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(updateRequest.WorkspaceID) == 0 {
		err = fmt.Errorf("'workspace_id' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to update a workspace without workspace_id")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(updateRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to update a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	stanConn, err := initStanConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer stanConn.Disconnect()

	var wg sync.WaitGroup
	wg.Add(1)

	eventSource, err := initEventSource()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	eventsSubscribe := []cacao_common.EventType{
		cacao_common_service.WorkspaceUpdatedEvent,
		cacao_common_service.WorkspaceUpdateFailedEvent,
	}

	transactionID := cacao_common_messaging.NewTransactionID()
	callback := func(ev cacao_common.EventType, ce cloudevents.Event) {
		defer wg.Done()

		if ev == cacao_common_service.WorkspaceUpdatedEvent {
			idResponse(w, string(workspaceID), http.StatusAccepted)
		} else if ev == cacao_common_service.WorkspaceUpdateFailedEvent {
			var response cacao_common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal a workspace update response")
				JSONError(w, err.Error(), http.StatusInternalServerError)
				return
			}

			logger.Errorf("unable to update a workspace - %s", response.Status.Message)
			JSONError(w, response.Status.Message, response.Status.Code)
		}
	}

	listener := cacao_common_messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to add an event listener")
		JSONError(w, "unable to add an event listener : "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(cacao_common_service.WorkspaceUpdateRequestedEvent, updateRequestMap, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish an update workspace event")
		JSONError(w, "unable to publish an update workspace event: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// wait for response, max 10 sec
	if waitTimeout(&wg, 10*time.Second) {
		// timed out
		logger.Error("unable to update a workspace - workspace service is not responding")
		JSONError(w, "workspace service is not responding", http.StatusRequestTimeout)
		return
	}
}

func deleteWorkspace(w http.ResponseWriter, r *http.Request) {
	if isAdmin(r) {
		deleteWorkspaceAdmin(w, r)
	} else {
		deleteWorkspaceUser(w, r)
	}
}

func deleteWorkspaceAdmin(w http.ResponseWriter, r *http.Request) {
	workspaceID := mux.Vars(r)["workspaceID"]
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "deleteWorkspaceAdmin",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to delete a workspace (admin)")

	deleteRequest, status, err := makeWorkspaceDeleteAdminRequest(r, cacao_common.ID(workspaceID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceDeleteAdminRequestRedacted(deleteRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(deleteRequest.WorkspaceID) == 0 {
		err = fmt.Errorf("'workspace_id' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete a workspace without workspace_id")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(deleteRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	stanConn, err := initStanConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer stanConn.Disconnect()

	var wg sync.WaitGroup
	wg.Add(1)

	eventSource, err := initEventSource()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	eventsSubscribe := []cacao_common.EventType{
		cacao_common_service.WorkspaceDeletedEvent,
		cacao_common_service.WorkspaceDeleteFailedEvent,
	}

	transactionID := cacao_common_messaging.NewTransactionID()
	callback := func(ev cacao_common.EventType, ce cloudevents.Event) {
		defer wg.Done()

		if ev == cacao_common_service.WorkspaceDeletedEvent {
			idResponse(w, string(workspaceID), http.StatusAccepted)
		} else if ev == cacao_common_service.WorkspaceDeleteFailedEvent {
			var response cacao_common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal a workspace delete response")
				JSONError(w, err.Error(), http.StatusInternalServerError)
				return
			}

			logger.Errorf("unable to delete a workspace - %s", response.Status.Message)
			JSONError(w, response.Status.Message, response.Status.Code)
		}
	}

	listener := cacao_common_messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to add an event listener")
		JSONError(w, "unable to add an event listener : "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(cacao_common_service.WorkspaceDeleteAdminRequestedEvent, deleteRequest, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish a delete workspace event")
		JSONError(w, "unable to publish a delete workspace event : "+err.Error(), http.StatusInternalServerError)
		return
	}

	// wait for response, max 10 sec
	if waitTimeout(&wg, 10*time.Second) {
		// timed out
		logger.Error("unable to delete a workspace - workspace service is not responding")
		JSONError(w, "workspace service is not responding", http.StatusRequestTimeout)
		return
	}
}

func deleteWorkspaceUser(w http.ResponseWriter, r *http.Request) {
	workspaceID := mux.Vars(r)["workspaceID"]
	logger := log.WithFields(log.Fields{
		"package":      "api",
		"function":     "deleteWorkspaceUser",
		"workspace_id": workspaceID,
	})
	logger.Info("received request to delete a workspace")

	deleteRequest, status, err := makeWorkspaceDeleteRequest(r, cacao_common.ID(workspaceID))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to parse and make a workspace request")
		JSONError(w, err.Error(), status)
		return
	}

	logRequest := getWorkspaceDeleteRequestRedacted(deleteRequest)
	logger = logger.WithFields(log.Fields{"request": logRequest})
	logger.Info("parsed a request")

	if len(deleteRequest.WorkspaceID) == 0 {
		err = fmt.Errorf("'workspace_id' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete a workspace without workspace_id")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if len(deleteRequest.User.Username) == 0 {
		err = fmt.Errorf("'user' is missing from request but is required")
		logger.WithFields(log.Fields{"error": err}).Error("unable to delete a workspace without user")
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	stanConn, err := initStanConn()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer stanConn.Disconnect()

	var wg sync.WaitGroup
	wg.Add(1)

	eventSource, err := initEventSource()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to initialize a NATS/STAN connection")
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	eventsSubscribe := []cacao_common.EventType{
		cacao_common_service.WorkspaceDeletedEvent,
		cacao_common_service.WorkspaceDeleteFailedEvent,
	}

	transactionID := cacao_common_messaging.NewTransactionID()
	callback := func(ev cacao_common.EventType, ce cloudevents.Event) {
		defer wg.Done()

		if ev == cacao_common_service.WorkspaceDeletedEvent {
			idResponse(w, string(workspaceID), http.StatusAccepted)
		} else if ev == cacao_common_service.WorkspaceDeleteFailedEvent {
			var response cacao_common.ServiceRequestResult
			err := json.Unmarshal(ce.Data(), &response)
			if err != nil {
				logger.WithFields(log.Fields{"error": err}).Error("unable to unmarshal a workspace delete response")
				JSONError(w, err.Error(), http.StatusInternalServerError)
				return
			}

			logger.Errorf("unable to delete a workspace - %s", response.Status.Message)
			JSONError(w, response.Status.Message, response.Status.Code)
		}
	}

	listener := cacao_common_messaging.Listener{
		Callback:   callback,
		ListenOnce: true,
	}

	listenerID, err := eventSource.AddListenerMultiEventType(eventsSubscribe, transactionID, listener)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to add an event listener")
		JSONError(w, "unable to add an event listener : "+err.Error(), http.StatusInternalServerError)
		return
	}

	defer eventSource.RemoveListenerByID(listenerID)

	err = stanConn.PublishWithTransactionID(cacao_common_service.WorkspaceDeleteRequestedEvent, deleteRequest, transactionID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to publish a delete workspace event")
		JSONError(w, "unable to publish a delete workspace event : "+err.Error(), http.StatusInternalServerError)
		return
	}

	// wait for response, max 10 sec
	if waitTimeout(&wg, 10*time.Second) {
		// timed out
		logger.Error("unable to delete a workspace - workspace service is not responding")
		JSONError(w, "workspace service is not responding", http.StatusRequestTimeout)
		return
	}
}
*/
