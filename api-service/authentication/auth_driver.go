package authentication

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cyverse/cacao-common/service"
)

// AuthDriver is primary interface for authentication drivers
type AuthDriver interface {

	// assumes gorilla mux; gives the ability to add additional handle functions for a given driver
	AddRoutes(router *mux.Router)

	// Authenticate is the primary method authenticate users
	// This should return a user object and err, if either is appropriate
	Authenticate(header http.Header, w http.ResponseWriter) (user service.User, err error)
}
