package authentication

import (
	"context"
	"strconv"
	"time"

	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"

	log "github.com/sirupsen/logrus"
)

// BaseNatsDriver simply provides a way to interface with the query and events queue and provides no other
// authentication methods, other than getOrCreateUser()
type BaseNatsDriver struct {
	context        context.Context
	natsConfig     messaging.NatsConfig
	stanConfig     messaging.StanConfig // not used except for initialization of UserClient
	autoCreateUser bool
}

// getOrCreateUser gets the User by username; if isAutoCreate = true, it will create the user as well using email, firstname, lastname
// the isAdmin field is set to value, if the method has to create the user
// NB: Instead of getOrCreate there will eventually be a way for the user to retrieve the other required values
func (b *BaseNatsDriver) getOrCreateUser(username, email, firstname, lastname string, isAdmin bool) (service.User, error) {
	log.Trace("BaseNatsDriver.getOrCreateUser(): start (username = " + username + ", isAdmin = " + strconv.FormatBool(isAdmin) + ")")
	var user service.User = nil
	var err error = nil

	// Get the userclient, using the username as the actor
	// TODO: provide a more informative error type/message
	log.Trace("BaseNatsDriver.getOrCreateUser:\tcreating NatsUserClient")
	var userclient service.UserClient
	userclient, err = service.NewNatsUserClient(b.context, service.ReservedCacaoSystemActor, "", b.natsConfig, b.stanConfig)
	if err != nil {
		return nil, err
	}

	// Get the user object
	// TODO: provide a more informative error type/message
	log.Trace("BaseNatsDriver.getOrCreateUser:\tgetting User")
	user, err = userclient.Get(username)
	if err == nil { // it is here that the user exists, just return
		log.Trace("BaseNatsDriver.getOrCreateUser:\tfound user, user.GetIsAdmin() == " + strconv.FormatBool(user.GetIsAdmin()))
		return user, nil
	}

	// create the user if the user doesn't exist
	log.Trace("BaseNatsDriver.getOrCreateUser:\tdid not find user, error = " + err.Error())
	if err.Error() == service.UserUsernameNotFoundError && b.autoCreateUser {
		log.Trace("BaseNatsDriver.getOrCreateUser:\tattempting to create")
		user = nil
		if firstname == "" {
			firstname = username
		}
		if lastname == "" {
			lastname = "unknown"
		}
		if email == "" {
			email = username + "@cyverse.org"
		}
		newUser := service.NewUser(username)
		newUser.SetFirstName(firstname)
		newUser.SetLastName(lastname)
		newUser.SetPrimaryEmail(email)
		newUser.SetIsAdmin(isAdmin)
		err = userclient.Add(newUser)
		if err == nil {
			user = newUser

			// TODO: try to resolve why we need to wait for new auto creation, async isn't working?
			log.Trace("BaseNatsDriver.getOrCreateUser: successfully added user, sleeping for 2 seconds")
			time.Sleep(2 * time.Second)
		}
	}

	return user, err
}
