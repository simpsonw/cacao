#!/bin/bash

SUDO="sudo"
APT_UPDATE_RAN=0

function run_apt_update {
    if [ "$APT_UPDATE_RAN" == 0 ]; then
        $SUDO apt update -y
        APT_UPDATE_RAN=1
    fi

}
if [ $# -lt 1 ] || [[ "$1" != "local"  && "$1" != "prod" && "$1" != "ci" ]]; then
    echo "Usage: $0 [local|prod|ci]"
	  exit 1
fi

CACAO_DEPLOY_ENVIRONMENT="$1"
if [ "$CACAO_DEPLOY_ENVIRONMENT" == "prod" ] && [ ! -f "hosts.yml" ]; then
    printf "No hosts.yml found for production deployment.  Exiting.\n"
    exit 1
fi

if [ "$CACAO_DEPLOY_ENVIRONMENT" == "ci" ]; then
    # Dont' use sudo in CI since we're already root in the container
    SUDO=''
    run_apt_update
    apt install -y apt-utils
    echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
    apt install -y openssh-client
fi

CACAO_CONFIG_FILE="config.$CACAO_DEPLOY_ENVIRONMENT.yml"
printf "Deploying to $CACAO_DEPLOY_ENVIRONMENT...\n"

if ! command -v envsubst &> /dev/null ; then
    run_apt_update
    apt install -y gettext-base
fi

if ! command -v pip3 &> /dev/null ; then
    run_apt_update
    $SUDO apt install -y python3-pip
fi

if ! command -v ansible &> /dev/null ; then
    run_apt_update
    yes | pip3 install -Iv ansible==2.9.16
fi

if ! command -v jq &> /dev/null ; then
    run_apt_update
    $SUDO apt-get install -y jq
fi

if ! command -v curl &> /dev/null ; then
    run_apt_update
    $SUDO apt-get install -y curl
fi

if [ ! -f "$CACAO_CONFIG_FILE" ] ; then
    cp "example_$CACAO_CONFIG_FILE" $CACAO_CONFIG_FILE

    if [ "$CACAO_DEPLOY_ENVIRONMENT" == "local" ]; then
	DEFAULT_CLUSTER_TYPE="k3d"
        read -p "Enter your cluster type, either k3s or k3d [$DEFAULT_CLUSTER_TYPE]: " CLUSTER_TYPE
        CLUSTER_TYPE=${CLUSTER_TYPE:-$DEFAULT_CLUSTER_TYPE}
        export CLUSTER_TYPE

        DEFAULT_SRC_DIR=`dirname $PWD`
        read -p "Enter the path to the Cacao source code directory [$DEFAULT_SRC_DIR]: " SRC_DIR
        SRC_DIR=${SRC_DIR:-$DEFAULT_SRC_DIR}
        export SRC_DIR
        DEFAULT_GIT_URL=`git remote get-url origin`
        DEFAULT_GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
    else
        DEFAULT_GIT_URL="https://gitlab.com/cyverse/cacao.git"
        DEFAULT_GIT_BRANCH="master"
    fi

    if [ "$CACAO_DEPLOY_ENVIRONMENT" == "ci" ]; then
        GIT_URL=$CI_MERGE_REQUEST_PROJECT_URL
        GIT_BRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
        CACAO_ROOT="${CACAO_ROOT:-/opt/cacao}" # set to /opt/cacao as default, if the env variable doesn't already exist
        export CACAO_ROOT
    else
        read -p "Enter the URL to the Git remote repo [$DEFAULT_GIT_URL]: " GIT_URL
        GIT_URL=${GIT_URL:-$DEFAULT_GIT_URL}

        read -p "Enter the Git branch you want to use [$DEFAULT_GIT_BRANCH]: " GIT_BRANCH
        GIT_BRANCH=${GIT_BRANCH:-$DEFAULT_GIT_BRANCH}
    fi
    export GIT_URL
    export GIT_BRANCH

    if [ "$CACAO_DEPLOY_ENVIRONMENT" == "prod" ]; then
        read -p "Enter the Stache API Endpoint: " STACHE_API_ENDPOINT
        read -p "Enter the X-STACHE-KEY: " X_STACHE_KEY
        source <(curl -s https://stache.arizona.edu$STACHE_API_ENDPOINT?key=$X_STACHE_KEY | jq -r .secret )
    fi

    envsubst < $CACAO_CONFIG_FILE > config_subst.yml
    mv config_subst.yml $CACAO_CONFIG_FILE
fi

if [ -L config.yml ]; then
    rm config.yml
fi

ln -s $CACAO_CONFIG_FILE config.yml

ansible-galaxy role install -r requirements.yml

if [ "$CACAO_DEPLOY_ENVIRONMENT" == "local" ]; then

    if [ `id -u` -eq 0 ]; then
       ansible-playbook -i localhost.yml playbook-cacao.yml
       if [ $? -ne 0 ]; then
            echo "?Non-zero status was returned, propagating the non-zero status"
            exit 1
       fi
    else
       USER_IN_DOCKER_GROUP=`id -Gn $USER | grep '\bdocker\b'`
       if [[ ! $USER_IN_DOCKER_GROUP ]]; then
           $SUDO addgroup docker
           $SUDO usermod -aG docker $USER
           printf "User \"$USER\" has been added to the \"docker\" group\n"
           printf "To complete local deployment, please log out, log back in, and re-run this script.\n"
           exit 1
       fi

       ansible-playbook -K -i localhost.yml playbook-cacao.yml
       if [ $? -ne 0 ]; then
            echo "?Non-zero status was returned, propagating the non-zero status"
            exit 1
       fi
    fi

    printf "Setup complete! \n"
    printf "If you don't see any failures, go to CACAO_SRC_DIR and run 'skaffold dev --status-check=false' to start Cacao\n"
    cat config.yml | grep CACAO_SRC_DIR:
elif [ "$CACAO_DEPLOY_ENVIRONMENT" == "ci" ]; then
    ansible-playbook --private-key ~/.ssh/id_ed25519 -i hosts.yml playbook-cacao.yml
    if [ $? -ne 0 ]; then
        echo "?Non-zero status was returned, propagating the non-zero status"
        exit 1
    else
        printf "Set up complete!\n"
    fi
elif [ "$CACAO_DEPLOY_ENVIRONMENT" == "prod" ]; then
    ansible-playbook -i hosts.yml playbook-cacao.yml
    if [ $? -ne 0 ]; then
        echo "?Non-zero status was returned, propagating the non-zero status"
        exit 1
    else
        printf "Set up complete!\n"
    fi
fi
