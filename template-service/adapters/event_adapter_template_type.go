package adapters

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func (adapter *EventAdapter) handleTemplateTypeCreateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateTypeCreateRequest",
	})

	var createRequest cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &createRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateTypeModel")
		return fmt.Errorf("unable to unmarshal JSON bytes into TemplateTypeModel")
	}

	templateType := types.ConvertTypeFromModel(createRequest)

	err = adapter.IncomingPort.CreateType(createRequest.GetSessionActor(), createRequest.GetSessionEmulator(), templateType, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateTypeDeleteRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateTypeDeleteRequest",
	})

	var deleteRequest cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &deleteRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateTypeModel")
		return fmt.Errorf("unable to unmarshal JSON bytes into TemplateTypeModel")
	}

	templateType := types.ConvertTypeFromModel(deleteRequest)

	err = adapter.IncomingPort.DeleteType(deleteRequest.GetSessionActor(), deleteRequest.GetSessionEmulator(), templateType, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateTypeUpdateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateTypeUpdateRequest",
	})

	var updateRequestMap map[string]interface{}
	err := json.Unmarshal(jsonData, &updateRequestMap)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into map")
		return fmt.Errorf("unable to unmarshal JSON bytes into map")
	}

	var updateRequest cacao_common_service.TemplateTypeModel
	err = json.Unmarshal(jsonData, &updateRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateTypeModel")
		return fmt.Errorf("unable to unmarshal JSON bytes into TemplateTypeModel")
	}

	// check which fields to update
	updateFieldNames := []string{}
	if _, ok := updateRequestMap["formats"]; ok {
		// has ok
		updateFieldNames = append(updateFieldNames, "formats")
	}

	if _, ok := updateRequestMap["engine"]; ok {
		// has ok
		updateFieldNames = append(updateFieldNames, "engine")
	}

	if _, ok := updateRequestMap["provider_types"]; ok {
		// has ok
		updateFieldNames = append(updateFieldNames, "provider_types")
	}

	templateType := types.ConvertTypeFromModel(updateRequest)

	err = adapter.IncomingPort.UpdateType(updateRequest.GetSessionActor(), updateRequest.GetSessionEmulator(), templateType, updateFieldNames, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

// Implement OutgoingEventPort

// TypeCreated ...
func (adapter *EventAdapter) TypeCreated(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeCreated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertTypeToModel(session, templateType)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeCreatedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreatedEvent)
	}

	return nil
}

// TypeCreateFailed ...
func (adapter *EventAdapter) TypeCreateFailed(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeCreateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       fmt.Sprintf("%d", status.Code),
		ErrorMessage:    status.Message,
	}

	model := types.ConvertTypeToModel(session, templateType)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeCreateFailedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeCreateFailedEvent)
	}

	return nil
}

// TypeUpdated ...
func (adapter *EventAdapter) TypeUpdated(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeUpdated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertTypeToModel(session, templateType)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeUpdatedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdatedEvent)
	}

	return nil
}

// TypeUpdateFailed ...
func (adapter *EventAdapter) TypeUpdateFailed(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeUpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       fmt.Sprintf("%d", status.Code),
		ErrorMessage:    status.Message,
	}

	model := types.ConvertTypeToModel(session, templateType)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeUpdateFailedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeUpdateFailedEvent)
	}

	return nil
}

// TypeDeleted ...
func (adapter *EventAdapter) TypeDeleted(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeDeleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertTypeToModel(session, templateType)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeDeletedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeletedEvent)
	}

	return nil
}

// TypeDeleteFailed ...
func (adapter *EventAdapter) TypeDeleteFailed(actor string, emulator string, templateType types.TemplateType, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.TypeDeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       fmt.Sprintf("%d", status.Code),
		ErrorMessage:    status.Message,
	}

	model := types.ConvertTypeToModel(session, templateType)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateTypeDeleteFailedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateTypeDeleteFailedEvent)
	}

	return nil
}
