package adapters

import (
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingEventPort
	// internal
	Connection     cacao_common_messaging.StreamingEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *EventAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Init",
	})

	logger.Info("initializing EventAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *EventAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Finalize",
	})

	logger.Info("finalizing EventAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *EventAdapter) getEventHandlerMapping() []cacao_common_messaging.StreamingEventHandlerMapping {
	return []cacao_common_messaging.StreamingEventHandlerMapping{
		{
			Subject:      cacao_common_service.TemplateTypeCreateRequestedEvent,
			EventHandler: adapter.handleTemplateTypeCreateRequest,
		},
		{
			Subject:      cacao_common_service.TemplateTypeDeleteRequestedEvent,
			EventHandler: adapter.handleTemplateTypeDeleteRequest,
		},
		{
			Subject:      cacao_common_service.TemplateTypeUpdateRequestedEvent,
			EventHandler: adapter.handleTemplateTypeUpdateRequest,
		},
		{
			Subject:      cacao_common_service.TemplateImportRequestedEvent,
			EventHandler: adapter.handleTemplateImportRequest,
		},
		{
			Subject:      cacao_common_service.TemplateDeleteRequestedEvent,
			EventHandler: adapter.handleTemplateDeleteRequest,
		},
		{
			Subject:      cacao_common_service.TemplateUpdateRequestedEvent,
			EventHandler: adapter.handleTemplateUpdateRequest,
		},
		{
			Subject:      cacao_common_service.TemplateSyncRequestedEvent,
			EventHandler: adapter.handleTemplateSyncRequest,
		},
		{
			Subject:      cacao_common.EventType(""),
			EventHandler: adapter.handleDefaultEvent,
		},
	}
}

// Start starts the adapter
func (adapter *EventAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("starting EventAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-event" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	stanConn, err := cacao_common_messaging.ConnectStanForService(&natsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *EventAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.StartMock",
	})

	logger.Info("starting EventAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	stanConn, err := cacao_common_messaging.CreateMockStanConnection(&adapter.Config.NatsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *EventAdapter) handleDefaultEvent(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleDefaultEvent",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)
	return nil
}
