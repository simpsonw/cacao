package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTypes returns all template types
func (adapter *MongoAdapter) ListTypes() ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.ListTypes",
	})

	results := []types.TemplateType{}

	err := adapter.Store.List(adapter.Config.TemplateTypeMongoDBCollectionName, &results)
	if err != nil {
		logger.WithError(err).Error("unable to list template types")
		return nil, fmt.Errorf("unable to list template types")
	}

	return results, nil
}

// MockListTypes sets expected results for ListTypes
func (adapter *MongoAdapter) MockListTypes(expectedTemplateTypes []types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("List", adapter.Config.TemplateTypeMongoDBCollectionName).Return(expectedTemplateTypes, expectedError)
	return nil
}

// ListTypesForProviderType returns all template types for the given ProviderType
func (adapter *MongoAdapter) ListTypesForProviderType(providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.ListTypesForProviderType",
	})

	results := []types.TemplateType{}

	filter := map[string]interface{}{
		"provider_types": map[string]interface{}{
			"$in": string(providerType),
		},
	}

	err := adapter.Store.ListConditional(adapter.Config.TemplateTypeMongoDBCollectionName, filter, &results)
	if err != nil {
		logger.WithError(err).Errorf("unable to list template types for provider type %s", string(providerType))
		return nil, fmt.Errorf("unable to list template types for provider type %s", string(providerType))
	}

	return results, nil
}

// MockListTypesForProviderType sets expected results for ListTypesForProviderType
func (adapter *MongoAdapter) MockListTypesForProviderType(providerType cacao_common_service.TemplateProviderType, expectedTemplateTypes []types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	filter := map[string]interface{}{
		"provider_types": map[string]interface{}{
			"$in": string(providerType),
		},
	}

	mock.On("ListConditional", adapter.Config.TemplateTypeMongoDBCollectionName, filter).Return(expectedTemplateTypes, expectedError)
	return nil
}

// GetType returns the template type with the name
func (adapter *MongoAdapter) GetType(templateTypeName string) (types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.GetType",
	})

	result := types.TemplateType{}

	err := adapter.Store.Get(adapter.Config.TemplateTypeMongoDBCollectionName, templateTypeName, &result)
	if err != nil {
		logger.WithError(err).Errorf("unable to get the template type for name %s", templateTypeName)
		return result, fmt.Errorf("unable to get the template type for name %s", templateTypeName)
	}

	return result, nil
}

// MockGetType sets expected results for GetType
func (adapter *MongoAdapter) MockGetType(templateTypeName string, expectedTemplateType types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TemplateTypeMongoDBCollectionName, templateTypeName).Return(expectedTemplateType, expectedError)
	return nil
}

// CreateType inserts a template type
func (adapter *MongoAdapter) CreateType(templateType types.TemplateType) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.CreateType",
	})

	err := adapter.Store.Insert(adapter.Config.TemplateTypeMongoDBCollectionName, templateType)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			logger.WithError(err).Errorf("unable to insert a template type because name %s conflicts", templateType.Name)
			return types.NewTemplateTypeConflictErrorf("unable to insert a template type because name %s conflicts", templateType.Name)
		}

		logger.WithError(err).Errorf("unable to insert a template type with name %s", templateType.Name)
		return fmt.Errorf("unable to insert a template type with name %s", templateType.Name)
	}

	return nil
}

// MockCreateType sets expected results for CreateType
func (adapter *MongoAdapter) MockCreateType(templateType types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TemplateTypeMongoDBCollectionName).Return(expectedError)
	return nil
}

// UpdateType updates/edits a template type
func (adapter *MongoAdapter) UpdateType(templateType types.TemplateType, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.UpdateType",
	})

	updated, err := adapter.Store.Update(adapter.Config.TemplateTypeMongoDBCollectionName, templateType.Name, templateType, updateFieldNames)
	if err != nil {
		logger.WithError(err).Errorf("unable to update the template type for name %s", templateType.Name)
		return fmt.Errorf("unable to update the template type for name %s", templateType.Name)
	}

	if !updated {
		logger.Errorf("unable to update the template type for name %s", templateType.Name)
		return fmt.Errorf("unable to update the template type for id %s", templateType.Name)
	}

	return nil
}

// MockUpdateType sets expected results for UpdateType
func (adapter *MongoAdapter) MockUpdateType(templateType types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Update", adapter.Config.TemplateTypeMongoDBCollectionName, templateType.Name).Return(expectedResult, expectedError)
	return nil
}

// DeleteType deletes a template type
func (adapter *MongoAdapter) DeleteType(templateTypeName string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.DeleteType",
	})

	deleted, err := adapter.Store.Delete(adapter.Config.TemplateTypeMongoDBCollectionName, templateTypeName)
	if err != nil {
		logger.WithError(err).Errorf("unable to delete the template type for name %s", templateTypeName)
		return fmt.Errorf("unable to delete the template type for name %s", templateTypeName)
	}

	if !deleted {
		logger.Errorf("unable to delete the template type for name %s", templateTypeName)
		return types.NewTemplateTypeNotFoundErrorf("unable to delete the template type for name %s", templateTypeName)
	}

	return nil
}

// MockDeleteType sets expected results for DeleteType
func (adapter *MongoAdapter) MockDeleteType(templateTypeName string, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Delete", adapter.Config.TemplateTypeMongoDBCollectionName, templateTypeName).Return(expectedResult, expectedError)
	return nil
}
