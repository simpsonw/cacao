package adapters

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func createTestEventInPort() *ports.MockIncomingEventPort {
	var config types.Config
	config.ProcessDefaults()

	eventInImpl := &ports.MockIncomingEventPort{}
	eventInImpl.Init(&config)

	return eventInImpl
}

func createTestEventAdapter(eventInImpl ports.IncomingEventPort) *EventAdapter {
	var config types.Config
	config.ProcessDefaults()

	eventAdapter := &EventAdapter{}
	eventAdapter.Init(&config)

	eventAdapter.IncomingPort = eventInImpl

	go eventAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return eventAdapter
}

func TestInitEventAdapter(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)
	assert.NotNil(t, eventAdapter)
	assert.NotEmpty(t, eventAdapter.Connection)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}
