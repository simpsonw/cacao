package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

func (adapter *QueryAdapter) handleTemplateSourceTypeListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateSourceTypeListQuery",
	})

	var listRequest cacao_common_service.Session
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into Session")
		return nil, fmt.Errorf("unable to unmarshal JSON bytes into Session")
	}

	listSourceTypesResult, err := adapter.IncomingPort.ListSourceTypes(listRequest.GetSessionActor(), listRequest.SessionEmulator)
	var templateSourceTypeListResponse cacao_common.ServiceRequestResult

	if err == nil {
		templateSourceTypeListResponse = cacao_common.ServiceRequestResult{
			Data: listSourceTypesResult,
			Status: cacao_common.HTTPStatus{
				Message: "listed template source types successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		logger.Error(err)

		templateSourceTypeListResponse = cacao_common.ServiceRequestResult{
			Status: cacao_common.HTTPStatus{
				Message: err.Error(),
				Code:    http.StatusInternalServerError,
			},
		}
	}

	resBytes, err := json.Marshal(templateSourceTypeListResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}
