package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestTemplateTypeCreateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}

	eventInImpl.SetCreateTypeHandler(func(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateType.Name)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateTypeCreateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateTypeCreateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}

	eventInImpl.SetCreateTypeHandler(func(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateType.Name)
		return fmt.Errorf("unable to create a template type")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateTypeCreateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateTypeUpdateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}

	eventInImpl.SetUpdateTypeHandler(func(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateType.Name)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateTypeUpdateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateTypeUpdateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
		Formats: []cacao_common_service.TemplateFormat{
			cacao_common_service.TemplateFormatYAML,
			cacao_common_service.TemplateFormatJSON,
		},
		Engine: cacao_common_service.TemplateEngineTerraform,
		ProviderTypes: []cacao_common_service.TemplateProviderType{
			cacao_common_service.TemplateProviderTypeOpenStack,
			cacao_common_service.TemplateProviderTypeKubernetes,
		},
	}

	eventInImpl.SetUpdateTypeHandler(func(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateType.Name)
		return fmt.Errorf("unable to update a template type")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateTypeUpdateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateTypeDeleteEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
	}

	eventInImpl.SetDeleteTypeHandler(func(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateType.Name)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateTypeDeleteRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateTypeDeleteEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testTemplateTypeName,
	}

	eventInImpl.SetDeleteTypeHandler(func(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateTypeName, templateType.Name)
		return fmt.Errorf("unable to delete a template type")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TemplateTypeDeleteRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTemplateTypeCreatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := types.TemplateType{
		Name: testTemplateTypeName,
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "template type is created",
		Code:    http.StatusOK,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateTypeCreatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateTypeCreatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		templateTypeJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var templateTypeModel cacao_common_service.TemplateTypeModel

		err = json.Unmarshal(templateTypeJSON, &templateTypeModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, templateTypeModel.GetSessionActor())
		assert.Equal(t, testUser, templateTypeModel.GetSessionEmulator())
		assert.Equal(t, testTemplateTypeName, templateTypeModel.Name)
		return nil
	})

	err := eventAdapter.TypeCreated(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateTypeCreateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := types.TemplateType{
		Name: testTemplateTypeName,
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "template type is not created",
		Code:    http.StatusInternalServerError,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateTypeCreateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateTypeCreateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		templateTypeJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var templateTypeModel cacao_common_service.TemplateTypeModel

		err = json.Unmarshal(templateTypeJSON, &templateTypeModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, templateTypeModel.GetSessionActor())
		assert.Equal(t, testUser, templateTypeModel.GetSessionEmulator())
		assert.Equal(t, testTemplateTypeName, templateTypeModel.Name)
		return nil
	})

	err := eventAdapter.TypeCreateFailed(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateTypeUpdatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := types.TemplateType{
		Name: testTemplateTypeName,
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "template type is updated",
		Code:    http.StatusOK,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateTypeUpdatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateTypeUpdatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		templateTypeJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var templateTypeModel cacao_common_service.TemplateTypeModel

		err = json.Unmarshal(templateTypeJSON, &templateTypeModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, templateTypeModel.GetSessionActor())
		assert.Equal(t, testUser, templateTypeModel.GetSessionEmulator())
		assert.Equal(t, testTemplateTypeName, templateTypeModel.Name)
		return nil
	})

	err := eventAdapter.TypeUpdated(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateTypeUpdateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := types.TemplateType{
		Name: testTemplateTypeName,
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "template type is not updated",
		Code:    http.StatusInternalServerError,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateTypeUpdateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateTypeUpdateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		templateTypeJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var templateTypeModel cacao_common_service.TemplateTypeModel

		err = json.Unmarshal(templateTypeJSON, &templateTypeModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, templateTypeModel.GetSessionActor())
		assert.Equal(t, testUser, templateTypeModel.GetSessionEmulator())
		assert.Equal(t, testTemplateTypeName, templateTypeModel.Name)
		return nil
	})

	err := eventAdapter.TypeUpdateFailed(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateTypeDeletedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := types.TemplateType{
		Name: testTemplateTypeName,
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "template type is deleted",
		Code:    http.StatusOK,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateTypeDeletedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateTypeDeletedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		templateTypeJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var templateTypeModel cacao_common_service.TemplateTypeModel

		err = json.Unmarshal(templateTypeJSON, &templateTypeModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, templateTypeModel.GetSessionActor())
		assert.Equal(t, testUser, templateTypeModel.GetSessionEmulator())
		assert.Equal(t, testTemplateTypeName, templateTypeModel.Name)
		return nil
	})

	err := eventAdapter.TypeDeleted(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTemplateTypeDeleteFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testTemplateTypeName := "test_template_type_name1"

	eventData := types.TemplateType{
		Name: testTemplateTypeName,
	}

	testStatus := cacao_common.HTTPStatus{
		Message: "template type is not deleted",
		Code:    http.StatusInternalServerError,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TemplateTypeDeleteFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TemplateTypeDeleteFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common.ServiceRequestResult
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testStatus.Code, response.Status.Code)

		templateTypeJSON, err := json.Marshal(response.Data)
		assert.NoError(t, err)

		var templateTypeModel cacao_common_service.TemplateTypeModel

		err = json.Unmarshal(templateTypeJSON, &templateTypeModel)
		assert.NoError(t, err)

		assert.Equal(t, testUser, templateTypeModel.GetSessionActor())
		assert.Equal(t, testUser, templateTypeModel.GetSessionEmulator())
		assert.Equal(t, testTemplateTypeName, templateTypeModel.Name)
		return nil
	})

	err := eventAdapter.TypeDeleteFailed(testUser, testUser, eventData, testStatus, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}
