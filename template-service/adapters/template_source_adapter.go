package adapters

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/git"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// GitAdapter implements TemplateSourcePort
type GitAdapter struct {
	Config           *types.Config
	GitStorageOption *git.RAMStorageOption
}

// Init initialize git adapter
func (adapter *GitAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.Init",
	})

	logger.Info("Initializing Git Adapter")

	adapter.Config = config

	adapter.GitStorageOption = &git.RAMStorageOption{
		SizeLimit: config.GitRAMSizeLimit,
	}
}

// Finalize finalizes mongodb adapter
func (adapter *GitAdapter) Finalize() {
	adapter.GitStorageOption = nil
}

// GetTemplateMetadata returns all template metadata
func (adapter *GitAdapter) GetTemplateMetadata(source cacao_common_service.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (cacao_common_service.TemplateMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.GetTemplateMetadata",
	})

	if source.Type == types.TemplateSourceTypeGit {
		return adapter.getTemplateMetadataFromGit(source, credential)
	}

	logger.Errorf("unhandled template source type %s", source.Type)
	return cacao_common_service.TemplateMetadata{}, fmt.Errorf("unhandled template source type %s", source.Type)
}

func (adapter *GitAdapter) getTemplateMetadataFromGit(source cacao_common_service.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (cacao_common_service.TemplateMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.getTemplateMetadataFromGit",
	})

	referenceType := git.BranchReferenceType
	referenceName := "master"
	templateDir := "/"

	if val, ok := source.AccessParameters["branch"]; ok {
		referenceType = git.BranchReferenceType
		referenceName = val.(string)
	}

	if val, ok := source.AccessParameters["tag"]; ok {
		referenceType = git.TagReferenceType
		referenceName = val.(string)
	}

	if val, ok := source.AccessParameters["path"]; ok {
		templateDir = val.(string)
	}

	repoConfig := &git.RepositoryConfig{
		URL:           source.URI,
		Username:      credential.Username,
		Password:      credential.Password,
		ReferenceType: referenceType,
		ReferenceName: referenceName,
	}

	clone, err := git.NewGitCloneToRAM(repoConfig, adapter.GitStorageOption)
	if err != nil {
		logger.WithError(err).Errorf("unable to clone git repository %s", source.URI)
		return cacao_common_service.TemplateMetadata{}, err
	}

	defer clone.Release()

	metadata, err := adapter.readTemplateMetadataFromGit(clone, templateDir)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read template metadata")
		return cacao_common_service.TemplateMetadata{}, err
	}

	return metadata, nil
}

// getMetadataFilenameCandidates returns candidate metadata filenames for a template project
func (adapter *GitAdapter) getMetadataFilenameCandidates() []string {
	return []string{
		"cacao.json",
		"metadata.json",
		"cacao-metadata.json",
		".cacao.json",
	}
}

// readTemplateMetadataFromGit reads template metadata from git repository
func (adapter *GitAdapter) readTemplateMetadataFromGit(clone *git.Clone, subdirpath string) (cacao_common_service.TemplateMetadata, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "GitAdapter.readTemplateMetadataFromGit",
	})

	subpath := "/"
	if len(subdirpath) > 0 {
		subpath = subdirpath
	}

	if !strings.HasPrefix(subpath, "/") {
		subpath = "/" + subpath
	}
	subpath = filepath.Clean(subpath)

	metadata := cacao_common_service.TemplateMetadata{}

	fs := clone.GetFileSystem()
	files, err := fs.ReadDir(subpath)
	if err != nil {
		logger.WithError(err).Errorf("failed to list files from %s", clone.Config.URL)
		return metadata, err
	}

	metadataFileCandidates := adapter.getMetadataFilenameCandidates()
	metadataFileCandidatesMap := map[string]bool{}

	for _, metadataFileCandidate := range metadataFileCandidates {
		metadataFileCandidatesMap[metadataFileCandidate] = true
	}

	for _, file := range files {
		if !file.IsDir() {
			// must be a file
			if _, ok := metadataFileCandidatesMap[file.Name()]; ok {
				// found
				filePath := filepath.Join(subpath, file.Name())
				fd, err := fs.Open(filePath)
				if err != nil {
					logger.WithError(err).Errorf("failed to open a metadata file %s", filePath)
					return metadata, err
				}

				metadataBytes, err := ioutil.ReadAll(fd)
				if err != nil {
					logger.WithError(err).Errorf("failed to read metadata from a file %s", filePath)
					return metadata, err
				}

				// validate
				err = types.ValidateTemplateMetadataJSON(metadataBytes)
				if err != nil {
					logger.Error(err)
					return metadata, err
				}

				// unmarshal
				err = json.Unmarshal(metadataBytes, &metadata)
				if err != nil {
					logger.Error(err)
					return metadata, err
				}

				return metadata, nil
			}
		}
	}

	return metadata, fmt.Errorf("failed to find metadata file in the git repository %s", clone.Config.URL)
}
