package adapters

import (
	"context"
	"encoding/json"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestTemplateSourceTypeListQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	expectedResults := []cacao_common_service.TemplateSourceType{
		types.TemplateSourceTypeGit,
	}

	queryInImpl.SetListSourceTypesHandler(func(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateSourceTypeListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common.ServiceRequestResult

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.Data)
	assert.Equal(t, http.StatusOK, result.Status.Code)
	templateSourceTypes := result.Data.([]interface{})
	assert.EqualValues(t, len(expectedResults), len(templateSourceTypes))

	firstElem := cacao_common_service.TemplateSourceType(templateSourceTypes[0].(string))
	assert.Equal(t, types.TemplateSourceTypeGit, firstElem)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}
