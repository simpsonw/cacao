package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func TestTemplateListQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	testTime := time.Now().UTC()

	expectedResults := []types.Template{
		{
			ID:          "0001",
			Owner:       testUser,
			Name:        "test_template1",
			Description: "test_template_description1",
			Public:      true,
			Source: cacao_common_service.TemplateSource{
				Type: types.TemplateSourceTypeGit,
				URI:  "https://github.com/cyverse/tf-openstack-single-image",
				AccessParameters: map[string]interface{}{
					"branch": "master",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
			},
			Metadata: cacao_common_service.TemplateMetadata{
				Name:             "single image openstack instances",
				Author:           "Edwin Skidmore",
				AuthorEmail:      "edwin@cyverse.org",
				Description:      "launches a single image on a single cloud",
				TemplateTypeName: "openstack_terraform",
			},
			CreatedAt: testTime,
			UpdatedAt: testTime,
		},
		{
			ID:          "0002",
			Owner:       testUser,
			Name:        "test_template2",
			Description: "test_template_description2",
			Public:      false,
			Source: cacao_common_service.TemplateSource{
				Type: types.TemplateSourceTypeGit,
				URI:  "https://github.com/cyverse/tf-openstack-single-image",
				AccessParameters: map[string]interface{}{
					"branch": "master",
				},
				Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
			},
			Metadata: cacao_common_service.TemplateMetadata{
				Name:             "single image openstack instances",
				Author:           "Edwin Skidmore",
				AuthorEmail:      "edwin@cyverse.org",
				Description:      "launches a single image on a single cloud",
				TemplateTypeName: "openstack_terraform",
			},
			CreatedAt: testTime,
			UpdatedAt: testTime,
		},
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.Template, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common.ServiceRequestResult

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.Data)
	assert.Equal(t, http.StatusOK, result.Status.Code)
	assert.EqualValues(t, len(expectedResults), len(result.Data.([]interface{})))

	firstElem := result.Data.([]interface{})[0]
	firstElemMap := firstElem.(map[string]interface{})
	assert.Equal(t, "0001", firstElemMap["id"])             // field 'ID' goes to json 'id' field
	assert.Equal(t, "test_template1", firstElemMap["name"]) // field 'Name' goes to json 'name' field

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTemplateListQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.Template, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return nil, fmt.Errorf("unable to list templates")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common.ServiceRequestResult

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.Data)
	assert.Equal(t, http.StatusInternalServerError, result.Status.Code)
	assert.NotEmpty(t, result.Status.Message)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTemplateGetQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	queryData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testTemplateID,
	}

	testTime := time.Now().UTC()

	expectedResult := types.Template{
		ID:          testTemplateID,
		Owner:       testUser,
		Name:        "test_template1",
		Description: "test_template_description1",
		Public:      true,
		Source: cacao_common_service.TemplateSource{
			Type: types.TemplateSourceTypeGit,
			URI:  "https://github.com/cyverse/tf-openstack-single-image",
			AccessParameters: map[string]interface{}{
				"branch": "master",
			},
			Visibility: cacao_common_service.TemplateSourceVisibilityPrivate,
		},
		Metadata: cacao_common_service.TemplateMetadata{
			Name:             "single image openstack instances",
			Author:           "Edwin Skidmore",
			AuthorEmail:      "edwin@cyverse.org",
			Description:      "launches a single image on a single cloud",
			TemplateTypeName: "openstack_terraform",
		},
		CreatedAt: testTime,
		UpdatedAt: testTime,
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, templateID cacao_common.ID) (types.Template, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, templateID)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common.ServiceRequestResult

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.Data)
	assert.Equal(t, http.StatusOK, result.Status.Code)

	elem := result.Data
	elemMap := elem.(map[string]interface{})
	assert.Equal(t, testTemplateID.String(), elemMap["id"]) // field 'ID' goes to json 'id' field
	assert.Equal(t, "test_template1", elemMap["name"])      // field 'Name' goes to json 'name' field

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTemplateGetQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testTemplateID := cacao_common_service.NewTemplateID()

	queryData := cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testTemplateID,
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, templateID cacao_common.ID) (types.Template, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testTemplateID, templateID)
		return types.Template{}, fmt.Errorf("unable to get a template")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TemplateGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common.ServiceRequestResult

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.Data)
	assert.Equal(t, http.StatusInternalServerError, result.Status.Code)
	assert.NotEmpty(t, result.Status.Message)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}
