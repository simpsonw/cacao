package adapters

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func (adapter *EventAdapter) handleTemplateImportRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateImportRequest",
	})

	var importRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &importRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateModel")
		return fmt.Errorf("unable to unmarshal JSON bytes into TemplateModel")
	}

	template := types.ConvertFromModel(importRequest)

	err = adapter.IncomingPort.Import(importRequest.GetSessionActor(), importRequest.GetSessionEmulator(), template, importRequest.CredentialID, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateDeleteRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateDeleteRequest",
	})

	var deleteRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &deleteRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateModel")
		return fmt.Errorf("unable to unmarshal JSON bytes into TemplateModel")
	}

	template := types.ConvertFromModel(deleteRequest)

	err = adapter.IncomingPort.Delete(deleteRequest.GetSessionActor(), deleteRequest.GetSessionEmulator(), template, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateUpdateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateUpdateRequest",
	})

	var updateRequestMap map[string]interface{}
	err := json.Unmarshal(jsonData, &updateRequestMap)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into map")
		return fmt.Errorf("unable to unmarshal JSON bytes into map")
	}

	var updateRequest cacao_common_service.TemplateModel
	err = json.Unmarshal(jsonData, &updateRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateModel")
		return fmt.Errorf("unable to unmarshal JSON bytes into TemplateModel")
	}

	// check which fields to update
	updateFieldNames := []string{}
	if _, ok := updateRequestMap["name"]; ok {
		// has ok
		updateFieldNames = append(updateFieldNames, "name")
	}

	if _, ok := updateRequestMap["description"]; ok {
		// has ok
		updateFieldNames = append(updateFieldNames, "description")
	}

	if _, ok := updateRequestMap["public"]; ok {
		// has ok
		updateFieldNames = append(updateFieldNames, "public")
	}

	if _, ok := updateRequestMap["source"]; ok {
		// has ok
		updateFieldNames = append(updateFieldNames, "source")
	}

	template := types.ConvertFromModel(updateRequest)

	err = adapter.IncomingPort.Update(updateRequest.GetSessionActor(), updateRequest.GetSessionEmulator(), template, updateFieldNames, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateSyncRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateSyncRequest",
	})

	var syncRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &syncRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateModel")
		return fmt.Errorf("unable to unmarshal JSON bytes into TemplateModel")
	}

	template := types.ConvertFromModel(syncRequest)

	err = adapter.IncomingPort.Sync(syncRequest.GetSessionActor(), syncRequest.GetSessionEmulator(), template, syncRequest.CredentialID, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

// Implement OutgoingEventPort

// Imported ...
func (adapter *EventAdapter) Imported(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Imported",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateImportedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateImportedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateImportedEvent)
	}

	return nil
}

// ImportFailed ...
func (adapter *EventAdapter) ImportFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.ImportFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       fmt.Sprintf("%d", status.Code),
		ErrorMessage:    status.Message,
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateImportFailedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateImportFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateImportFailedEvent)
	}

	return nil
}

// Updated ...
func (adapter *EventAdapter) Updated(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Updated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateUpdatedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdatedEvent)
	}

	return nil
}

// UpdateFailed ...
func (adapter *EventAdapter) UpdateFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.UpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       fmt.Sprintf("%d", status.Code),
		ErrorMessage:    status.Message,
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateUpdateFailedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdateFailedEvent)
	}

	return nil
}

// Deleted ...
func (adapter *EventAdapter) Deleted(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Deleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateDeletedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateDeletedEvent)
	}

	return nil
}

// DeleteFailed ...
func (adapter *EventAdapter) DeleteFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.DeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       fmt.Sprintf("%d", status.Code),
		ErrorMessage:    status.Message,
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateDeleteFailedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateDeleteFailedEvent)
	}

	return nil
}

// Synced ...
func (adapter *EventAdapter) Synced(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Synced",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateSyncedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncedEvent)
	}

	return nil
}

// SyncFailed ...
func (adapter *EventAdapter) SyncFailed(actor string, emulator string, template types.Template, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.SyncFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       fmt.Sprintf("%d", status.Code),
		ErrorMessage:    status.Message,
	}

	model := types.ConvertToModel(session, template)

	response := cacao_common.ServiceRequestResult{
		Data:   model,
		Status: status,
	}

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateSyncFailedEvent, response, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncFailedEvent)
	}

	return nil
}
