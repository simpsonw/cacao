package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func (adapter *QueryAdapter) handleTemplateListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateListQuery",
	})

	var listRequest cacao_common_service.Session
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into Session")
		return nil, fmt.Errorf("unable to unmarshal JSON bytes into Session")
	}

	listResult, err := adapter.IncomingPort.List(listRequest.GetSessionActor(), listRequest.SessionEmulator)
	var templateListResponse cacao_common.ServiceRequestResult

	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		models := []cacao_common_service.TemplateModel{}
		for _, template := range listResult {
			model := types.ConvertToModel(session, template)
			models = append(models, model)
		}

		templateListResponse = cacao_common.ServiceRequestResult{
			Data: models,
			Status: cacao_common.HTTPStatus{
				Message: "listed templates successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		logger.Error(err)

		templateListResponse = cacao_common.ServiceRequestResult{
			Status: cacao_common.HTTPStatus{
				Message: err.Error(),
				Code:    http.StatusInternalServerError,
			},
		}
	}

	resBytes, err := json.Marshal(templateListResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleTemplateGetQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateGetQuery",
	})

	var getRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateModel")
		return nil, fmt.Errorf("unable to unmarshal JSON bytes into TemplateModel")
	}

	getResult, err := adapter.IncomingPort.Get(getRequest.GetSessionActor(), getRequest.GetSessionEmulator(), getRequest.GetID())
	var templateGetResponse cacao_common.ServiceRequestResult

	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		model := types.ConvertToModel(session, getResult)

		templateGetResponse = cacao_common.ServiceRequestResult{
			Data: model,
			Status: cacao_common.HTTPStatus{
				Message: "got a template successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		if types.IsTemplateNotFoundError(err) {
			templateGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusNotFound,
				},
			}
		} else if types.IsTemplateNotAuthorizedError(err) {
			templateGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusNotFound,
				},
			}
		} else {
			logger.Error(err)

			templateGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusInternalServerError,
				},
			}
		}
	}

	resBytes, err := json.Marshal(templateGetResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}
