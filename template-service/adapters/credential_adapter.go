package adapters

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// CredentialAdapter implements CredentialPort
type CredentialAdapter struct {
	Config *types.Config
}

// Init initialize credential adapter
func (adapter *CredentialAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "CredentialAdapter.Init",
	})

	logger.Info("initializing CredentialAdapter")

	adapter.Config = config
}

// Finalize finalizes credential adapter
func (adapter *CredentialAdapter) Finalize() {
}

// GetTemplateSourceCredential returns template source credential
func (adapter *CredentialAdapter) GetTemplateSourceCredential(actor string, emulator string, credentialID cacao_common.ID) (cacao_common_service.TemplateSourceCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "CredentialAdapter.GetTemplateSourceCredential",
	})

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-outquery" + xid.New().String()

	credentialResult := cacao_common_service.TemplateSourceCredential{}

	credentialClient, err := cacao_common_service.NewNatsCredentialClient(context.TODO(), actor, emulator, natsConfig, adapter.Config.StanConfig)
	if err != nil {
		logger.Error(err)
		return credentialResult, err
	}

	credential, err := credentialClient.Get(actor, credentialID.String())
	if err != nil {
		logger.Error(err)
		return credentialResult, err
	}

	jsonData := []byte(credential.GetValue())
	err = json.Unmarshal(jsonData, &credentialResult)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateSourceCredential")
		return credentialResult, fmt.Errorf("unable to unmarshal JSON bytes into TemplateSourceCredential")
	}

	return credentialResult, nil
}
