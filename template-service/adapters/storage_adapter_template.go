package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// List returns all templates owned by a user
func (adapter *MongoAdapter) List(user string) ([]types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.Template{}

	filter := map[string]interface{}{
		"$or": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"public": true,
			},
		},
	}

	err := adapter.Store.ListConditional(adapter.Config.TemplateMongoDBCollectionName, filter, &results)
	if err != nil {
		logger.WithError(err).Errorf("unable to list templates for the user %s", user)
		return nil, fmt.Errorf("unable to list templates for the user %s", user)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedTemplates []types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	filter := map[string]interface{}{
		"$or": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"public": true,
			},
		},
	}

	mock.On("ListConditional", adapter.Config.TemplateMongoDBCollectionName, filter).Return(expectedTemplates, expectedError)
	return nil
}

// Get returns the template with the ID
func (adapter *MongoAdapter) Get(user string, templateID cacao_common.ID) (types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), &result)
	if err != nil {
		logger.WithError(err).Errorf("unable to get the template for id %s", templateID)
		return result, fmt.Errorf("unable to get the template for id %s", templateID)
	}

	if !result.Public && result.Owner != user {
		// unauthorized
		logger.Errorf("unauthorized access to the template for id %s", templateID)
		return result, types.NewTemplateNotAuthorizedErrorf("unauthorized access to the template for id %s", templateID)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, templateID cacao_common.ID, expectedTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(expectedTemplate, expectedError)
	return nil
}

// Create inserts a template
func (adapter *MongoAdapter) Create(template types.Template) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.TemplateMongoDBCollectionName, template)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			logger.WithError(err).Errorf("unable to insert a template because id %s conflicts", template.ID)
			return types.NewTemplateConflictErrorf("unable to insert a template because id %s conflicts", template.ID)
		}

		logger.WithError(err).Errorf("unable to insert a template with id %s", template.ID)
		return fmt.Errorf("unable to insert a template with id %s", template.ID)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(template types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TemplateMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a template
func (adapter *MongoAdapter) Update(template types.Template, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// get and check ownership
	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, template.ID.String(), &result)
	if err != nil {
		logger.WithError(err).Errorf("unable to get the template for id %s", template.ID)
		return types.NewTemplateNotFoundErrorf("unable to get the template for id %s", template.ID)
	}

	if result.Owner != template.Owner {
		// unauthorized
		logger.Errorf("unauthorized access to the template for id %s", template.ID)
		return types.NewTemplateNotAuthorizedErrorf("unauthorized access to the template for id %s", template.ID)
	}

	// update
	updated, err := adapter.Store.Update(adapter.Config.TemplateMongoDBCollectionName, template.ID.String(), template, updateFieldNames)
	if err != nil {
		logger.WithError(err).Errorf("unable to update the template for id %s", template.ID)
		return fmt.Errorf("unable to update the template for id %s", template.ID)
	}

	if !updated {
		logger.Errorf("unable to update the template for id %s", template.ID)
		return types.NewTemplateNotFoundErrorf("unable to update the template for id %s", template.ID)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingTemplate types.Template, newTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, newTemplate.ID.String()).Return(existingTemplate, expectedError)
	mock.On("Update", adapter.Config.TemplateMongoDBCollectionName, newTemplate.ID.String()).Return(expectedResult, expectedError)
	return nil
}

// Delete deletes a template
func (adapter *MongoAdapter) Delete(user string, templateID cacao_common.ID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// get and check ownership
	result := types.Template{}

	err := adapter.Store.Get(adapter.Config.TemplateMongoDBCollectionName, templateID.String(), &result)
	if err != nil {
		logger.WithError(err).Errorf("unable to get the template for id %s", templateID)
		return types.NewTemplateNotFoundErrorf("unable to get the template for id %s", templateID)
	}

	if result.Owner != user {
		// unauthorized
		logger.Errorf("unauthorized access to the template for id %s", templateID)
		return types.NewTemplateNotAuthorizedErrorf("unauthorized access to the template for id %s", templateID)
	}

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.TemplateMongoDBCollectionName, templateID.String())
	if err != nil {
		logger.WithError(err).Errorf("unable to delete the template for id %s", templateID)
		return fmt.Errorf("unable to delete the template for id %s", templateID)
	}

	if !deleted {
		logger.Errorf("unable to delete the template for id %s", templateID)
		return types.NewTemplateNotFoundErrorf("unable to delete the template for id %s", templateID)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(user string, templateID cacao_common.ID, existingTemplate types.Template, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(existingTemplate, expectedError)
	mock.On("Delete", adapter.Config.TemplateMongoDBCollectionName, templateID.String()).Return(expectedResult, expectedError)
	return nil
}
