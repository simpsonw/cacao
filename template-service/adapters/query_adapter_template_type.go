package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func (adapter *QueryAdapter) handleTemplateTypeListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateTypeListQuery",
	})

	var listRequest cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateTypeModel")
		return nil, fmt.Errorf("unable to unmarshal JSON bytes into TemplateTypeModel")
	}

	// check if the request has provider type filter
	var listTypesResult []types.TemplateType
	if len(listRequest.ProviderTypes) > 0 {
		providerTypeFilter := listRequest.ProviderTypes[0]
		listTypesResult, err = adapter.IncomingPort.ListTypesForProviderType(listRequest.GetSessionActor(), listRequest.SessionEmulator, providerTypeFilter)
	} else {
		listTypesResult, err = adapter.IncomingPort.ListTypes(listRequest.GetSessionActor(), listRequest.SessionEmulator)
	}

	var templateTypeListResponse cacao_common.ServiceRequestResult

	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		models := []cacao_common_service.TemplateTypeModel{}
		for _, templateType := range listTypesResult {
			model := types.ConvertTypeToModel(session, templateType)
			models = append(models, model)
		}

		templateTypeListResponse = cacao_common.ServiceRequestResult{
			Data: models,
			Status: cacao_common.HTTPStatus{
				Message: "listed template types successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		logger.Error(err)

		templateTypeListResponse = cacao_common.ServiceRequestResult{
			Status: cacao_common.HTTPStatus{
				Message: err.Error(),
				Code:    http.StatusInternalServerError,
			},
		}
	}

	resBytes, err := json.Marshal(templateTypeListResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleTemplateTypeGetQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateTypeGetQuery",
	})

	var getRequest cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal JSON bytes into TemplateTypeModel")
		return nil, fmt.Errorf("unable to unmarshal JSON bytes into TemplateTypeModel")
	}

	getResult, err := adapter.IncomingPort.GetType(getRequest.GetSessionActor(), getRequest.GetSessionEmulator(), getRequest.GetName())
	var templateTypeGetResponse cacao_common.ServiceRequestResult

	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		model := types.ConvertTypeToModel(session, getResult)

		templateTypeGetResponse = cacao_common.ServiceRequestResult{
			Data: model,
			Status: cacao_common.HTTPStatus{
				Message: "Got a template successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		if types.IsTemplateTypeNotFoundError(err) {
			templateTypeGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusNotFound,
				},
			}
		} else if types.IsTemplateTypeNotAuthorizedError(err) {
			templateTypeGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusUnauthorized,
				},
			}
		} else {
			logger.WithFields(log.Fields{"error": err}).Error("unable to handle the request")
			templateTypeGetResponse = cacao_common.ServiceRequestResult{
				Status: cacao_common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusInternalServerError,
				},
			}
		}
	}

	resBytes, err := json.Marshal(templateTypeGetResponse)
	if err != nil {
		logger.WithError(err).Error("unable to marshal ServiceRequestResult into JSON bytes")
		return nil, fmt.Errorf("unable to marshal ServiceRequestResult into JSON bytes")
	}

	return resBytes, nil
}
