package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// ListTypesHandler is a handler for ListTypes query
type ListTypesHandler func(actor string, emulator string) ([]types.TemplateType, error)

// ListTypesForProviderTypeHandler is a handler for ListTypesForProviderType query
type ListTypesForProviderTypeHandler func(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error)

// GetTypeHandler is a handler for GetType query
type GetTypeHandler func(actor string, emulator string, typeName string) (types.TemplateType, error)

/////////////////////////////////////////////////////////////////////////
// TemplateSourceType
/////////////////////////////////////////////////////////////////////////

// ListSourceTypesHandler is a handler for ListSourceTypes query
type ListSourceTypesHandler func(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error)

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// ListHandler is a handler for List query
type ListHandler func(actor string, emulator string) ([]types.Template, error)

// GetHandler is a handler for Get query
type GetHandler func(actor string, emulator string, templateID cacao_common.ID) (types.Template, error)

// MockIncomingQueryPort is a mock implementation of IncomingQueryPort
type MockIncomingQueryPort struct {
	Config *types.Config

	// TemplateType
	ListTypesHandler                ListTypesHandler
	ListTypesForProviderTypeHandler ListTypesForProviderTypeHandler
	GetTypeHandler                  GetTypeHandler

	// TemplateSourceType
	ListSourceTypesHandler ListSourceTypesHandler

	// Template
	ListHandler ListHandler
	GetHandler  GetHandler
}

// Init inits the port
func (port *MockIncomingQueryPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingQueryPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingQueryPort) InitChannel(channel chan types.TemplateChannelRequest) {}

// Start starts the port
func (port *MockIncomingQueryPort) Start() {}

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// SetListTypesHandler sets a handler for ListTypes query
func (port *MockIncomingQueryPort) SetListTypesHandler(listTypesHandler ListTypesHandler) {
	port.ListTypesHandler = listTypesHandler
}

// SetListTypesForProviderTypeHandler sets a handler for ListTypesForProviderType query
func (port *MockIncomingQueryPort) SetListTypesForProviderTypeHandler(listTypesForProviderTypeHandler ListTypesForProviderTypeHandler) {
	port.ListTypesForProviderTypeHandler = listTypesForProviderTypeHandler
}

// SetGetTypeHandler sets a handler for GetType query
func (port *MockIncomingQueryPort) SetGetTypeHandler(getTypeHandler GetTypeHandler) {
	port.GetTypeHandler = getTypeHandler
}

// ListTypes lists template types
func (port *MockIncomingQueryPort) ListTypes(actor string, emulator string) ([]types.TemplateType, error) {
	return port.ListTypesHandler(actor, emulator)
}

// ListTypesForProviderType lists template types for the given provider type
func (port *MockIncomingQueryPort) ListTypesForProviderType(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
	return port.ListTypesForProviderTypeHandler(actor, emulator, providerType)
}

// GetType returns a template type
func (port *MockIncomingQueryPort) GetType(actor string, emulator string, typeName string) (types.TemplateType, error) {
	return port.GetTypeHandler(actor, emulator, typeName)
}

/////////////////////////////////////////////////////////////////////////
// TemplateSourceType
/////////////////////////////////////////////////////////////////////////

// SetListSourceTypesHandler sets a handler for ListSourceTypes query
func (port *MockIncomingQueryPort) SetListSourceTypesHandler(listSourceTypesHandler ListSourceTypesHandler) {
	port.ListSourceTypesHandler = listSourceTypesHandler
}

// ListSourceTypes lists template types
func (port *MockIncomingQueryPort) ListSourceTypes(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error) {
	return port.ListSourceTypesHandler(actor, emulator)
}

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// SetListHandler sets a handler for List query
func (port *MockIncomingQueryPort) SetListHandler(listHandler ListHandler) {
	port.ListHandler = listHandler
}

// SetGetHandler sets a handler for Get query
func (port *MockIncomingQueryPort) SetGetHandler(getHandler GetHandler) {
	port.GetHandler = getHandler
}

// List lists templates
func (port *MockIncomingQueryPort) List(actor string, emulator string) ([]types.Template, error) {
	return port.ListHandler(actor, emulator)
}

// Get returns a template
func (port *MockIncomingQueryPort) Get(actor string, emulator string, templateID cacao_common.ID) (types.Template, error) {
	return port.GetHandler(actor, emulator, templateID)
}
