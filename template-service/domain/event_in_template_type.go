package domain

import (
	"fmt"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// CreateType creates a template type
func (impl *EventPortImpl) CreateType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return fmt.Errorf("input validation error: actor is empty")
	}

	if len(templateType.Name) == 0 {
		return fmt.Errorf("input validation error: name is empty")
	}

	if len(templateType.Engine) == 0 {
		return fmt.Errorf("input validation error: engine is empty")
	}

	if len(templateType.Formats) == 0 {
		return fmt.Errorf("input validation error: formats are empty")
	}

	if len(templateType.ProviderTypes) == 0 {
		return fmt.Errorf("input validation error: provider types are empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	templateTypeRequest := types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TemplateType{
			Name:          templateType.Name,
			Formats:       templateType.Formats,
			Engine:        templateType.Engine,
			ProviderTypes: templateType.ProviderTypes,
		},
		Operation:     string(cacao_common_service.TemplateTypeCreateRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- templateTypeRequest

	// receive response
	templateTypeResponse := <-responseChannel
	return templateTypeResponse.Error
}

// UpdateType updates the template type
func (impl *EventPortImpl) UpdateType(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return fmt.Errorf("input validation error: actor is empty")
	}

	if len(templateType.Name) == 0 {
		return fmt.Errorf("input validation error: name is empty")
	}

	if len(templateType.Engine) == 0 {
		return fmt.Errorf("input validation error: engine is empty")
	}

	if len(templateType.Formats) == 0 {
		return fmt.Errorf("input validation error: formats are empty")
	}

	if len(templateType.ProviderTypes) == 0 {
		return fmt.Errorf("input validation error: provider types are empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	templateTypeRequest := types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TemplateType{
			Name:          templateType.Name,
			Formats:       templateType.Formats,
			Engine:        templateType.Engine,
			ProviderTypes: templateType.ProviderTypes,
		},
		Operation:     string(cacao_common_service.TemplateTypeUpdateRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- templateTypeRequest

	// receive response
	templateTypeResponse := <-responseChannel
	return templateTypeResponse.Error
}

// DeleteType deletes the template type
func (impl *EventPortImpl) DeleteType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return fmt.Errorf("input validation error: actor is empty")
	}

	if len(templateType.Name) == 0 {
		return fmt.Errorf("input validation error: name is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	templateTypeRequest := types.TemplateChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TemplateType{
			Name: templateType.Name,
		},
		Operation:     string(cacao_common_service.TemplateTypeDeleteRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- templateTypeRequest

	// receive response
	templateTypeResponse := <-responseChannel
	return templateTypeResponse.Error
}
