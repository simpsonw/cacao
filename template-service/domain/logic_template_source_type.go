package domain

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTemplateSourceTypes returns all TemplateSourceTypes
func (d *Domain) ListTemplateSourceTypes(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error) {
	return []cacao_common_service.TemplateSourceType{
		types.TemplateSourceTypeGit,
	}, nil
}
