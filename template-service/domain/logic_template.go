package domain

import (
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTemplates returns templates owned by a user
func (d *Domain) ListTemplates(actor string, emulator string) ([]types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.ListTemplates",
	})

	templates, err := d.Storage.List(actor)
	if err != nil {
		logger.WithError(err).Errorf("failed to list templates for the owner %s", actor)
		return nil, fmt.Errorf("failed to list templates for the owner %s", actor)
	}

	return templates, nil
}

// GetTemplate returns the template with the ID
func (d *Domain) GetTemplate(actor string, emulator string, templateID cacao_common.ID) (types.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.GetTemplate",
	})

	template, err := d.Storage.Get(actor, templateID)
	if err != nil {
		if types.IsTemplateNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the template for id %s", templateID)
			return types.Template{}, types.NewTemplateNotFoundErrorf("unable to find the template for id %s", templateID)
		} else if types.IsTemplateNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the template for id %s is not authorized", templateID)
			return types.Template{}, types.NewTemplateNotAuthorizedErrorf("access to the template for id %s is not authorized", templateID)
		} else {
			logger.WithError(err).Errorf("failed to get the template for id %s", templateID)
			return types.Template{}, fmt.Errorf("failed to get the template for id %s", templateID)
		}
	}

	return template, nil
}

// ImportTemplate imports a template
func (d *Domain) ImportTemplate(actor string, emulator string, template types.Template, credentialID cacao_common.ID, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.ImportTemplate",
	})

	// Import
	templateMetadata, err := d.importTemplateMetadata(actor, emulator, template.Source, credentialID)
	if err != nil {
		importFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		err2 := d.EventOut.ImportFailed(actor, emulator, importFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an ImportFailed event")
		}

		logger.WithError(err).Errorf("failed to import a template for id %s", template.ID)
		return fmt.Errorf("failed to import a template for id %s", template.ID)
	}

	template.Metadata = templateMetadata

	err = d.Storage.Create(template)
	if err != nil {
		importFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsTemplateNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		}

		err2 := d.EventOut.ImportFailed(actor, emulator, importFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an ImportFailed event")
		}

		logger.WithError(err).Errorf("failed to import a template for id %s", template.ID)
		return fmt.Errorf("failed to import a template for id %s", template.ID)
	}

	// output event
	importedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor,
		Name:      template.Name,
		CreatedAt: template.CreatedAt,
	}

	status := cacao_common.HTTPStatus{
		Message: "imported a template successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.Imported(actor, emulator, importedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Imported event")
	}

	return nil
}

// UpdateTemplate updates the template
func (d *Domain) UpdateTemplate(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.UpdateTemplate",
	})

	err := d.Storage.Update(template, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name, // may be empty
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsTemplateNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		} else if types.IsTemplateNotFoundError(err) {
			status.Code = http.StatusNotFound
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.WithError(err).Errorf("failed to update the template for id %s", template.ID)
		return fmt.Errorf("failed to update the template for id %s", template.ID)
	}

	// get the final result
	updatedTemplate, err := d.Storage.Get(actor, template.ID)
	if err != nil {
		// update failed somehow - imported but not exist
		logger.WithError(err).Errorf("failed to update the template for id %s", template.ID)

		updateFailedEvent := types.Template{
			ID:    template.ID,
			Owner: actor,
			Name:  template.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		if types.IsTemplateNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the template for id %s", template.ID)
			return types.NewTemplateNotFoundErrorf("unable to find the template for id %s", template.ID)
		} else if types.IsTemplateNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the template for id %s is not authorized", template.ID)
			return types.NewTemplateNotAuthorizedErrorf("access to the template for id %s is not authorized", template.ID)
		} else {
			logger.WithError(err).Errorf("failed to update the template for id %s", template.ID)
			return fmt.Errorf("failed to update the template for id %s", template.ID)
		}
	}

	// output event
	updatedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor,
		Name:      updatedTemplate.Name,
		UpdatedAt: updatedTemplate.UpdatedAt,
	}

	status := cacao_common.HTTPStatus{
		Message: "updated the template successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.Updated(actor, emulator, updatedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Updated event")
	}

	return nil
}

// DeleteTemplate deletes the template
func (d *Domain) DeleteTemplate(actor string, emulator string, templateID cacao_common.ID, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.DeleteTemplate",
	})

	// get the template first
	template, err := d.Storage.Get(actor, templateID)
	if err != nil {
		// not exist
		logger.WithError(err).Errorf("failed to delete the template for id %s", templateID)

		deleteFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  "", // unknown
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateNotFoundError(err) {
			status.Code = http.StatusNotFound
		} else if types.IsTemplateNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		if types.IsTemplateNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the template for id %s", templateID)
			return types.NewTemplateNotFoundErrorf("unable to find the template for id %s", templateID)
		} else if types.IsTemplateNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the template for id %s is not authorized", templateID)
			return types.NewTemplateNotAuthorizedErrorf("access to the template for id %s is not authorized", templateID)
		} else {
			logger.WithError(err).Errorf("failed to delete the template for id %s", templateID)
			return types.NewTemplateNotFoundErrorf("failed to delete the template for id %s", templateID)
		}
	}

	err = d.Storage.Delete(actor, templateID)
	if err != nil {
		deleteFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  template.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsTemplateNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		} else if types.IsTemplateNotFoundError(err) {
			status.Code = http.StatusNotFound
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.WithError(err).Errorf("failed to delete the template for id %s", templateID)
		return fmt.Errorf("failed to delete the template for id %s", templateID)
	}

	// output event
	deletedEvent := types.Template{
		ID:    templateID,
		Owner: actor,
		Name:  template.Name,
	}

	status := cacao_common.HTTPStatus{
		Message: "deleted a template successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.Deleted(actor, emulator, deletedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deleted event")
	}

	return nil
}

// SyncTemplate syncs the template
func (d *Domain) SyncTemplate(actor string, emulator string, templateID cacao_common.ID, credentialID cacao_common.ID, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.SyncTemplate",
	})

	now := time.Now().UTC()

	// get the template
	template, err := d.Storage.Get(actor, templateID)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  "", // unknown
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateNotFoundError(err) {
			status.Code = http.StatusNotFound
		} else if types.IsTemplateNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		}

		err2 := d.EventOut.SyncFailed(actor, emulator, syncFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an SyncFailed event")
		}

		if types.IsTemplateNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the template for id %s", templateID)
			return types.NewTemplateNotFoundErrorf("unable to find the template for id %s", templateID)
		} else if types.IsTemplateNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the template for id %s is not authorized", templateID)
			return types.NewTemplateNotAuthorizedErrorf("access to the template for id %s is not authorized", templateID)
		} else {
			logger.WithError(err).Errorf("failed to sync the template for id %s", templateID)
			return fmt.Errorf("failed to sync the template for id %s", templateID)
		}
	}

	// Import
	templateMetadata, err := d.importTemplateMetadata(actor, emulator, template.Source, credentialID)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  template.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		err2 := d.EventOut.SyncFailed(actor, emulator, syncFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an SyncFailed event")
		}

		logger.WithError(err).Errorf("failed to sync a template for id %s", templateID)
		return fmt.Errorf("failed to sync a template for id %s", templateID)
	}

	template.Metadata = templateMetadata
	template.UpdatedAt = now

	updateFields := []string{
		"metadata", "updated_at",
	}

	err = d.Storage.Update(template, updateFields)
	if err != nil {
		syncFailedEvent := types.Template{
			ID:    templateID,
			Owner: actor,
			Name:  template.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsTemplateNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		} else if types.IsTemplateNotFoundError(err) {
			status.Code = http.StatusNotFound
		}

		err2 := d.EventOut.SyncFailed(actor, emulator, syncFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an SyncFailed event")
		}

		logger.WithError(err).Errorf("failed to sync a template for id %s", templateID)
		return fmt.Errorf("failed to sync a template for id %s", templateID)
	}

	// output event
	syncedEvent := types.Template{
		ID:        template.ID,
		Owner:     actor,
		Name:      template.Name,
		UpdatedAt: template.UpdatedAt,
	}

	status := cacao_common.HTTPStatus{
		Message: "synced a template successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.Synced(actor, emulator, syncedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Synced event")
	}

	return nil
}

func (d *Domain) importTemplateMetadata(actor string, emulator string, source cacao_common_service.TemplateSource, credentialID cacao_common.ID) (cacao_common_service.TemplateMetadata, error) {
	credential, err := d.Credential.GetTemplateSourceCredential(actor, emulator, credentialID)
	if err != nil {
		return cacao_common_service.TemplateMetadata{}, err
	}

	return d.TemplateSource.GetTemplateMetadata(source, credential)
}
