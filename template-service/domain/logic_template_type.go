package domain

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTemplateTypes returns all template types
func (d *Domain) ListTemplateTypes(actor string, emulator string) ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.ListTemplateTypes",
	})

	templateTypes, err := d.Storage.ListTypes()
	if err != nil {
		logger.WithError(err).Errorf("failed to list template types")
		return nil, fmt.Errorf("failed to list template types")
	}

	return templateTypes, nil
}

// ListTemplateTypesForProviderType returns all template types that supports the given provider type
func (d *Domain) ListTemplateTypesForProviderType(actor string, emulator string, templateProviderType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.ListTemplateTypesForProviderType",
	})

	templateTypes, err := d.Storage.ListTypesForProviderType(templateProviderType)
	if err != nil {
		logger.WithError(err).Errorf("failed to list template types for provider type %s", templateProviderType)
		return nil, fmt.Errorf("failed to list template types for provider type %s", templateProviderType)
	}

	return templateTypes, nil
}

// GetTemplateType returns the template type with the name
func (d *Domain) GetTemplateType(actor string, emulator string, templateTypeName string) (types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.GetTemplateType",
	})

	templateType, err := d.Storage.GetType(templateTypeName)
	if err != nil {
		if types.IsTemplateTypeNotFoundError(err) {
			logger.WithError(err).Errorf("unable to find the template type for name %s", templateTypeName)
			return types.TemplateType{}, types.NewTemplateNotFoundErrorf("unable to find the template type for name %s", templateTypeName)
		} else if types.IsTemplateTypeNotAuthorizedError(err) {
			logger.WithError(err).Errorf("access to the template type for name %s is not authorized", templateTypeName)
			return types.TemplateType{}, types.NewTemplateNotAuthorizedErrorf("access to the template type for name %s is not authorized", templateTypeName)
		} else {
			logger.WithError(err).Errorf("failed to get the template type for name %s", templateTypeName)
			return types.TemplateType{}, fmt.Errorf("failed to get the template type for name %s", templateTypeName)
		}
	}

	return templateType, nil
}

// CreateTemplateType creates a template type
func (d *Domain) CreateTemplateType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.CreateTemplateType",
	})

	err := d.Storage.CreateType(templateType)
	if err != nil {
		createFailedEvent := types.TemplateType{
			Name: templateType.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateTypeConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsTemplateTypeNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		}

		err2 := d.EventOut.TypeCreateFailed(actor, emulator, createFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.WithError(err).Errorf("failed to create a template type for name %s", templateType.Name)
		return fmt.Errorf("failed to create a template type for name %s", templateType.Name)
	}

	// output event
	createdEvent := types.TemplateType{
		Name: templateType.Name,
	}

	status := cacao_common.HTTPStatus{
		Message: "created a template type successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.TypeCreated(actor, emulator, createdEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("Failed to send a Created event")
	}

	return nil
}

// UpdateTemplateType updates the template type
func (d *Domain) UpdateTemplateType(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.UpdateTemplateType",
	})

	err := d.Storage.UpdateType(templateType, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.TemplateType{
			Name: templateType.Name,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateTypeConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsTemplateTypeNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		} else if types.IsTemplateTypeNotFoundError(err) {
			status.Code = http.StatusNotFound
		}

		err2 := d.EventOut.TypeUpdateFailed(actor, emulator, updateFailedEvent, status, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.WithError(err).Errorf("failed to update the template type for name %s", templateType.Name)
		return fmt.Errorf("failed to update the template type for name %s", templateType.Name)
	}

	// output event
	updatedEvent := types.TemplateType{
		Name: templateType.Name,
	}

	status := cacao_common.HTTPStatus{
		Message: "updated the template type successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.TypeUpdated(actor, emulator, updatedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Updated event")
	}

	return nil
}

// DeleteTemplateType deletes the template type
func (d *Domain) DeleteTemplateType(actor string, emulator string, templateTypeName string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "domain.DeleteTemplateType",
	})

	err := d.Storage.DeleteType(templateTypeName)
	if err != nil {
		deleteFailedEvent := types.TemplateType{
			Name: templateTypeName,
		}

		status := cacao_common.HTTPStatus{
			Message: err.Error(),
			Code:    http.StatusInternalServerError,
		}

		if types.IsTemplateTypeConflictError(err) {
			status.Code = http.StatusConflict
		} else if types.IsTemplateTypeNotAuthorizedError(err) {
			status.Code = http.StatusUnauthorized
		} else if types.IsTemplateTypeNotFoundError(err) {
			status.Code = http.StatusNotFound
		}

		err2 := d.EventOut.TypeDeleteFailed(actor, emulator, deleteFailedEvent, status, transactionID)
		if err != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.WithError(err).Errorf("failed to delete the template type for name %s", templateTypeName)
		return fmt.Errorf("failed to delete the template type for name %s", templateTypeName)
	}

	// output event
	deletedEvent := types.TemplateType{
		Name: templateTypeName,
	}

	status := cacao_common.HTTPStatus{
		Message: "deleted the template type successfully",
		Code:    http.StatusAccepted,
	}

	err = d.EventOut.TypeDeleted(actor, emulator, deletedEvent, status, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deleted event")
	}

	return nil
}
