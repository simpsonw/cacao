package types

import "fmt"

// TemplateTypeNotFoundError is a struct for template type not found error
type TemplateTypeNotFoundError struct {
	message string
}

// NewTemplateTypeNotFoundError creates TemplateTypeNotFoundError struct
func NewTemplateTypeNotFoundError(message string) *TemplateTypeNotFoundError {
	return &TemplateTypeNotFoundError{
		message: message,
	}
}

// NewTemplateTypeNotFoundErrorf creates TemplateTypeNotFoundError struct
func NewTemplateTypeNotFoundErrorf(format string, v ...interface{}) *TemplateTypeNotFoundError {
	return &TemplateTypeNotFoundError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *TemplateTypeNotFoundError) Error() string {
	return e.message
}

// IsTemplateTypeNotFoundError evaluates if the given error is TemplateTypeNotFoundError
func IsTemplateTypeNotFoundError(err error) bool {
	if _, ok := err.(*TemplateTypeNotFoundError); ok {
		return true
	}

	return false
}

// TemplateTypeNotAuthorizedError is a struct for unauthorized access error
type TemplateTypeNotAuthorizedError struct {
	message string
}

// NewTemplateTypeNotAuthorizedError creates TemplateTypeNotAuthorizedError struct
func NewTemplateTypeNotAuthorizedError(message string) *TemplateTypeNotAuthorizedError {
	return &TemplateTypeNotAuthorizedError{
		message: message,
	}
}

// NewTemplateTypeNotAuthorizedErrorf creates TemplateTypeNotAuthorizedError struct
func NewTemplateTypeNotAuthorizedErrorf(format string, v ...interface{}) *TemplateTypeNotAuthorizedError {
	return &TemplateTypeNotAuthorizedError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *TemplateTypeNotAuthorizedError) Error() string {
	return e.message
}

// IsTemplateTypeNotAuthorizedError evaluates if the given error is TemplateTypeNotAuthorizedError
func IsTemplateTypeNotAuthorizedError(err error) bool {
	if _, ok := err.(*TemplateTypeNotAuthorizedError); ok {
		return true
	}

	return false
}

// TemplateTypeConflictError is a struct for conflict error
type TemplateTypeConflictError struct {
	message string
}

// NewTemplateTypeConflictError creates TemplateTypeConflictError struct
func NewTemplateTypeConflictError(message string) *TemplateTypeConflictError {
	return &TemplateTypeConflictError{
		message: message,
	}
}

// NewTemplateTypeConflictErrorf creates TemplateTypeConflictError struct
func NewTemplateTypeConflictErrorf(format string, v ...interface{}) *TemplateTypeConflictError {
	return &TemplateTypeConflictError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *TemplateTypeConflictError) Error() string {
	return e.message
}

// IsTemplateTypeConflictError evaluates if the given error is TemplateTypeConflictError
func IsTemplateTypeConflictError(err error) bool {
	if _, ok := err.(*TemplateTypeConflictError); ok {
		return true
	}

	return false
}

// TemplateNotFoundError is a struct for template not found error
type TemplateNotFoundError struct {
	message string
}

// NewTemplateNotFoundError creates TemplateNotFoundError struct
func NewTemplateNotFoundError(message string) *TemplateNotFoundError {
	return &TemplateNotFoundError{
		message: message,
	}
}

// NewTemplateNotFoundErrorf creates TemplateNotFoundError struct
func NewTemplateNotFoundErrorf(format string, v ...interface{}) *TemplateNotFoundError {
	return &TemplateNotFoundError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *TemplateNotFoundError) Error() string {
	return e.message
}

// IsTemplateNotFoundError evaluates if the given error is TemplateNotFoundError
func IsTemplateNotFoundError(err error) bool {
	if _, ok := err.(*TemplateNotFoundError); ok {
		return true
	}

	return false
}

// TemplateNotAuthorizedError is a struct for unauthorized access error
type TemplateNotAuthorizedError struct {
	message string
}

// NewTemplateNotAuthorizedError creates TemplateNotAuthorizedError struct
func NewTemplateNotAuthorizedError(message string) *TemplateNotAuthorizedError {
	return &TemplateNotAuthorizedError{
		message: message,
	}
}

// NewTemplateNotAuthorizedErrorf creates TemplateNotAuthorizedError struct
func NewTemplateNotAuthorizedErrorf(format string, v ...interface{}) *TemplateNotAuthorizedError {
	return &TemplateNotAuthorizedError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *TemplateNotAuthorizedError) Error() string {
	return e.message
}

// IsTemplateNotAuthorizedError evaluates if the given error is TemplateNotAuthorizedError
func IsTemplateNotAuthorizedError(err error) bool {
	if _, ok := err.(*TemplateNotAuthorizedError); ok {
		return true
	}

	return false
}

// TemplateConflictError is a struct for conflict error
type TemplateConflictError struct {
	message string
}

// NewTemplateConflictError creates TemplateConflictError struct
func NewTemplateConflictError(message string) *TemplateConflictError {
	return &TemplateConflictError{
		message: message,
	}
}

// NewTemplateConflictErrorf creates TemplateConflictError struct
func NewTemplateConflictErrorf(format string, v ...interface{}) *TemplateConflictError {
	return &TemplateConflictError{
		message: fmt.Sprintf(format, v...),
	}
}

// Error returns error message
func (e *TemplateConflictError) Error() string {
	return e.message
}

// IsTemplateConflictError evaluates if the given error is TemplateConflictError
func IsTemplateConflictError(err error) bool {
	if _, ok := err.(*TemplateConflictError); ok {
		return true
	}

	return false
}
