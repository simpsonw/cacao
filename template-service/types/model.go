package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// ConvertTypeFromModel converts TemplateTypeModel to TemplateType
func ConvertTypeFromModel(model cacao_common_service.TemplateTypeModel) TemplateType {
	templateType := TemplateType{
		Name:          model.Name,
		Formats:       model.Formats,
		Engine:        model.Engine,
		ProviderTypes: model.ProviderTypes,
	}

	return templateType
}

// ConvertFromModel converts TemplateModel to Template
func ConvertFromModel(model cacao_common_service.TemplateModel) Template {
	template := Template{
		ID:          model.ID,
		Owner:       model.Owner,
		Name:        model.Name,
		Description: model.Description,
		Public:      model.Public,
		Source:      model.Source,
		Metadata:    model.Metadata,
		CreatedAt:   model.CreatedAt,
		UpdatedAt:   model.UpdatedAt,
	}

	return template
}

// ConvertTypeToModel converts TemplateType to TemplateTypeModel
func ConvertTypeToModel(session cacao_common_service.Session, templateType TemplateType) cacao_common_service.TemplateTypeModel {
	return cacao_common_service.TemplateTypeModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
		},
		Name:          templateType.Name,
		Formats:       templateType.Formats,
		Engine:        templateType.Engine,
		ProviderTypes: templateType.ProviderTypes,
	}
}

// ConvertToModel converts Template to TemplateModel
func ConvertToModel(session cacao_common_service.Session, template Template) cacao_common_service.TemplateModel {
	return cacao_common_service.TemplateModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
		},
		ID:          template.ID,
		Owner:       template.Owner,
		Name:        template.Name,
		Description: template.Description,
		Public:      template.Public,
		Source:      template.Source,
		Metadata:    template.Metadata,
		CreatedAt:   template.CreatedAt,
		UpdatedAt:   template.UpdatedAt,
	}
}
