package types

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// TemplateType is a struct for storing template type information
type TemplateType struct {
	Name          string                                      `bson:"_id" json:"name,omitempty"`
	Formats       []cacao_common_service.TemplateFormat       `bson:"formats" json:"formats,omitempty"`
	Engine        cacao_common_service.TemplateEngine         `bson:"engine" json:"engine"`
	ProviderTypes []cacao_common_service.TemplateProviderType `bson:"provider_types" json:"provider_types,omitempty"`
}

// Template is a struct for storing template information
type Template struct {
	ID          common.ID                             `bson:"_id" json:"id,omitempty"`
	Owner       string                                `bson:"owner" json:"owner,omitempty"`
	Name        string                                `bson:"name" json:"name,omitempty"`
	Description string                                `bson:"description" json:"description,omitempty"`
	Public      bool                                  `bson:"public" json:"public,omitempty"`
	Source      cacao_common_service.TemplateSource   `bson:"source" json:"source,omitempty"`
	Metadata    cacao_common_service.TemplateMetadata `bson:"source" json:"metadata,omitempty"`
	CreatedAt   time.Time                             `bson:"created_at" json:"created_at,omitempty"`
	UpdatedAt   time.Time                             `bson:"updated_at" json:"updated_at,omitempty"`
}
