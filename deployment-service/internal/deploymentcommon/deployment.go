package deploymentcommon

import (
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Creator is the creator of deployments
type Creator struct {
	User     string `bson:"creator"`
	Emulator string `bson:"creator_emulator"`
}

// Deployment is the storage format of deployment
type Deployment struct {
	ID        common.ID `bson:"_id"`
	CreatedAt time.Time `bson:"created_at"`
	UpdatedAt time.Time `bson:"updated_at"`
	Workspace common.ID `bson:"workspace"`
	// user who created the deployment
	CreatedBy            Creator              `bson:",inline"`
	Template             common.ID            `bson:"template"`
	TemplateType         string               `bson:"template_type"`
	PrimaryCloudProvider common.ID            `bson:"primary_provider"`
	CloudCredentials     []common.ID          `bson:"cloud_credentials"`
	GitCredential        common.ID            `bson:"git_credential"`
	Parameters           DeploymentParameters `bson:"parameters"`
	// most recent run sorted by creation timestamp
	LastRun *DeploymentRun `bson:"last_run"`
}

// Owner returns the owner of the deployment.
// Currently owner is the creator.
func (d Deployment) Owner() string {
	return d.CreatedBy.User
}

// ConvertToExternal converts to external representation
func (d Deployment) ConvertToExternal() service.Deployment {
	var lastRun *service.DeploymentRun
	if d.LastRun != nil {
		run := d.LastRun.ConvertToExternal()
		lastRun = &run
	}
	return service.Deployment{
		ID:           d.ID,
		CreatedAt:    d.CreatedAt,
		UpdatedAt:    d.UpdatedAt,
		Owner:        d.Owner(),
		Workspace:    d.Workspace,
		Template:     d.Template,
		TemplateType: d.TemplateType,
		PrimaryCloud: d.PrimaryCloudProvider,
		Parameters:   d.Parameters.ConvertToExternal(),
		LastRun:      lastRun,
	}
}

// DeploymentStatus is the status of a deployment
type DeploymentStatus string

const (
	// DeploymentPreflight ...
	DeploymentPreflight DeploymentStatus = "pre-flight"
	// DeploymentRunning ...
	DeploymentRunning DeploymentStatus = "running"
	// DeploymentActive ...
	DeploymentActive DeploymentStatus = "active"
	// DeploymentErrored ...
	DeploymentErrored DeploymentStatus = "errored"
)

// DeploymentParameters ...
type DeploymentParameters []DeploymentParameter

// ConvertToExternal converts to external representation
func (dp DeploymentParameters) ConvertToExternal() []service.DeploymentParameter {
	external := make([]service.DeploymentParameter, 0)
	for _, param := range dp {
		external = append(external, param.ConvertToExternal())
	}
	return external
}

// DeploymentParameter contain the type and value of a parameter to deployment
type DeploymentParameter struct {
	Name  string      `bson:"name"`
	Type  string      `bson:"type"` // int,string,bool,float,uuid,url,secret, etc.
	Value interface{} `bson:"value"`
}

// ConvertToExternal converts to external representation
func (dp DeploymentParameter) ConvertToExternal() service.DeploymentParameter {
	return service.DeploymentParameter{
		Name:  dp.Name,
		Type:  dp.Type,
		Value: dp.Value,
	}
}

// RawStateType is type of raw state
type RawStateType string

// RawDeploymentState is the raw state of a deployment (run)
type RawDeploymentState struct {
	Type    RawStateType    `bson:"type"`
	TFState *TerraformState `bson:"terraform"`
	//State   interface{}     `bson:"state,omitempty"`
}

// DeploymentRun ...
type DeploymentRun struct {
	// ID of the deployment run
	ID               common.ID            `bson:"_id"`
	Deployment       common.ID            `bson:"deployment"`
	CreatedAt        time.Time            `bson:"created_at"`
	EndsAt           time.Time            `bson:"ended_at"`
	TemplateSnapshot TemplateSnapshot     `bson:"template"`
	Parameters       DeploymentParameters `bson:"parameters"`
	Status           string               `bson:"status"`
	// most recent state
	LastState      DeploymentStateView `bson:"last_state"`
	StateUpdatedAt time.Time           `bson:"state_updated_at"`
	RawState       *RawDeploymentState `bson:"raw_state"`
	Logs           *string             `bson:"logs"`

	// TODO implement history later
	Histories []DeploymentHistory `bson:"histories"`
}

// HasRawState returns true if the run has raw state available
func (r DeploymentRun) HasRawState() bool {
	return r.RawState != nil
}

// HasLogs returns true if the run has logs available
func (r DeploymentRun) HasLogs() bool {
	return r.Logs != nil
}

// ConvertToExternal converts to external representation
func (r DeploymentRun) ConvertToExternal() service.DeploymentRun {
	return service.DeploymentRun{
		ID:             r.ID,
		Deployment:     r.Deployment,
		CreatedAt:      r.CreatedAt,
		EndedAt:        r.EndsAt,
		GitURL:         r.TemplateSnapshot.GitURL,
		CommitHash:     r.TemplateSnapshot.CommitHash,
		Parameters:     r.Parameters.ConvertToExternal(),
		Status:         r.Status,
		LastState:      r.LastState.ConvertToExternal(),
		StateUpdatedAt: r.StateUpdatedAt,
	}
}

// TemplateSnapshot is a snapshot in time of a template.
// It contains info that can be used to identify a specific instance template.
type TemplateSnapshot struct {
	TemplateID      common.ID `bson:"template_id"`
	UpstreamTracked struct {
		Branch string `bson:"branch"`
		Tag    string `bson:"tag"`
	} `bson:",inline"`
	GitURL     string `bson:"git_url"`
	CommitHash string `bson:"git_commit"`
}

// DeploymentHistory is a history record of the state/status of the deployment and its runs.
type DeploymentHistory struct {
	// ID is the id of the history object itself
	ID         common.ID `bson:"_id"`
	Deployment common.ID `bson:"deployment"`
	// The deployment run that this history belongs to
	Run        common.ID            `bson:"run"`
	Timestamp  time.Time            `bson:"timestamp"`
	Parameters DeploymentParameters `bson:"param"`
	StateView  DeploymentStateView  `bson:"state"`
	RawState   *string              `bson:"raw_state"`
	Logs       *string              `bson:"logs"`
}

// HasRawState checks if history has raw state available
func (hist DeploymentHistory) HasRawState() bool {
	return hist.RawState != nil
}

// HasLogs checks if history has logs available
func (hist DeploymentHistory) HasLogs() bool {
	return hist.Logs != nil
}

// DeploymentStateView is a higher level view of raw state
type DeploymentStateView service.DeploymentStateView

// ConvertToExternal converts to external representation
func (sv DeploymentStateView) ConvertToExternal() service.DeploymentStateView {
	return service.DeploymentStateView(sv)
}
