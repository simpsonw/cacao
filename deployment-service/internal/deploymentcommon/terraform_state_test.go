package deploymentcommon

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gotest.tools/v3/assert"
	"testing"
)

func TestTerraformStateParser_ToStateView(t *testing.T) {

	type args struct {
		state TerraformState
	}
	tests := []struct {
		name    string
		args    args
		want    service.DeploymentStateView
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			"",
			args{
				state: TerraformState{
					Version:          0,
					TerraformVersion: "",
					Serial:           0,
					Lineage:          "",
					Outputs:          nil,
					Resources: []TerraformResource{
						{
							Module:     "module.koa",
							Mode:       "managed",
							Type:       openstackComputeInstanceV2,
							Name:       "os_instances",
							TFProvider: "provider[\\\"terraform.cyverse.org/cyverse/openstack\\\"]",
							Instances: []TerraformResourceInstance{
								{
									IndexKey:      0,
									SchemaVersion: 0,
									Attributes: map[string]interface{}{
										"access_ip_v4":            "172.0.0.0",
										"access_ip_v6":            "",
										"admin_pass":              nil,
										"all_metadata":            map[string]interface{}{},
										"all_tags":                []string{},
										"availability_zone":       "nova",
										"availability_zone_hints": nil,
										"block_device":            []interface{}{},
										"config_drive":            nil,
										"flavor_id":               "ffffffff-ffff-ffff-aaaa-aaaaaaaaaaaa",
										"flavor_name":             "tiny1",
										"floating_ip":             nil,
										"force_delete":            false,
										"id":                      "dddddddd-dddd-dddd-dddd-dddddddddddd",
										"image_id":                "aaaaaaaa-aaaa-aaaa-eeee-eeeeeeeeeeee",
										"image_name":              "ubuntu",
										"key_pair":                "myKeyPair",
										"metadata":                nil,
										"name":                    "test_instance",
										"network": []map[string]interface{}{
											{
												"access_network": false,
												"fixed_ip_v4":    "172.0.0.0",
												"fixed_ip_v6":    "",
												"floating_ip":    "",
												"mac":            "aa:aa:aa:aa:cc:cc",
												"name":           "user-api-net",
												"port":           "",
												"uuid":           "eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee",
											},
										},
										"network_mode":        nil,
										"personality":         []interface{}{},
										"power_state":         "active",
										"region":              "Region",
										"scheduler_hints":     []interface{}{},
										"security_groups":     []string{"user-api-sg"},
										"stop_before_destroy": false,
										"tags":                nil,
										"timeouts":            nil,
										"user_data":           nil,
										"vendor_options":      []interface{}{},
										"volume":              []interface{}{},
									},
									SensitiveAttributes: nil,
									Private:             "",
									Dependencies:        nil,
								},
							},
						},
					},
				},
			},
			service.DeploymentStateView{
				Resources: []service.DeploymentResource{
					{
						ID:           "dddddddd-dddd-dddd-dddd-dddddddddddd",
						Type:         service.OpenStackInstance,
						ProviderType: "openstack",
						Provider:     "",
						Attributes: map[string]interface{}{
							"access_ip_v4":            "172.0.0.0",
							"access_ip_v6":            "",
							"admin_pass":              nil,
							"all_metadata":            map[string]interface{}{},
							"all_tags":                []string{},
							"availability_zone":       "nova",
							"availability_zone_hints": nil,
							"block_device":            []interface{}{},
							"config_drive":            nil,
							"flavor_id":               "ffffffff-ffff-ffff-aaaa-aaaaaaaaaaaa",
							"flavor_name":             "tiny1",
							"floating_ip":             nil,
							"force_delete":            false,
							"id":                      "dddddddd-dddd-dddd-dddd-dddddddddddd",
							"image_id":                "aaaaaaaa-aaaa-aaaa-eeee-eeeeeeeeeeee",
							"image_name":              "ubuntu",
							"key_pair":                "myKeyPair",
							"metadata":                nil,
							"name":                    "test_instance",
							"network": []map[string]interface{}{
								{
									"access_network": false,
									"fixed_ip_v4":    "172.0.0.0",
									"fixed_ip_v6":    "",
									"floating_ip":    "",
									"mac":            "aa:aa:aa:aa:cc:cc",
									"name":           "user-api-net",
									"port":           "",
									"uuid":           "eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee",
								},
							},
							"network_mode":        nil,
							"personality":         []interface{}{},
							"power_state":         "active",
							"region":              "Region",
							"scheduler_hints":     []interface{}{},
							"security_groups":     []string{"user-api-sg"},
							"stop_before_destroy": false,
							"tags":                nil,
							"timeouts":            nil,
							"user_data":           nil,
							"vendor_options":      []interface{}{},
							"volume":              []interface{}{},
						},
						SensitiveAttributes: nil,
						AvailableActions:    []service.DeploymentResourceAction{},
					},
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			parser := TerraformStateParser{}
			got, err := parser.ToStateView(tt.args.state)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToStateView() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.DeepEqual(t, tt.want, got)
		})
	}
}
