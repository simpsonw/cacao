package deploymentcommon

const openstackComputeInstanceV2 = "openstack_compute_instance_v2"

// OpenstackComputeInstance openstack_compute_instance_v2
type OpenstackComputeInstance struct {
	AccessIPV4  string `json:"access_ip_v4"`
	AccessIPV6  string `json:"access_ip_v6"`
	AdminPass   string `json:"admin_pass"`
	BlockDevice []struct {
		SourceType          string `json:"source_type"`
		UUID                string `json:"uuid"`
		VolumeSize          int    `json:"volume_size"`
		DestinationType     string `json:"destination_type"`
		BootIndex           int    `json:"boot_index"`
		DeleteOnTermination *bool  `json:"delete_on_termination"`
		GuestFormat         string `json:"guest_format"`
		VolumeType          string `json:"volume_type"`
		DeviceType          string `json:"device_type"`
		DiskBus             string `json:"disk_bus"`
	} `json:"block_device"`
	ID        string `json:"id"`
	ImageID   string `json:"image_id"`
	ImageName string `json:"image_name"`
	KeyPair   string `json:"key_pair"`
	Name      string `json:"name"`
	Network   []struct {
		IPV4 string `json:"fixed_ip_v4"`
		IPV6 string `json:"fixed_ip_v6"`
		Mac  string `json:"mac"`
		Name string `json:"name"`
		UUID string `json:"uuid"`
	} `json:"network"`
	PowerState    string   `json:"power_state"`
	Region        string   `json:"region"`
	SecurityGroup []string `json:"security_groups"`
	UserData      string   `json:"user_data"`
	Volume        []struct {
		ID       string `json:"id"`
		VolumeID string `json:"volume_id"`
		Device   string `json:"device"`
	} `json:"volume"`
	SchedulerHints []SchedulerHint `json:"scheduler_hints"`
}

const openstackComputeFloatingIPAssociate = "openstack_compute_floatingip_associate_v2"

// OpenstackComputeFloatingIPAssociate openstack_compute_floatingip_associate_v2
type OpenstackComputeFloatingIPAssociate struct {
	FixedIP             string      `json:"fixed_ip"`
	FloatingIP          string      `json:"floating_ip"`
	ID                  string      `json:"id"`          // "111.111.111.111/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/"
	InstanceID          string      `json:"instance_id"` // uuid
	Region              string      `json:"region"`
	Timeouts            interface{} `json:"timeouts"` // FIXME null
	WaitUntilAssociated *bool       `json:"wait_until_associated"`
}

const openstackNetworkingFloatingipV2 = "openstack_networking_floatingip_v2"

// OpenstackNetworkingFloatingIP openstack_networking_floatingip_v2
type OpenstackNetworkingFloatingIP struct {
	Address     string      `json:"address"`
	AllTags     []string    `json:"all_tags"`
	Description string      `json:"description"`
	DNSDomain   string      `json:"dns_domain"`
	DNSName     string      `json:"dns_name"`
	FixedIP     string      `json:"fixed_ip"`
	ID          string      `json:"id"`   // uuid
	Pool        string      `json:"pool"` // "ext-net"
	PortID      string      `json:"port_id"`
	Region      string      `json:"region"`    // "RegionOne"
	SubnetID    string      `json:"subnet_id"` // uuid
	Tags        []string    `json:"tags"`
	TenantID    string      `json:"tenant_id"` // uuid
	Timeouts    interface{} `json:"timeouts"`  // FIXME null
	ValueSpecs  []string    `json:"value_specs"`
}

const openstackBlockstorageVolumeV3 = "openstack_blockstorage_volume_v3"

// OpenstackBlockStorageVolume openstack_blockstorage_volume_v3
type OpenstackBlockStorageVolume struct {
	ID                 string                 `json:"id"` // FIXME verify that volume has this field
	Region             string                 `json:"region"`
	Size               int                    `json:"size"`
	EnableOnlineResize *bool                  `json:"enable_online_resize"`
	Name               string                 `json:"name"`
	Description        string                 `json:"Description"`
	AvailabilityZone   string                 `json:"availability_zone"`
	Metadata           map[string]interface{} `json:"metadata"`
	SnapshotID         string                 `json:"snapshot_id"`
	SourceVolID        string                 `json:"source_vol_id"`
	ImageID            string                 `json:"image_id"`
	VolumeType         string                 `json:"volume_type"`
	ConsistencyGroupID string                 `json:"consistency_group_id"`
	SourceReplica      string                 `json:"source_replica"`
	Multiattach        *bool                  `json:"multiattach"`
	Attachment         []struct {
		ID         string `json:"id"`
		InstanceID string `json:"instance_id"`
		Device     string `json:"device"`
	} `json:"attachment"`
	SchedulerHints []SchedulerHint `json:"scheduler_hints"`
}

// SchedulerHint is the scheduler_hints field shared by many openstack resource
type SchedulerHint struct {
	DifferentHost        []string               `json:"different_host"`
	SameHost             []string               `json:"same_host"`
	Query                string                 `json:"query"`
	LocalToInstance      string                 `json:"local_to_instance"`
	AdditionalProperties map[string]interface{} `json:"additional_properties"`
}
