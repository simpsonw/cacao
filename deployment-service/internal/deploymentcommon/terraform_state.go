package deploymentcommon

import (
	"encoding/json"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
)

const (
	// TerraformRawState ...
	TerraformRawState RawStateType = "terraform"
)

// TerraformState is top level struct for Terraform raw state
type TerraformState struct {
	Version          int                    `json:"version"`
	TerraformVersion string                 `json:"terraform_version"`
	Serial           int                    `json:"serial"`
	Lineage          string                 `json:"lineage"` // uuid
	Outputs          map[string]interface{} `json:"outputs"`
	Resources        []TerraformResource    `json:"resources"`
}

// Type ...
func (state TerraformState) Type() RawStateType {
	return TerraformRawState
}

// String marshals the state into string, this method will log.Fatal() if marshal failed
func (state TerraformState) String() string {
	bytes, err := json.Marshal(state)
	if err != nil {
		log.Fatal(err)
	}
	return string(bytes)
}

// TerraformResource is the resource in Terraform raw state
type TerraformResource struct {
	Module     string                      `json:"module"`
	Mode       string                      `json:"mode"`
	Type       string                      `json:"type"`
	Name       string                      `json:"name"`
	TFProvider string                      `json:"provider"` // "provider[\"terraform.cyverse.org/cyverse/openstack\"]"
	Instances  []TerraformResourceInstance `json:"instances"`
}

// TerraformResourceInstance is instance of resource in Terraform raw state
type TerraformResourceInstance struct {
	IndexKey            int                    `json:"index_key"`
	SchemaVersion       int                    `json:"schema_version"`
	Attributes          map[string]interface{} `json:"attributes"`
	SensitiveAttributes []interface{}          `json:"sensitive_attributes"`
	Private             string                 `json:"private"` // base64 encoded
	Dependencies        []string               `json:"dependencies"`
}

// TerraformStateParser is parser for Terraform raw state
type TerraformStateParser struct {
	// temporarily stores the resources that are auxiliary during parsing, e.g. ip association
	auxiliaryResources []TerraformResource
}

// FromJSONString parse Terraform state from JSON string
func (parser TerraformStateParser) FromJSONString(jsonStateStr string) (*TerraformState, error) {
	var tfState TerraformState
	err := json.Unmarshal([]byte(jsonStateStr), &tfState)
	if err != nil {
		return nil, err
	}
	return &tfState, nil
}

// ToStateView converts TerraformState to a state view
func (parser TerraformStateParser) ToStateView(state TerraformState) (service.DeploymentStateView, error) {
	parser.auxiliaryResources = make([]TerraformResource, 0)

	var view service.DeploymentStateView
	allResources := make([]service.DeploymentResource, 0)
	for _, tfResType := range state.Resources {
		if parser.filterAuxiliaryResources(tfResType) {
			continue
		}
		resources, err := parser.parseResource(tfResType)
		if err != nil {
			return view, err
		}
		allResources = append(allResources, resources...)
	}
	allResources, err := parser.attachAuxiliaryResources(allResources)
	if err != nil {
		return view, err
	}
	view.Resources = allResources
	return view, nil
}

// filter out auxiliary resources, and return true if resource is auxiliary
func (parser TerraformStateParser) filterAuxiliaryResources(resource TerraformResource) bool {
	switch resource.Type {
	case openstackComputeFloatingIPAssociate:
		// since IP association is not a real resource. stores the it for later
		parser.auxiliaryResources = append(parser.auxiliaryResources, resource)
		return true
	default:
		return false
	}
}

func (parser TerraformStateParser) parseResource(tfRes TerraformResource) ([]service.DeploymentResource, error) {
	var resources = make([]service.DeploymentResource, 0)
	for _, tfResInst := range tfRes.Instances {
		res, err := parser.parseResourceInstance(tfRes, tfResInst)
		if err != nil {
			return nil, err
		}
		if res != nil {
			resources = append(resources, *res)
		}
	}
	return resources, nil
}

// might return resource=nil, since some Terraform Resource does not map to a deployment resource (e.g. IP association)
func (parser TerraformStateParser) parseResourceInstance(
	tfRes TerraformResource, tfResInst TerraformResourceInstance) (*service.DeploymentResource, error) {
	var resource service.DeploymentResource
	switch tfRes.Type {
	case openstackComputeInstanceV2:
		var vmInstance OpenstackComputeInstance
		err := mapstructure.Decode(tfResInst.Attributes, &vmInstance)
		if err != nil {
			return nil, err
		}
		resource = service.DeploymentResource{
			ID:                  vmInstance.ID,
			Type:                service.OpenStackInstance,
			ProviderType:        "openstack",
			Provider:            "", // FIXME need to figure out how to get this info
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	case openstackNetworkingFloatingipV2:
		var floatingIP OpenstackNetworkingFloatingIP
		err := mapstructure.Decode(tfResInst.Attributes, &floatingIP)
		if err != nil {
			return nil, err
		}
		resource = service.DeploymentResource{
			ID:                  floatingIP.ID,
			Type:                service.DeploymentResourceType(tfRes.Type),
			ProviderType:        "openstack",
			Provider:            "", // FIXME need to figure out how to get this info
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	case openstackComputeFloatingIPAssociate:
		// not a real resource, this should be filtered out
	case openstackBlockstorageVolumeV3:
		var vol OpenstackBlockStorageVolume
		err := mapstructure.Decode(tfResInst.Attributes, &vol)
		if err != nil {
			return nil, err
		}
		resource = service.DeploymentResource{
			ID:                  vol.ID,
			Type:                service.OpenStackVolume,
			ProviderType:        "openstack",
			Provider:            "", // FIXME need to figure out how to get this info
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	default:
		var resID string
		var idFound bool
		for _, field := range []string{"id", "Id", "ID"} {
			value, ok := tfResInst.Attributes[field]
			if ok {
				str, ok := value.(string)
				if ok {
					resID = str
					idFound = true
					break
				}
			}
		}
		if !idFound {
			log.WithFields(log.Fields{"resType": tfRes.Type,
				"resInstAttr": tfResInst.Attributes}).Warnf("unable to get ID from resource")
			return nil, nil
		}

		resource = service.DeploymentResource{
			ID:                  resID,
			Type:                service.DeploymentResourceType(tfRes.Type),
			ProviderType:        "", // FIXME need to figure out how to get this info
			Provider:            "", // FIXME need to figure out how to get this info
			Attributes:          tfResInst.Attributes,
			SensitiveAttributes: nil,
			AvailableActions:    make([]service.DeploymentResourceAction, 0),
		}
	}
	return &resource, nil
}

// attachAuxiliaryResources attaches attributes of auxiliary resources to its related resource  (e.g. attach IP association to VM)
func (parser TerraformStateParser) attachAuxiliaryResources(resources []service.DeploymentResource) ([]service.DeploymentResource, error) {
	for _, auxRes := range parser.auxiliaryResources {
		switch auxRes.Type {
		case openstackComputeFloatingIPAssociate:
			for _, tfResInst := range auxRes.Instances {
				var ipAssociate OpenstackComputeFloatingIPAssociate
				err := mapstructure.Decode(tfResInst.Attributes, &ipAssociate)
				if err != nil {
					return nil, err
				}
				//ipAssociate.InstanceID
				for _, res := range resources {
					if res.Type == openstackComputeInstanceV2 && res.ID == ipAssociate.InstanceID {
						// "floating_ip" is from field tag in cacao-common.service.OpenStackInstanceAttributes
						res.Attributes["floating_ip"] = ipAssociate.FloatingIP
					}
				}
			}
		default:
			panic("unknown aux resource: " + auxRes.Type)
		}
	}
	return resources, nil
}
