package deploymentcommon

/*
func (rs RawDeploymentState) MarshalBSONValue() (bsontype.Type, []byte, error) {
	switch rs.Type {
	case TerraformRawState:
		var tfState TerraformState
		switch data := rs.State.(type) {
		default:
			log.WithField("type", reflect.TypeOf(rs.State)).Error("BadTypeForTerraformState")
			return bsontype.EmbeddedDocument, nil, fmt.Errorf("BadTypeForTerraformState")
		case TerraformState:
			tfState = data
		case string:
			err := json.Unmarshal([]byte(data), &tfState)
			if err != nil {
				return bsontype.EmbeddedDocument, nil, err
			}
		case map[string]interface{}:
			err := mapstructure.Decode(data, &tfState)
			if err != nil {
				return bsontype.EmbeddedDocument, nil, err
			}
		case map[interface{}]interface{}:
			err := mapstructure.Decode(data, &tfState)
			if err != nil {
				return bsontype.EmbeddedDocument, nil, err
			}
		}
		return bson.MarshalValue(bson.M{"type": TerraformRawState, "state": tfState})
	default:
		return bsontype.Undefined, nil, fmt.Errorf("UnknownRawState")
	}
}

func (rs *RawDeploymentState) UnmarshalBSONValue(bsonType bsontype.Type, data []byte) error {

	switch bsonType {
	case bsontype.EmbeddedDocument:
		var staging struct {
			Type RawStateType `bson:"type"`
		}
		err := bson.Unmarshal(data, &staging)
		if err != nil {
			return err
		}
		switch staging.Type {
		case TerraformRawState:
			var tfState TerraformState
			err = bson.Unmarshal(data, &tfState)
			if err != nil {
				return err
			}
			rs.State = tfState
			return nil
		default:
			log.Error(data)
			log.Error(string(data))
			log.WithField("type", staging.Type).Error()
			return fmt.Errorf("UnknownRawState")
		}
	case bsontype.String:
		var staging struct {
			Type RawStateType `bson:"type"`
		}
		err := json.Unmarshal(data, &staging)
		if err != nil {
			return err
		}
		switch staging.Type {
		case TerraformRawState:
			var tfState TerraformState
			err := json.Unmarshal(data, &tfState)
			if err != nil {
				return err
			}
			rs.State = tfState
			return nil
		default:
			log.WithField("type", staging.Type).Error()
			return fmt.Errorf("UnknownRawState")
		}
	case bsontype.Null:
		rs.Type = ""
		return nil
	default:
		log.WithField("type", bsonType).Error("BadTypeForTerraformState")
		return fmt.Errorf("BadTypeForTerraformState")
	}
}
*/
