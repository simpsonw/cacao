package storage

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const deploymentMongoCollection string = "deployment"

// MongoDeploymentStorage is mongo storage for deployment object
type MongoDeploymentStorage struct {
	conf     db.MongoDBConfig
	objStore *db.MongoDBObjectStore
}

// NewMongoDeploymentStorage creates a new MongoDeploymentStorage
func NewMongoDeploymentStorage(conf db.MongoDBConfig) *MongoDeploymentStorage {
	return &MongoDeploymentStorage{
		conf: conf,
	}
}

// Init establish connection to mongo
func (m *MongoDeploymentStorage) Init() error {
	objStore, err := db.CreateMongoDBObjectStore(&m.conf)
	if err != nil {
		return err
	}
	m.objStore = objStore
	log.Info("connected to Mongodb")
	return nil
}

// Create creates a Deployment object in mongo
func (m MongoDeploymentStorage) Create(deployment deploymentcommon.Deployment) error {
	err := m.objStore.Insert(deploymentMongoCollection, deployment)
	return err
}

// Get fetches a Deployment by its ID
func (m MongoDeploymentStorage) Get(id common.ID) (deploymentcommon.Deployment, error) {
	var deployment deploymentcommon.Deployment
	err := m.objStore.Get(deploymentMongoCollection, string(id), &deployment)
	if err != nil {
		log.Error(err)
		if err == mongo.ErrNoDocuments {
			return deployment, ErrDeploymentNotFound
		}
		return deployment, err
	}
	return deployment, err
}

// List returns a list of deployments with pagination (offet&limit), sort and filters
func (m MongoDeploymentStorage) List(filters DeploymentFilter, offset int64, limit int64, sort DeploymentSort) ([]deploymentcommon.Deployment, error) {
	var deployments []deploymentcommon.Deployment
	mongoFilters := toMongoFilters(filters)

	findOpts := options.Find().SetSkip(offset)
	if limit > 0 {
		findOpts.SetLimit(limit)
	}
	findOpts.SetSort(m.sortOption(sort))

	// TODO use range queries for better performance if sort by ID (or other unique identifier)
	cursor, err := m.objStore.Connection.Database.Collection(deploymentMongoCollection).Find(
		context.TODO(), mongoFilters, findOpts)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, ErrEmptyDeploymentList
		}
		return nil, err
	}

	err = cursor.All(context.TODO(), &deployments)
	if err != nil {
		return nil, err
	}
	return deployments, err
}

// Update updates a deployment with specified ID
// FIXME unimplemented
func (m MongoDeploymentStorage) Update(id common.ID, deployment deploymentcommon.Deployment) error {
	// FIXME
	panic("unimplemented")
}

// Delete deletes a deployment by its ID
// FIXME unimplemented
func (m MongoDeploymentStorage) Delete(id common.ID) error {
	// FIXME
	panic("unimplemented")
}

// toMongoFilters converts a filter object to filter in mongo-format
func toMongoFilters(filters DeploymentFilter) bson.M {
	mongoFilters := bson.M{}
	if filters.IsCreatorSet() {
		mongoFilters["creator"] = filters.Creator
	}
	if filters.IsTemplateSet() {
		mongoFilters["template"] = filters.Template
	}
	if filters.IsWorkspaceSet() {
		mongoFilters["workspace"] = filters.Workspace
	}
	if filters.IsPrimaryCloudProviderSet() {
		mongoFilters["provider"] = filters.PrimaryCloudProvider
	}
	if filters.IsStatusSet() {
		mongoFilters["status"] = filters.Status
	}
	return mongoFilters
}

func (m MongoDeploymentStorage) sortOption(sort DeploymentSort) bson.M {
	opt := bson.M{}
	opt[sort.SortBy.String()] = sort.SortDir
	return opt
}
