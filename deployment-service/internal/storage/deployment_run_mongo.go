package storage

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DeploymentRunCollection is the mongo collection for storing deployment runs
const DeploymentRunCollection string = "deploymentRun"

// MongoDeploymentRunStorage is a mongo storage for deployment run
type MongoDeploymentRunStorage struct {
	config   db.MongoDBConfig
	objStore db.MongoDBObjectStore
}

// NewMongoDeploymentRunStorage creates a new MongoDeploymentRunStorage
func NewMongoDeploymentRunStorage(config db.MongoDBConfig) *MongoDeploymentRunStorage {
	return &MongoDeploymentRunStorage{config: config}
}

// Init initialize the mongo storage
func (m *MongoDeploymentRunStorage) Init() error {
	objStore, err := db.CreateMongoDBObjectStore(&m.config)
	if err != nil {
		return err
	}
	m.objStore = *objStore
	return nil
}

// Create creates a deployment run
func (m MongoDeploymentRunStorage) Create(run deploymentcommon.DeploymentRun) (runID common.ID, err error) {
	panic("implement me")
}

// Get returns the deployment run with the specific ID
func (m MongoDeploymentRunStorage) Get(runID common.ID) (deploymentcommon.DeploymentRun, error) {
	var run deploymentcommon.DeploymentRun
	err := m.objStore.Get(m.collection(), runID.String(), &run)
	if err == mongo.ErrNoDocuments {
		return deploymentcommon.DeploymentRun{}, ErrRunNotFound
	} else if err != nil {
		return deploymentcommon.DeploymentRun{}, err
	}
	return run, nil
}

// GetLatestRun returns the latest deployment run of a deployment if any
func (m MongoDeploymentRunStorage) GetLatestRun(deployment common.ID) (*deploymentcommon.DeploymentRun, error) {
	filter := bson.M{
		"deployment": deployment.String(),
	}
	sort := bson.M{
		"created_at": -1,
	}
	var run deploymentcommon.DeploymentRun
	err := m.objStore.Connection.Get(m.collection(), filter, &run, options.FindOne().SetSort(sort))
	if err != nil {
		return nil, err
	}
	return &run, nil
}

// List return a list of deployment run with filter
func (m MongoDeploymentRunStorage) List(filter DeploymentRunFilter, pagination service.RequestPagination) ([]deploymentcommon.DeploymentRun, error) {
	var runs []deploymentcommon.DeploymentRun

	err := m.objStore.Connection.List(m.collection(), m.filterToMongo(filter), &runs, m.paginationToMongoOption(pagination))
	if err == mongo.ErrNoDocuments {
		return nil, ErrEmptyRunList
	} else if err != nil {
		return nil, err
	}
	return runs, nil
}

// Delete deletes a deployment run by ID
func (m MongoDeploymentRunStorage) Delete(runID common.ID) error {
	panic("implement me")
}

// DeleteAllRunByDeployment deletes all deployment run within a deployment
func (m MongoDeploymentRunStorage) DeleteAllRunByDeployment(deployment common.ID) error {
	panic("implement me")
}

func (m MongoDeploymentRunStorage) collection() string {
	return DeploymentRunCollection
}

func (m MongoDeploymentRunStorage) filterToMongo(filter DeploymentRunFilter) bson.M {
	mongoFilter := bson.M{}
	if filter.IsDeploymentSet() {
		mongoFilter["deployment"] = filter.Deployment.String()
	}
	return mongoFilter
}

func (m MongoDeploymentRunStorage) paginationToMongoOption(pagination service.RequestPagination) *options.FindOptions {
	opt := options.Find()
	if pagination.PageSizeLimit > 0 {
		opt.SetLimit(int64(pagination.PageSizeLimit))
	}
	if pagination.Offset > 0 {
		opt.SetSkip(int64(pagination.Offset))
	}
	return opt
}
