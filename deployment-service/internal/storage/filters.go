package storage

import (
	"gitlab.com/cyverse/cacao-common/common"
	"time"
)

// DeploymentFilter is filter for searching deployment
type DeploymentFilter struct {
	// Only return deployments that is created by a user
	Creator string
	// Only return deployments that has the specified source template
	Template common.ID
	// Only return deployments that is in the specified workspace
	Workspace common.ID
	// Only return deployments that has the specified primary cloud provider
	PrimaryCloudProvider common.ID
	// Only return deployments that has the specified status
	Status string
}

// IsCreatorSet ...
func (df DeploymentFilter) IsCreatorSet() bool {
	return len(df.Creator) > 0
}

// IsTemplateSet ...
func (df DeploymentFilter) IsTemplateSet() bool {
	return len(df.Template.String()) > 0
}

// IsWorkspaceSet ...
func (df DeploymentFilter) IsWorkspaceSet() bool {
	return len(df.Workspace.String()) > 0
}

// IsPrimaryCloudProviderSet ...
func (df DeploymentFilter) IsPrimaryCloudProviderSet() bool {
	return len(df.PrimaryCloudProvider.String()) > 0
}

// IsStatusSet ...
func (df DeploymentFilter) IsStatusSet() bool {
	return len(df.Status) > 0
}

// DeploymentRunFilter is filter for searching deployment run
type DeploymentRunFilter struct {
	Deployment common.ID
}

// IsDeploymentSet ...
func (drf DeploymentRunFilter) IsDeploymentSet() bool {
	return len(drf.Deployment.String()) > 0
}

// DeploymentHistoryFilter is filter for searching deployment history
type DeploymentHistoryFilter struct {
	// Only return histories for a certain deployment
	Deployment common.ID
	// Only return histories that has a timestamp before the specified
	Before *time.Time
	// Only return histories that has a timestamp after the specified
	After *time.Time
}

// IsDeploymentSet ...
func (dhf DeploymentHistoryFilter) IsDeploymentSet() bool {
	return len(dhf.Deployment.String()) > 0
}

// IsBeforeSet ...
func (dhf DeploymentHistoryFilter) IsBeforeSet() bool {
	return dhf.Before != nil
}

// IsAfterSet ...
func (dhf DeploymentHistoryFilter) IsAfterSet() bool {
	return dhf.After != nil
}
