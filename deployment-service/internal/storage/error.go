package storage

import (
	"errors"
)

var (
	// ErrDeploymentNotFound is error occurs when the deployment specified does not exist
	ErrDeploymentNotFound = errors.New("DeploymentNotFound")
	// ErrRunNotFound is error occurs when the deployment run specified does not exist
	ErrRunNotFound = errors.New("RunNotFound")
	// ErrEmptyDeploymentList is error occurs when a search returns no deployment
	ErrEmptyDeploymentList = errors.New("EmptyDeploymentList")
	// ErrEmptyRunList is error occurs when a search returns no deployment run
	ErrEmptyRunList = errors.New("EmptyRunList")
	// ErrUnknownSortByField is error occurs when the sort by field specified is not recognized
	ErrUnknownSortByField = errors.New("UnknownSortByField")
	// ErrUnknownSortDirection is error occurs when the sort direction specified is not recognized
	ErrUnknownSortDirection = errors.New("UnknownSortDirection")
)
