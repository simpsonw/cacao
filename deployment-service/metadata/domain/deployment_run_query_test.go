package domain

import (
	"encoding/json"
	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	domainmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/domain/mocks"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	typesmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/types/mocks"
	"math/rand"
	"testing"
	"time"
)

func TestGetRunHandler_Handle(t *testing.T) {
	t.Run("existing run", testGetRunHandlerExistingRun)
	t.Run("not found", testGetRunHandlerRunNotFound)
	t.Run("not authorized", testGetRunHandlerNotAuthorized)
}

func testGetRunHandlerExistingRun(t *testing.T) {
	getRunQuery := service.DeploymentGetRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := deploymentcommon.DeploymentRun{
		ID:               getRunQuery.Run,
		Deployment:       getRunQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState:         nil,
		Logs:             nil,
		Histories:        nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRunQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", getRunQuery.GetSessionActor(), run.Deployment).Return(true, nil)

	h := GetRunHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRunReply{}, reply)
	getRunReply := reply.(service.DeploymentGetRunReply)
	assert.Equal(t, "", getRunReply.ErrorType)
	assert.Equal(t, "", getRunReply.ErrorMessage)
	assert.Equal(t, run.ID, getRunReply.Run.ID)
	assert.Equal(t, run.Deployment, getRunReply.Deployment)
	assert.Equal(t, run.Deployment, getRunReply.Run.Deployment)
	assert.Equal(t, run.ConvertToExternal(), getRunReply.Run)
}

func testGetRunHandlerRunNotFound(t *testing.T) {
	getRunQuery := service.DeploymentGetRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRunQuery.Run).Return(deploymentcommon.DeploymentRun{}, storage.ErrRunNotFound)

	var perm = &domainmocks.PermissionChecker{}

	h := GetRunHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRunReply{}, reply)
	getRunReply := reply.(service.DeploymentGetRunReply)
	assert.Equal(t, types.DeploymentRunNotFound, getRunReply.ErrorType)
	assert.Equal(t, "", getRunReply.ErrorMessage)
}

func testGetRunHandlerNotAuthorized(t *testing.T) {
	getRunQuery := service.DeploymentGetRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := deploymentcommon.DeploymentRun{
		ID:               getRunQuery.Run,
		Deployment:       getRunQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState:         nil,
		Logs:             nil,
		Histories:        nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRunQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On(
		"DeploymentAccessByID",
		getRunQuery.GetSessionActor(),
		run.Deployment,
	).Return(false, nil) // not authorized

	h := GetRunHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRunReply{}, reply)
	getRunReply := reply.(service.DeploymentGetRunReply)
	assert.Equal(t, service.GeneralActorNotAuthorizedError, getRunReply.ErrorType)
	assert.Equal(t, "", getRunReply.ErrorMessage)
}

func TestListRunHandler_HandleStream(t *testing.T) {
	t.Run("existing runs", testListRunHandlerExistingRun)
	// FIXME
	t.Run("empty list", testListRunHandlerEmptyList)
	t.Run("streaming in multiple replies", testListRunHandlerStreamingList)
	t.Run("pagination", testListRunHandlerPagination)
}

func testListRunHandlerExistingRun(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	runs := make([]deploymentcommon.DeploymentRun, 0)
	for i := 0; i < 10; i++ {
		runs = append(runs, deploymentcommon.DeploymentRun{
			ID:         common.NewID(types.RunIDPrefix),
			Deployment: listRunQuery.Deployment,
		})
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		storage.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	).Return(runs, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListRunHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListRunReply{}, reply)
	listRunReply := reply.(service.DeploymentListRunReply)
	assert.Equal(t, "", listRunReply.ErrorType)
	assert.Equal(t, "", listRunReply.ErrorMessage)
	assert.Len(t, listRunReply.Runs, len(runs))
	for i := range listRunReply.Runs {
		assert.Equal(t, runs[i].ID, listRunReply.Runs[i].ID)
		assert.Equal(t, runs[i].Deployment, listRunReply.Runs[i].Deployment)
		assert.Equal(t, runs[i].ConvertToExternal(), listRunReply.Runs[i])
	}
}

func testListRunHandlerEmptyList(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		storage.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	).Return(nil, storage.ErrEmptyRunList) // empty list

	var perm = &domainmocks.PermissionChecker{}

	h := ListRunHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListRunReply{}, reply)
	listRunReply := reply.(service.DeploymentListRunReply)
	assert.Equal(t, types.DeploymentRunNotFound, listRunReply.ErrorType)
	assert.Equal(t, "", listRunReply.ErrorMessage)
	assert.Len(t, listRunReply.Runs, 0)
}

func testListRunHandlerStreamingList(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	runs := make([]deploymentcommon.DeploymentRun, 0)
	runCount := types.DeploymentRunStreamThreshold*5 + rand.Intn(100) + 3
	for i := 0; i < runCount; i++ {
		runs = append(runs, deploymentcommon.DeploymentRun{
			ID:         common.NewID(types.RunIDPrefix),
			Deployment: listRunQuery.Deployment,
		})
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		storage.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        0,
			PageSizeLimit: -1,
		},
	).Return(runs, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListRunHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	expectedRunCount := len(runs) / types.DeploymentRunStreamThreshold
	if len(runs)%types.DeploymentRunStreamThreshold > 0 {
		expectedRunCount++
	}
	if !assert.Len(t, replies, expectedRunCount) {
		return
	}
	for replyIndex, reply := range replies {
		assert.IsType(t, service.DeploymentListRunReply{}, reply)
		listRunReply := reply.(service.DeploymentListRunReply)
		assert.Equal(t, "", listRunReply.ErrorType)
		assert.Equal(t, "", listRunReply.ErrorMessage)
		if replyIndex >= len(replies)-1 {
			assert.Len(t, listRunReply.Runs, len(runs)%types.DeploymentRunStreamThreshold)
		} else {
			assert.Len(t, listRunReply.Runs, types.DeploymentRunStreamThreshold)
		}
	}
}

func testListRunHandlerPagination(t *testing.T) {
	listRunQuery := service.DeploymentListRunQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		RequestPagination: service.RequestPagination{
			Offset:        5,
			PageSizeLimit: 5,
		},
	}
	ce, err := messaging.CreateCloudEvent(listRunQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	runs := make([]deploymentcommon.DeploymentRun, 0)
	for i := 0; i < 20; i++ {
		runs = append(runs, deploymentcommon.DeploymentRun{
			ID:         common.NewID(types.RunIDPrefix),
			Deployment: listRunQuery.Deployment,
		})
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On(
		"List",
		storage.DeploymentRunFilter{
			Deployment: listRunQuery.Deployment,
		},
		service.RequestPagination{
			Offset:        listRunQuery.Offset,
			PageSizeLimit: listRunQuery.PageSizeLimit,
		},
	).Return(runs[listRunQuery.Offset:listRunQuery.PageSizeLimit+listRunQuery.Offset], nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListRunHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListRunReply{}, reply)
	listRunReply := reply.(service.DeploymentListRunReply)
	assert.Equal(t, "", listRunReply.ErrorType)
	assert.Equal(t, "", listRunReply.ErrorMessage)
	assert.Equal(t, listRunReply.Offset, listRunReply.Offset)
	assert.Equal(t, len(listRunReply.Runs), listRunReply.Count)
	assert.Equal(t, listRunQuery.PageSizeLimit, listRunReply.Count)
	for i := range listRunReply.Runs {
		assert.Equal(t, runs[i+listRunQuery.Offset].ID, listRunReply.Runs[i].ID)
		assert.Equal(t, runs[i+listRunQuery.Offset].Deployment, listRunReply.Runs[i].Deployment)
		assert.Equal(t, runs[i+listRunQuery.Offset].ConvertToExternal(), listRunReply.Runs[i])
	}
}

func TestGetRawStateQueryHandler_Handle(t *testing.T) {
	t.Run("existing run", testGetRawStateQueryHandlerExistingRun)
	t.Run("not raw state", testGetRawStateQueryHandlerNoRawState)
	t.Run("not authorized", testGetRawStateQueryHandlerNotAuthorized)
}

func testGetRawStateQueryHandlerExistingRun(t *testing.T) {
	getRawStateQuery := service.DeploymentGetRawStateQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRawStateQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := deploymentcommon.DeploymentRun{
		ID:               getRawStateQuery.Run,
		Deployment:       getRawStateQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState: &deploymentcommon.RawDeploymentState{
			Type: deploymentcommon.TerraformRawState,
			TFState: &deploymentcommon.TerraformState{
				Version:          0,
				TerraformVersion: "",
				Serial:           0,
				Lineage:          "",
				Outputs:          nil,
				Resources: []deploymentcommon.TerraformResource{
					{
						Module:     "",
						Mode:       "",
						Type:       "",
						Name:       randomString(),
						TFProvider: "",
						Instances:  nil,
					},
				},
			},
		},
		Logs:      nil,
		Histories: nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRawStateQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", getRawStateQuery.GetSessionActor(), run.Deployment).Return(true, nil)

	h := GetRawStateQueryHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRawStateReply{}, reply)
	getRawStateReply := reply.(service.DeploymentGetRawStateReply)
	assert.Equal(t, "", getRawStateReply.ErrorType)
	assert.Equal(t, "", getRawStateReply.ErrorMessage)
	assert.True(t, len(getRawStateReply.RawState) > 0)

	var rawState deploymentcommon.TerraformState
	err = json.Unmarshal(getRawStateReply.RawState, &rawState)
	assert.NoError(t, err)
	assert.Equal(t, *run.RawState.TFState, rawState)
}

func testGetRawStateQueryHandlerNoRawState(t *testing.T) {
	getRawStateQuery := service.DeploymentGetRawStateQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRawStateQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := deploymentcommon.DeploymentRun{
		ID:               getRawStateQuery.Run,
		Deployment:       getRawStateQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState:         nil, // no raw state
		Logs:             nil,
		Histories:        nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRawStateQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", getRawStateQuery.GetSessionActor(), run.Deployment).Return(true, nil)

	h := GetRawStateQueryHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRawStateReply{}, reply)
	getRawStateReply := reply.(service.DeploymentGetRawStateReply)
	assert.Equal(t, service.DeploymentNoRawStateAvailable, getRawStateReply.ErrorType)
	assert.Equal(t, "", getRawStateReply.ErrorMessage)
	assert.Nil(t, getRawStateReply.RawState)
}

func testGetRawStateQueryHandlerNotAuthorized(t *testing.T) {
	getRawStateQuery := service.DeploymentGetRawStateQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getRawStateQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := deploymentcommon.DeploymentRun{
		ID:               getRawStateQuery.Run,
		Deployment:       getRawStateQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState: &deploymentcommon.RawDeploymentState{
			Type: deploymentcommon.TerraformRawState,
			TFState: &deploymentcommon.TerraformState{
				Version:          0,
				TerraformVersion: "",
				Serial:           0,
				Lineage:          "",
				Outputs:          nil,
				Resources: []deploymentcommon.TerraformResource{
					{
						Module:     "",
						Mode:       "",
						Type:       "",
						Name:       randomString(),
						TFProvider: "",
						Instances:  nil,
					},
				},
			},
		},
		Logs:      nil,
		Histories: nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getRawStateQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On(
		"DeploymentAccessByID",
		getRawStateQuery.GetSessionActor(),
		run.Deployment,
	).Return(false, nil) // not authorized

	h := GetRawStateQueryHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetRawStateReply{}, reply)
	getRawStateReply := reply.(service.DeploymentGetRawStateReply)
	assert.Equal(t, service.GeneralActorNotAuthorizedError, getRawStateReply.ErrorType)
	assert.Equal(t, "", getRawStateReply.ErrorMessage)
	assert.Nil(t, getRawStateReply.RawState)
}

func TestGetLogsQueryHandler_Handle(t *testing.T) {
	t.Run("existing run", testGetLogsQueryHandlerExistingRun)
	t.Run("no logs", testGetLogsQueryHandlerNoLogs)
	t.Run("not authorized", testGetLogsQueryHandlerNotAuthorized)
}

func testGetLogsQueryHandlerExistingRun(t *testing.T) {
	getLogsQuery := service.DeploymentGetLogsQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getLogsQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	logs := randomString()
	run := deploymentcommon.DeploymentRun{
		ID:               getLogsQuery.Run,
		Deployment:       getLogsQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState:         nil,
		Logs:             &logs,
		Histories:        nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getLogsQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", getLogsQuery.GetSessionActor(), run.Deployment).Return(true, nil)

	h := GetLogsQueryHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetLogsReply{}, reply)
	getLogsReply := reply.(service.DeploymentGetLogsReply)
	assert.Equal(t, "", getLogsReply.ErrorType)
	assert.Equal(t, "", getLogsReply.ErrorMessage)
	assert.True(t, len(getLogsReply.Logs) > 0)
	assert.Equal(t, *run.Logs, getLogsReply.Logs)
}

func testGetLogsQueryHandlerNoLogs(t *testing.T) {
	getLogsQuery := service.DeploymentGetLogsQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getLogsQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	run := deploymentcommon.DeploymentRun{
		ID:               getLogsQuery.Run,
		Deployment:       getLogsQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState:         nil,
		Logs:             nil, // no logs
		Histories:        nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getLogsQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccessByID", getLogsQuery.GetSessionActor(), run.Deployment).Return(true, nil)

	h := GetLogsQueryHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetLogsReply{}, reply)
	getLogsReply := reply.(service.DeploymentGetLogsReply)
	assert.Equal(t, service.DeploymentNoLogsAvailable, getLogsReply.ErrorType)
	assert.Equal(t, "", getLogsReply.ErrorMessage)
	assert.Empty(t, getLogsReply.Logs)
}

func testGetLogsQueryHandlerNotAuthorized(t *testing.T) {
	getLogsQuery := service.DeploymentGetLogsQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Deployment: common.NewID(types.DeploymentIDPrefix),
		Run:        common.NewID(types.RunIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getLogsQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	logs := randomString()
	run := deploymentcommon.DeploymentRun{
		ID:               getLogsQuery.Run,
		Deployment:       getLogsQuery.Deployment,
		CreatedAt:        time.Time{},
		EndsAt:           time.Time{},
		TemplateSnapshot: deploymentcommon.TemplateSnapshot{},
		Parameters:       nil,
		Status:           "",
		LastState:        deploymentcommon.DeploymentStateView{},
		StateUpdatedAt:   time.Time{},
		RawState:         nil,
		Logs:             &logs,
		Histories:        nil,
	}
	var runStorage = &portsmocks.DeploymentRunStorage{}
	runStorage.On("Get", getLogsQuery.Run).Return(run, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On(
		"DeploymentAccessByID",
		getLogsQuery.GetSessionActor(),
		run.Deployment,
	).Return(false, nil) // not authorized

	h := GetLogsQueryHandler{
		storage: runStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	runStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetLogsReply{}, reply)
	getLogsReply := reply.(service.DeploymentGetLogsReply)
	assert.Equal(t, service.GeneralActorNotAuthorizedError, getLogsReply.ErrorType)
	assert.Equal(t, "", getLogsReply.ErrorMessage)
	assert.Empty(t, getLogsReply.Logs)
}

func randomString() string {
	// use xid to generate a random string
	return xid.New().String()
}
