package domain

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// GetRunHandler is handler for GetRun query
type GetRunHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// Handle handles the query
func (h GetRunHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "getRun",
	})
	var getRunQuery service.DeploymentGetRunQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getRunQuery)
	if err != nil {
		logger.WithField("cloudevent", query.CloudEvent()).WithError(err).Error("fail to unmarshal the query")
		return h.createErrorReply(types.QueryUnmarshalError, err.Error())
	}

	run, err := h.storage.Get(getRunQuery.Run)
	if err == storage.ErrRunNotFound {
		logger.WithError(err).Error()
		return h.createErrorReply(types.DeploymentRunNotFound, "")
	} else if err != nil {
		logger.WithError(err).Error()
		return h.createErrorReply(types.DeploymentRunNotFound, err.Error())
	}

	ok, err := h.perm.DeploymentAccessByID(getRunQuery.GetSessionActor(), run.Deployment)
	if err != nil {
		return h.createErrorReply(service.AuthorizationFailed, err.Error())
	} else if !ok {
		return h.createErrorReply(service.GeneralActorNotAuthorizedError, "")
	}

	return service.DeploymentGetRunReply{
		Session:    service.Session{},
		Deployment: run.Deployment,
		Run:        run.ConvertToExternal(),
	}
}

// HandleStream handles the query
func (h GetRunHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

// QueryOp ...
func (h GetRunHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRunQueryType
}

func (h GetRunHandler) createErrorReply(errType, errMsg string) service.DeploymentGetRunReply {
	return service.DeploymentGetRunReply{
		Session: service.Session{
			ErrorType:    errType,
			ErrorMessage: errMsg,
		},
	}
}

// ListRunHandler is handler for ListRun query
type ListRunHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// Handle handles the query
func (h ListRunHandler) Handle(query types.Query) Reply {
	panic("not implemented")
}

// HandleStream handles the query
func (h ListRunHandler) HandleStream(query types.Query) []Reply {
	logger := log.WithFields(log.Fields{
		"query": "listRun",
	})

	var listRunQuery service.DeploymentListRunQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &listRunQuery)
	if err != nil {
		logger.WithField("cloudevent", query.CloudEvent()).WithError(err).Error("fail to unmarshal the query")
		return []Reply{h.createErrorReply(types.QueryUnmarshalError, err.Error())}
	}
	filter := storage.DeploymentRunFilter{
		Deployment: listRunQuery.Deployment,
	}
	runs, err := h.storage.List(filter, listRunQuery.RequestPagination)
	if err == storage.ErrEmptyRunList {
		return []Reply{h.createErrorReply(types.DeploymentRunNotFound, "")}
	} else if err != nil {
		return []Reply{h.createErrorReply(types.DeploymentRunNotFound, err.Error())}
	}
	return h.createReplies(listRunQuery.Offset, runs)
}

// QueryOp ...
func (h ListRunHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRunQueryType
}

func (h ListRunHandler) createErrorReply(errType, errMsg string) service.DeploymentListRunReply {
	return service.DeploymentListRunReply{
		Session: service.Session{
			ErrorType:    errType,
			ErrorMessage: errMsg,
		},
	}
}

func (h ListRunHandler) createReplies(initialOffset int, runs []deploymentcommon.DeploymentRun) []Reply {
	replies := make([]Reply, 0)
	offsetCounter := initialOffset

	if runs == nil || len(runs) == 0 {
		return []Reply{h.createEmptyReply(initialOffset)}
	} else if len(runs) < listStreamingThreshold {
		return []Reply{h.singleReply(initialOffset, runs)}
	}
	allChunks := h.divideList(h.convertRunsToExternal(runs))
	for chunkIndex, chunk := range allChunks {
		replies = append(replies,
			service.DeploymentListRunReply{
				Session: service.Session{},
				Offset:  offsetCounter,
				Count:   len(chunk),
				Runs:    chunk,
				ReplyListStream: service.ReplyListStream{
					TotalReplies: len(allChunks),
					ReplyIndex:   chunkIndex,
				},
			})
		offsetCounter += len(chunk)
	}
	return replies
}

func (h ListRunHandler) createEmptyReply(initialOffset int) service.DeploymentListRunReply {
	return service.DeploymentListRunReply{
		Session:         service.Session{},
		Offset:          initialOffset,
		Count:           0,
		Runs:            []service.DeploymentRun{},
		ReplyListStream: service.ReplyListStream{},
	}
}

func (h ListRunHandler) singleReply(initialOffset int, runs []deploymentcommon.DeploymentRun) service.DeploymentListRunReply {
	return service.DeploymentListRunReply{
		Session:         service.Session{},
		Offset:          initialOffset,
		Count:           len(runs),
		Runs:            h.convertRunsToExternal(runs),
		ReplyListStream: service.ReplyListStream{},
	}
}

func (h ListRunHandler) convertRunsToExternal(runs []deploymentcommon.DeploymentRun) []service.DeploymentRun {
	extRuns := make([]service.DeploymentRun, 0, len(runs))
	for _, run := range runs {
		extRuns = append(extRuns, run.ConvertToExternal())
	}
	return extRuns
}

func (h ListRunHandler) divideList(runs []service.DeploymentRun) [][]service.DeploymentRun {

	allChunks := make([][]service.DeploymentRun, 0)
	var chunk []service.DeploymentRun
	var start = 0
	var end = listStreamingThreshold
	for start < len(runs) {
		chunk = runs[start:end]
		allChunks = append(allChunks, chunk)
		start = end
		if start+types.DeploymentRunStreamThreshold >= len(runs) {
			end = len(runs)
		} else {
			end = start + types.DeploymentRunStreamThreshold
		}
	}
	return allChunks
}

// GetRawStateQueryHandler handles GetRawState query
type GetRawStateQueryHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// QueryOp ...
func (h GetRawStateQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRawStateQueryType
}

// Handle handles the query
func (h GetRawStateQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "getRawState",
	})
	var getRawState service.DeploymentGetRawStateQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getRawState)
	if err != nil {
		logger.WithField("cloudevent", query.CloudEvent()).Error(err)
		return h.createErrorReply("queryUnmarshalError", err.Error())
	}
	logger = logger.WithFields(log.Fields{
		"deployment": getRawState.Deployment,
		"actor":      getRawState.SessionActor,
		"emulator":   getRawState.SessionEmulator,
	})

	run, err := h.storage.Get(getRawState.Run)
	if err != nil {
		return h.createErrorReply(service.DeploymentNotFound, err.Error())
	}

	if ok, err := h.perm.DeploymentAccessByID(getRawState.GetSessionActor(), run.Deployment); err != nil {
		logger.WithError(err).Error("permission check failed")
		return h.createErrorReply(service.AuthorizationFailed, "Error during authorization check")
	} else if !ok {
		return h.createErrorReply(service.GeneralActorNotAuthorizedError, "")
	}
	if !run.HasRawState() {
		return h.createErrorReply(service.DeploymentNoRawStateAvailable, "")
	}

	return h.createReply(*run.RawState)
}

// HandleStream handles the query
func (h GetRawStateQueryHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

func (h GetRawStateQueryHandler) createReply(rawState deploymentcommon.RawDeploymentState) service.DeploymentGetRawStateReply {
	var rawStateBytes []byte
	var err error
	switch rawState.Type {
	case deploymentcommon.TerraformRawState:
		rawStateBytes, err = json.Marshal(rawState.TFState)
		if err != nil {
			return h.createErrorReply(service.DeploymentRawStateMarshalError, err.Error())
		}
	default:
		rawStateBytes = nil
	}

	return service.DeploymentGetRawStateReply{
		Session:  service.Session{},
		RawState: rawStateBytes,
	}
}

func (h GetRawStateQueryHandler) createErrorReply(errType, errMsg string) service.DeploymentGetRawStateReply {
	return service.DeploymentGetRawStateReply{
		Session: service.Session{
			ErrorType:    errType,
			ErrorMessage: errMsg,
		},
		RawState: nil,
	}
}

// GetLogsQueryHandler handles GetLogs query
type GetLogsQueryHandler struct {
	storage ports.DeploymentRunStorage
	handlerCommon
}

// QueryOp ...
func (h GetLogsQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetLogsQueryType
}

// Handle handles the query
func (h GetLogsQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "GetLogsQueryHandler.Handle",
	})
	var getLogsQuery service.DeploymentGetLogsQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getLogsQuery)
	if err != nil {
		logger.WithError(err).Error("unable to unmarshal query")
		return h.createErrorReply("queryUnmarshalError", err.Error())
	}

	run, err := h.storage.Get(getLogsQuery.Run)
	if err != nil {
		return h.createErrorReply(service.DeploymentHistoryNotFound, err.Error())
	}

	if ok, err := h.perm.DeploymentAccessByID(getLogsQuery.GetSessionActor(), run.Deployment); err != nil {
		logger.WithError(err).Error("permission check failed")
		return h.createErrorReply(service.AuthorizationFailed, "Error during authorization check")
	} else if !ok {
		return h.createErrorReply(service.GeneralActorNotAuthorizedError, "")
	}

	if !run.HasLogs() {
		return h.createErrorReply(service.DeploymentNoLogsAvailable, "")
	}

	return h.createReply(*run.Logs)
}

// HandleStream handles the query
func (h GetLogsQueryHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

func (h GetLogsQueryHandler) createReply(logs string) service.DeploymentGetLogsReply {
	return service.DeploymentGetLogsReply{
		Logs: logs,
	}
}

func (h GetLogsQueryHandler) createErrorReply(errType, errMsg string) service.DeploymentGetLogsReply {
	var reply service.DeploymentGetLogsReply
	reply.ErrorType = errType
	reply.ErrorMessage = errMsg
	return reply
}
