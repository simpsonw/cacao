package domain

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
)

// PermissionChecker checks if user has permission to access certain object
type PermissionChecker interface {
	// check if a user has access to a deployment
	DeploymentAccess(user string, d deploymentcommon.Deployment) (bool, error)
	// check if a user has access to a deployment
	DeploymentAccessByID(user string, deployment common.ID) (bool, error)
	// check if a user has access to a workspace
	WorkspaceAccess(user string, workspace common.ID) (bool, error)
}

type permChecker struct {
	storage         ports.DeploymentStorage
	workspaceClient interface{}
}

// NewPermissionCheck creates a new permission checker
func NewPermissionCheck(storage ports.DeploymentStorage) PermissionChecker {
	return permChecker{storage: storage}
}

// DeploymentAccess checks for permission on deployment access
func (c permChecker) DeploymentAccess(user string, d deploymentcommon.Deployment) (bool, error) {
	// TODO creator of deployment are not necessarily always able to access deployment.
	// e.g. they could be removed from accessing the underlying workspace.
	if user == d.CreatedBy.User {
		return true, nil
	}
	return false, nil
	//return c.WorkspaceAccess(user, deployment.Workspace)
}

// DeploymentAccessByID checks for permission on deployment access
func (c permChecker) DeploymentAccessByID(user string, deploymentID common.ID) (bool, error) {
	deployment, err := c.storage.Get(deploymentID)
	if err != nil {
		return false, err
	}
	return c.DeploymentAccess(user, deployment)
}

// WorkspaceAccess checks for permission on workspace access
func (c permChecker) WorkspaceAccess(user string, workspace common.ID) (bool, error) {
	// FIXME
	//ws, err := c.workspaceClient.Get(workspace)
	//return ws.Owner == user
	return true, nil
}
