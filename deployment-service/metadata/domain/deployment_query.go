package domain

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// If the length of a list result exceeds the threshold, then streaming the result in multiple replies.
// This is also the max object count per reply.
// Typical payload length of 1 deployment is ~600 bytes, 1MB/600bytes = ~1700 deployment/NATS_msg
const listStreamingThreshold = 1000

// common dependencies for query handler
type handlerCommon struct {
	perm     PermissionChecker
	clientID string
}

// GetQueryHandler is handler for get query
type GetQueryHandler struct {
	storage ports.DeploymentStorage
	handlerCommon
}

// QueryOp ...
func (h GetQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetQueryType
}

// Handle handles the query, and returns a single reply
func (h GetQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "get",
	})
	var getQuery service.DeploymentGetQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getQuery)
	if err != nil {
		logger.WithField("cloudevent", query.CloudEvent()).WithError(err).Error("fail to unmarshal the query")
		return h.createErrorReply("", "queryUnmarshalError", err.Error())
	}
	logger = logger.WithFields(log.Fields{
		"deployment": getQuery.ID,
		"actor":      getQuery.SessionActor,
		"emulator":   getQuery.SessionEmulator,
	})

	deployment, err := h.storage.Get(getQuery.ID)
	if err == storage.ErrDeploymentNotFound {
		logger.Error(err)
		return h.createErrorReply(getQuery.ID, service.DeploymentNotFound, "")
	} else if err != nil {
		logger.WithError(err).Error("fail to fetch deployment")
		return h.createErrorReply(getQuery.ID, service.DeploymentNotFound, err.Error())
	}

	if ok, err := h.perm.DeploymentAccess(getQuery.GetSessionActor(), deployment); err != nil {
		logger.WithError(err).Error("permission check failed")
		return h.createErrorReply(getQuery.ID, service.AuthorizationFailed, "Error during authorization check")
	} else if !ok {
		logger.Error("not authorized")
		return h.createErrorReply(getQuery.ID, service.GeneralActorNotAuthorizedError, "")
	}

	return h.createReply(deployment)
}

// HandleStream handles the query
func (h GetQueryHandler) HandleStream(query types.Query) (replies []Reply) {
	return []Reply{h.Handle(query)}
}

func (h GetQueryHandler) createErrorReply(deployment common.ID, errType, errMsg string) service.DeploymentGetReply {
	var reply service.DeploymentGetReply
	reply.ErrorType = errType
	reply.ErrorMessage = errMsg
	reply.DeploymentID = deployment
	return reply
}

func (h GetQueryHandler) createReply(deployment deploymentcommon.Deployment) service.DeploymentGetReply {
	return service.DeploymentGetReply{
		Session:      service.Session{},
		Deployment:   deployment.ConvertToExternal(),
		DeploymentID: deployment.ID,
	}
}

// ListQueryHandler is handler for list query
type ListQueryHandler struct {
	storage ports.DeploymentStorage
	handlerCommon
}

// QueryOp ...
func (h ListQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentListQueryType
}

// Handle handles the query
func (h ListQueryHandler) Handle(query types.Query) Reply {
	panic("not supported")
}

// HandleStream handles the query
func (h ListQueryHandler) HandleStream(query types.Query) []Reply {
	logger := log.WithFields(log.Fields{
		"query": "list",
	})
	var listQuery service.DeploymentListQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &listQuery)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply("queryUnmarshalError", err.Error())
	}
	logger = logger.WithFields(log.Fields{
		"filters":  listQuery.Filters,
		"actor":    listQuery.SessionActor,
		"emulator": listQuery.SessionEmulator,
	})

	filters := storage.DeploymentFilter{
		// Only return deployments creator by user
		Creator:              listQuery.GetSessionActor(),
		Template:             listQuery.Filters.Template,
		Workspace:            listQuery.Filters.Workspace,
		PrimaryCloudProvider: listQuery.Filters.PrimaryCloudProvider,
		Status:               listQuery.Filters.Status,
	}
	sort, err := storage.FromExternalDeploymentSort(listQuery.SortBy, listQuery.SortDirection)
	if err != nil {
		logger.WithError(err).Error("invalid sort option in request")
		return h.createErrorReply(err.Error(), "")
	}
	deployments, err := h.storage.List(
		filters,
		int64(listQuery.Offset),
		int64(listQuery.Limit),
		sort,
	)
	if err == storage.ErrEmptyDeploymentList {
		return []Reply{h.createEmptyListReply()}
	} else if err != nil {
		return h.createErrorReply(err.Error(), err.Error())
	}
	log.WithField("len", len(deployments)).Warn()

	return h.createReplies(listQuery.Offset, deployments)
}

func (h ListQueryHandler) createErrorReply(errType, errMsg string) []Reply {
	var reply service.DeploymentListReply
	reply.ErrorType = errType
	reply.ErrorMessage = errMsg
	return []Reply{reply}
}

func (h ListQueryHandler) createReplies(initialOffset int, deployments []deploymentcommon.Deployment) []Reply {
	if deployments == nil || len(deployments) == 0 {
		return []Reply{h.createEmptyListReply()}
	} else if len(deployments) < listStreamingThreshold {
		return []Reply{h.singleReply(initialOffset, deployments)}
	} else {
		return h.multipleReplies(initialOffset, deployments)
	}
}

func (h ListQueryHandler) createEmptyListReply() service.DeploymentListReply {
	return service.DeploymentListReply{
		Session:     service.Session{},
		Offset:      0,
		Count:       0,
		Deployments: make([]service.Deployment, 0),
		ReplyListStream: service.ReplyListStream{
			ReplyIndex:   0,
			TotalReplies: 1,
		},
	}
}

func (h ListQueryHandler) singleReply(initialOffset int, deployments []deploymentcommon.Deployment) service.DeploymentListReply {
	return service.DeploymentListReply{
		Session:     service.Session{},
		Offset:      initialOffset,
		Count:       len(deployments),
		Deployments: h.convertDeploymentListToExternal(deployments),
		ReplyListStream: service.ReplyListStream{
			TotalReplies: 1,
			ReplyIndex:   0,
		},
	}
}

func (h ListQueryHandler) multipleReplies(initialOffset int, deployments []deploymentcommon.Deployment) []Reply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ListQueryHandler.multipleReplies",
	})
	allReplies := make([]Reply, 0)

	allChunks := h.divideList(deployments)
	offsetCounter := initialOffset
	for chunkIndex, chunk := range allChunks {
		reply := service.DeploymentListReply{
			Session:     service.Session{},
			Offset:      offsetCounter,
			Count:       len(chunk),
			Deployments: h.convertDeploymentListToExternal(chunk),
			ReplyListStream: service.ReplyListStream{
				TotalReplies: len(allChunks),
				ReplyIndex:   chunkIndex,
			},
		}
		allReplies = append(allReplies, reply)
	}
	logger.WithField("totalReplies", len(allReplies)).Debug("large deployment list divided into multiple replies")
	return allReplies
}

func (h ListQueryHandler) divideList(deployments []deploymentcommon.Deployment) [][]deploymentcommon.Deployment {
	if len(deployments) <= listStreamingThreshold {
		return [][]deploymentcommon.Deployment{deployments}
	}
	var allChunks = make([][]deploymentcommon.Deployment, 0)
	var chunk []deploymentcommon.Deployment
	var start = 0
	var end = listStreamingThreshold
	for start < len(deployments) {
		chunk = deployments[start:end]
		allChunks = append(allChunks, chunk)
		start = end
		if start+listStreamingThreshold >= len(deployments) {
			end = len(deployments)
		} else {
			end = start + listStreamingThreshold
		}
	}
	return allChunks
}

func (h ListQueryHandler) convertDeploymentListToExternal(deployments []deploymentcommon.Deployment) []service.Deployment {
	extDeployments := make([]service.Deployment, 0, len(deployments))
	for _, deployment := range deployments {
		extDeployments = append(extDeployments, deployment.ConvertToExternal())
	}
	return extDeployments
}
