package domain

import (
	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
	domainmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/domain/mocks"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	typesmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/types/mocks"
	"math/rand"
	"testing"
)

func TestGetQueryHandler_Handle(t *testing.T) {
	t.Run("existing deployment", testGetHandlerExistingDeployment)
	t.Run("deployment not found", testGetHandlerNotFound)
	t.Run("not authorized", testGetHandlerNotAuthorized)
	t.Run("empty request", testGetHandlerEmptyRequest)
}

func testGetHandlerExistingDeployment(t *testing.T) {
	getQuery := service.DeploymentGetQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		ID: common.NewID(types.DeploymentIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployment := deploymentcommon.Deployment{
		ID: getQuery.ID,
		CreatedBy: deploymentcommon.Creator{
			User:     getQuery.GetSessionActor(),
			Emulator: "",
		},
		Template:  common.NewID("template"),
		Workspace: common.NewID("workspace"),
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", getQuery.ID).Return(deployment, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On("DeploymentAccess", getQuery.GetSessionActor(), deployment).Return(true, nil)

	h := GetQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.Equal(t, "", getReply.ErrorType)
	assert.Equal(t, "", getReply.ErrorMessage)
	assert.Equal(t, deployment.ID, getReply.Deployment.ID)
	assert.Equal(t, deployment.Template, getReply.Deployment.Template)
	assert.Equal(t, deployment.Workspace, getReply.Deployment.Workspace)
}

func testGetHandlerNotFound(t *testing.T) {
	getQuery := service.DeploymentGetQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		ID: common.NewID(types.DeploymentIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deploymentStorage := &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", getQuery.ID).Return(deploymentcommon.Deployment{}, storage.ErrDeploymentNotFound)

	var perm = &domainmocks.PermissionChecker{}

	h := GetQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.Equal(t, service.DeploymentNotFound, getReply.ErrorType)
	assert.Equal(t, "", getReply.ErrorMessage)
}

func testGetHandlerNotAuthorized(t *testing.T) {
	getQuery := service.DeploymentGetQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		ID: common.NewID(types.DeploymentIDPrefix),
	}
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployment := deploymentcommon.Deployment{
		ID: getQuery.ID,
		CreatedBy: deploymentcommon.Creator{
			User:     getQuery.GetSessionActor(),
			Emulator: "",
		},
		Template:  common.NewID("template"),
		Workspace: common.NewID("workspace"),
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", deployment.ID).Return(deployment, nil)

	var perm = &domainmocks.PermissionChecker{}
	perm.On(
		"DeploymentAccess",
		getQuery.GetSessionActor(),
		deployment,
	).Return(false, nil) // not authorized

	h := GetQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.Equal(t, service.GeneralActorNotAuthorizedError, getReply.ErrorType)
	assert.Equal(t, "", getReply.ErrorMessage)
}

func testGetHandlerEmptyRequest(t *testing.T) {
	getQuery := service.DeploymentGetQuery{} // empty request
	ce, err := messaging.CreateCloudEvent(getQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On("Get", common.ID("")).Return(deploymentcommon.Deployment{}, storage.ErrDeploymentNotFound)

	var perm = &domainmocks.PermissionChecker{}

	h := GetQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	reply := h.Handle(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	assert.IsType(t, service.DeploymentGetReply{}, reply)
	getReply := reply.(service.DeploymentGetReply)
	assert.Equal(t, service.DeploymentNotFound, getReply.ErrorType)
	assert.Equal(t, "", getReply.ErrorMessage)
}

func TestListQueryHandler_HandleStream(t *testing.T) {
	t.Run("existing deployment", testListHandlerExistingDeployments)
	t.Run("empty list", testListHandlerEmptyList)
	t.Run("streaming in multiple replies", testListHandlerStreamingList)
	t.Run("streaming in multiple replies, multiple of threshold", testListHandlerStreamingListThresholdMultiple)
	t.Run("filter by workspace", testListHandlerFilterByWorkspace)
	t.Run("filter by template", testListHandlerFilterByTemplate)
	t.Run("sort descending order", testListHandlerDescendingSort)
}

func testListHandlerExistingDeployments(t *testing.T) {
	listQuery := service.DeploymentListQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Filters:       service.DeploymentFilter{},
		SortBy:        "",
		SortDirection: 0,
		Offset:        0,
		Limit:         -1,
	}
	ce, err := messaging.CreateCloudEvent(listQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployments := make([]deploymentcommon.Deployment, 0)
	for i := 0; i < 10; i++ {
		deployments = append(deployments,
			deploymentcommon.Deployment{
				ID: common.NewID(types.DeploymentIDPrefix),
				CreatedBy: deploymentcommon.Creator{
					User:     listQuery.GetSessionActor(),
					Emulator: "",
				},
				Template:  common.NewID("template"),
				Workspace: common.NewID("workspace"),
			})
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On(
		"List",
		storage.DeploymentFilter{
			Creator: listQuery.GetSessionActor(),
		},
		int64(0),
		int64(-1),
		storage.DeploymentSort{
			SortBy:  "_id",
			SortDir: 1,
		},
	).Return(deployments, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListReply{}, reply)
	listReply := reply.(service.DeploymentListReply)
	assert.Equal(t, "", listReply.ErrorType)
	assert.Equal(t, "", listReply.ErrorMessage)
	if !assert.Len(t, listReply.Deployments, len(deployments)) {
		return
	}
	for i := range listReply.Deployments {
		assert.Equal(t, deployments[i].ID, listReply.Deployments[i].ID)
		assert.Equal(t, deployments[i].Template, listReply.Deployments[i].Template)
		assert.Equal(t, deployments[i].Workspace, listReply.Deployments[i].Workspace)
	}
}

func testListHandlerStreamingList(t *testing.T) {
	listQuery := service.DeploymentListQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Filters:       service.DeploymentFilter{},
		SortBy:        "",
		SortDirection: 0,
		Offset:        0,
		Limit:         -1,
	}
	ce, err := messaging.CreateCloudEvent(listQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	// not multiple of threshold
	deploymentCount := 5*types.DeploymentStreamThreshold + rand.Intn(100) + 3
	deployments := make([]deploymentcommon.Deployment, 0, deploymentCount)
	for i := 0; i < deploymentCount; i++ {
		deployments = append(deployments,
			deploymentcommon.Deployment{
				ID: common.NewID(types.DeploymentIDPrefix),
				CreatedBy: deploymentcommon.Creator{
					User:     listQuery.GetSessionActor(),
					Emulator: "",
				},
				Template:  common.NewID("template"),
				Workspace: common.NewID("workspace"),
			})
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On(
		"List",
		storage.DeploymentFilter{
			Creator: listQuery.GetSessionActor(),
		},
		int64(0),
		int64(-1),
		storage.DeploymentSort{
			SortBy:  "_id",
			SortDir: 1,
		},
	).Return(deployments, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	expectedRepliesCount := len(deployments) / types.DeploymentStreamThreshold
	if len(deployments)%types.DeploymentStreamThreshold > 0 {
		expectedRepliesCount++
	}

	if !assert.Len(t, replies, expectedRepliesCount) {
		return
	}
	for replyIndex, reply := range replies {
		assert.IsType(t, service.DeploymentListReply{}, reply)
		listReply := reply.(service.DeploymentListReply)
		assert.Equal(t, "", listReply.ErrorType)
		assert.Equal(t, "", listReply.ErrorMessage)
		if replyIndex >= len(replies)-1 {
			assert.Len(t, listReply.Deployments, len(deployments)%types.DeploymentStreamThreshold)
		} else {
			assert.Len(t, listReply.Deployments, types.DeploymentStreamThreshold)
		}
	}
}

func testListHandlerStreamingListThresholdMultiple(t *testing.T) {
	listQuery := service.DeploymentListQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Filters:       service.DeploymentFilter{},
		SortBy:        "",
		SortDirection: 0,
		Offset:        0,
		Limit:         -1,
	}
	ce, err := messaging.CreateCloudEvent(listQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	// multiple of threshold
	deploymentCount := 5 * types.DeploymentStreamThreshold
	deployments := make([]deploymentcommon.Deployment, 0, deploymentCount)
	for i := 0; i < deploymentCount; i++ {
		deployments = append(deployments,
			deploymentcommon.Deployment{
				ID: common.NewID(types.DeploymentIDPrefix),
				CreatedBy: deploymentcommon.Creator{
					User:     listQuery.GetSessionActor(),
					Emulator: "",
				},
				Template:  common.NewID("template"),
				Workspace: common.NewID("workspace"),
			})
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On(
		"List",
		storage.DeploymentFilter{
			Creator: listQuery.GetSessionActor(),
		},
		int64(0),
		int64(-1),
		storage.DeploymentSort{
			SortBy:  "_id",
			SortDir: 1,
		},
	).Return(deployments, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	expectedRepliesCount := len(deployments) / types.DeploymentStreamThreshold
	if len(deployments)%types.DeploymentStreamThreshold > 0 {
		expectedRepliesCount++
	}

	if !assert.Len(t, replies, expectedRepliesCount) {
		return
	}
	for _, reply := range replies {
		assert.IsType(t, service.DeploymentListReply{}, reply)
		listReply := reply.(service.DeploymentListReply)
		assert.Equal(t, "", listReply.ErrorType)
		assert.Equal(t, "", listReply.ErrorMessage)
		assert.Len(t, listReply.Deployments, types.DeploymentStreamThreshold)
	}
}

func testListHandlerEmptyList(t *testing.T) {
	listQuery := service.DeploymentListQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Filters:       service.DeploymentFilter{},
		SortBy:        "",
		SortDirection: 0,
		Offset:        0,
		Limit:         -1,
	}
	ce, err := messaging.CreateCloudEvent(listQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On(
		"List",
		storage.DeploymentFilter{
			Creator: listQuery.GetSessionActor(),
		},
		int64(0),
		int64(-1),
		storage.DeploymentSort{
			SortBy:  "_id",
			SortDir: 1,
		},
	).Return(nil, storage.ErrEmptyDeploymentList)

	var perm = &domainmocks.PermissionChecker{}

	h := ListQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListReply{}, reply)
	listReply := reply.(service.DeploymentListReply)
	assert.Equal(t, "", listReply.ErrorType)
	assert.Equal(t, "", listReply.ErrorMessage)
	if !assert.Len(t, listReply.Deployments, 0) {
		return
	}
}

func testListHandlerFilterByWorkspace(t *testing.T) {
	listQuery := service.DeploymentListQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Filters: service.DeploymentFilter{
			User:                 "",
			Template:             "",
			Workspace:            common.NewID("workspace"),
			PrimaryCloudProvider: "",
			Status:               "",
		},
		SortBy:        "",
		SortDirection: 0,
		Offset:        0,
		Limit:         -1,
	}
	ce, err := messaging.CreateCloudEvent(listQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployments := make([]deploymentcommon.Deployment, 0)
	for i := 0; i < 10; i++ {
		deployments = append(deployments, deploymentcommon.Deployment{
			ID: common.NewID(types.DeploymentIDPrefix),
			CreatedBy: deploymentcommon.Creator{
				User:     listQuery.GetSessionActor(),
				Emulator: "",
			},
			Template:  common.NewID("template"),
			Workspace: listQuery.Filters.Workspace,
		})
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On(
		"List",
		storage.DeploymentFilter{
			Creator:   listQuery.GetSessionActor(),
			Workspace: listQuery.Filters.Workspace,
		},
		int64(0),
		int64(-1),
		storage.DeploymentSort{
			SortBy:  "_id",
			SortDir: 1,
		},
	).Return(deployments, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListReply{}, reply)
	listReply := reply.(service.DeploymentListReply)
	assert.Equal(t, "", listReply.ErrorType)
	assert.Equal(t, "", listReply.ErrorMessage)
	if !assert.Len(t, listReply.Deployments, len(deployments)) {
		return
	}
	for i := range listReply.Deployments {
		assert.Equal(t, deployments[i].ID, listReply.Deployments[i].ID)
		assert.Equal(t, deployments[i].Template, listReply.Deployments[i].Template)
		assert.Equal(t, deployments[i].Workspace, listReply.Deployments[i].Workspace)
	}
}

func testListHandlerFilterByTemplate(t *testing.T) {
	listQuery := service.DeploymentListQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Filters:       service.DeploymentFilter{},
		SortBy:        "",
		SortDirection: 0,
		Offset:        0,
		Limit:         -1,
	}
	ce, err := messaging.CreateCloudEvent(listQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployment := deploymentcommon.Deployment{
		ID: common.NewID(types.DeploymentIDPrefix),
		CreatedBy: deploymentcommon.Creator{
			User:     listQuery.GetSessionActor(),
			Emulator: "",
		},
		Template:  common.NewID("template"),
		Workspace: common.NewID("workspace"),
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On(
		"List",
		storage.DeploymentFilter{
			Creator:  listQuery.GetSessionActor(),
			Template: listQuery.Filters.Template,
		},
		int64(0),
		int64(-1),
		storage.DeploymentSort{
			SortBy:  "_id",
			SortDir: 1,
		},
	).Return([]deploymentcommon.Deployment{deployment}, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}
	replies := h.HandleStream(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)

	if !assert.Len(t, replies, 1) {
		return
	}
	reply := replies[0]
	assert.IsType(t, service.DeploymentListReply{}, reply)
	listReply := reply.(service.DeploymentListReply)
	assert.Equal(t, "", listReply.ErrorType)
	assert.Equal(t, "", listReply.ErrorMessage)
	if !assert.Len(t, listReply.Deployments, 1) {
		return
	}
	assert.Equal(t, deployment.ID, listReply.Deployments[0].ID)
	assert.Equal(t, deployment.Template, listReply.Deployments[0].Template)
	assert.Equal(t, deployment.Workspace, listReply.Deployments[0].Workspace)
}

func testListHandlerDescendingSort(t *testing.T) {
	listQuery := service.DeploymentListQuery{
		Session: service.Session{
			SessionActor:    xid.New().String(),
			SessionEmulator: "",
		},
		Filters:       service.DeploymentFilter{},
		SortBy:        "workspace",
		SortDirection: service.DescendingSort,
		Offset:        0,
		Limit:         -1,
	}
	ce, err := messaging.CreateCloudEvent(listQuery, string(service.DeploymentGetQueryType), "")
	assert.NoError(t, err)
	var query = &typesmocks.Query{}
	query.On("CloudEvent").Return(ce)

	deployments := make([]deploymentcommon.Deployment, 0)
	for i := 0; i < 10; i++ {
		deployments = append(deployments, deploymentcommon.Deployment{
			ID: common.NewID(types.DeploymentIDPrefix),
			CreatedBy: deploymentcommon.Creator{
				User:     listQuery.GetSessionActor(),
				Emulator: "",
			},
			Template:  common.NewID("template"),
			Workspace: listQuery.Filters.Workspace,
		})
	}
	var deploymentStorage = &portsmocks.DeploymentStorage{}
	deploymentStorage.On(
		"List",
		storage.DeploymentFilter{
			Creator: listQuery.GetSessionActor(),
		},
		int64(0),
		int64(-1),
		storage.DeploymentSort{
			SortBy:  "workspace",
			SortDir: storage.DescendingSort,
		},
	).Return(deployments, nil)

	var perm = &domainmocks.PermissionChecker{}

	h := ListQueryHandler{
		storage: deploymentStorage,
		handlerCommon: handlerCommon{
			perm:     perm,
			clientID: "",
		},
	}

	h.HandleStream(query)
	query.AssertExpectations(t)
	deploymentStorage.AssertExpectations(t)
	perm.AssertExpectations(t)
}
