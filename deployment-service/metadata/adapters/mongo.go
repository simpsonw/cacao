package adapters

import (
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/deployment-service/internal/storage"
)

// NewMongoDeploymentStorage ...
func NewMongoDeploymentStorage(conf db.MongoDBConfig) *storage.MongoDeploymentStorage {
	return storage.NewMongoDeploymentStorage(conf)
}

// NewMongoDeploymentRunStorage ...
func NewMongoDeploymentRunStorage(config db.MongoDBConfig) *storage.MongoDeploymentRunStorage {
	return storage.NewMongoDeploymentRunStorage(config)
}
