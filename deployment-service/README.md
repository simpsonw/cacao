# deployment service

Deployment service is an aggregate of services, which consist of metadata service, execution service, monitor service.
A deployment represents the instaniation of a template. A deployment run is each time the template is run/executed.

# Deployment Metadata Service
Metadata service handles all queries.

## Queries
- Get Deployment
- List Deployment
- Get Deployment Run
- List Deployment Run
- Get Raw State of Run
- Get Logs of Run

# Execution Service
Execution service handles the execution of templates.
It also handles creation/update/deletion events.

## Events
- DeploymentCreationRequested
- DeploymentParameterUpdateRequested
- DeploymentCreateRunRequested
- DeploymentDeleteRequested

# Monitor Service
Monitor service handles the monitoring of deployment (refreshing the state of deployment).
It handles events related to monitoring deployment.

## Events
- DeploymentMonitorAddRequested
- DeploymentMonitorRemovalRequested
- DeploymentStateRefreshRequested
