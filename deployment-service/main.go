// This file does nothing other than ensuring there is a *.go file in this
// directory. Linting tools such as ineffassign and misspell requires a go
// file in the directory they operate on.
package main
